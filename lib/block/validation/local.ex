defmodule Block.Validation.Local do
  def valid(block) do
    [
      Block.Validation.Local.Version,
      Block.Validation.Local.InnerHash,
      Block.Validation.Local.Nonce,
      Block.Validation.Local.ProofOfWork,
      Block.Validation.Local.PreviousHash,
      Block.Validation.Local.PreIssuer,
      Block.Validation.Local.Parameters,
      Block.Validation.Local.UniversalDividend,
      Block.Validation.Local.UnitBase,
      Block.Validation.Local.BlockSignature,
      Block.Validation.Local.BlockTime,
      Block.Validation.Local.IdentitySignature,
      Block.Validation.Local.MembershipSig,
      Block.Validation.Local.TransactionNumberOfLines,
      Block.Validation.Local.TransactionInput,
      Block.Validation.Local.TxInfNbIssuers,
      Block.Validation.Local.TransactionVersion,
      Block.Validation.Local.TransactionCount,
      Block.Validation.Local.TxSigOrdered
    ]
    |> Enum.all?(fn module -> module.valid(block) end)
    |> then(fn is_valid ->
      if is_valid do
        case Index.Generation.process(block) do
          {:ok, [local_iindex, local_mindex, local_cindex, local_sindex]} ->
            if IndexRules.check(local_iindex, local_mindex, local_cindex, local_sindex) do
              {:ok, [local_iindex, local_mindex, local_cindex, local_sindex]}
            else
              {:error, "Index rules not satisfied"}
            end

          {:error, error} ->
            {:error, error}
        end
      else
        {:error, "Block is not valid"}
      end
    end)
  end
end
