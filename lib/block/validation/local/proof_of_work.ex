defmodule Block.Validation.Local.ProofOfWork do
  @moduledoc """
  https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#proof-of-work
  """
  def valid(block) do
    required_zeros = div(block["powMin"], 16)
    String.match?(block["hash"], ~r/^0{#{required_zeros}}/)
  end
end
