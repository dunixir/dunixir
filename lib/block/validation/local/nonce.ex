defmodule Block.Validation.Local.Nonce do
  @moduledoc """
  https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#nonce
  """
  def valid(block) do
    is_integer(block["nonce"]) and block["nonce"] >= 0
  end
end
