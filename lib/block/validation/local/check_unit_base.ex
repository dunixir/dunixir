defmodule Block.Validation.Local.UnitBase do
  @moduledoc """
  https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#unitbase
  """
  def valid(block) do
    not (block["unitbase"] != 0 and block["number"] == 0)
  end
end
