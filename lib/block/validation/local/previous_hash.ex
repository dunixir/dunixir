defmodule Block.Validation.Local.PreviousHash do
  @moduledoc """
  https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#previoushash
  """
  def valid(block) do
    cond do
      block["number"] > 0 ->
        String.length(block["previousHash"]) > 0

      block["number"] == 0 ->
        String.length(block["previousHash"]) == 0
    end
  end
end
