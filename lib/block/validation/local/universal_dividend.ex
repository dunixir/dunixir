defmodule Block.Validation.Local.UniversalDividend do
  @moduledoc """
  https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#universal-dividend
  """
  def valid(block) do
    if block["number"] === 0, do: block["dividend"] === nil, else: true
  end
end
