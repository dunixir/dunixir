defmodule Block.Validation.Local.PreIssuer do
  @moduledoc """
  https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#previousissuer
  """
  def valid(block) do
    not (block["number"] == 0 and
           block["previousIssuer"] != nil) and
      not (block["number"] > 0 and
             block["previousIssuer"] == nil)
  end
end
