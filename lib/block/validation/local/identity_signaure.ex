defmodule Block.Validation.Local.IdentitySignature do
  @moduledoc """
  https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#identities
  """
  def valid(block) do
    Doc.Identity.local_validation(block["identities"], block["currency"])
  end
end
