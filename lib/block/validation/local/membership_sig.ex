defmodule Block.Validation.Local.MembershipSig do
  @moduledoc """
  https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#memberships-joiners-actives-leavers
  """
  def valid(block) do
    Doc.Membership.local_validation(
      block["joiners"],
      block["actives"],
      block["leavers"],
      block["currency"]
    )
  end
end
