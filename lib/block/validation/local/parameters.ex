defmodule Block.Validation.Local.Parameters do
  @moduledoc """
  https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#parameters
  """
  def valid(block) do
    (block["number"] == 0 and block["parameters"] != "") or
      (block["number"] > 0 and block["parameters"] == "")
  end
end
