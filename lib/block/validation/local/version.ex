defmodule Block.Validation.Local.Version do
  @moduledoc """
  https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#version
  """
  def valid(block) do
    block["version"] >= 10
  end
end
