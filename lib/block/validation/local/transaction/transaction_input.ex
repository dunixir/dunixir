defmodule Block.Validation.Local.TransactionInput do
  @moduledoc """
  A transaction must have at least 1 source

  https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#transactions
  """
  def valid(block) do
    case block["transactions"] do
      [] ->
        true

      transac ->
        Enum.all?(transac, fn tx ->
          case tx["inputs"] do
            [] -> false
            input -> Enum.any?(input, fn el -> String.length(el) != 0 end)
          end
        end)
    end
  end
end
