defmodule Block.Validation.Local.TransactionCount do
  @moduledoc """
  Signatures count must be the same as issuers count

  https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#transactions
  """
  def valid(block) do
    Enum.all?(block["transactions"], fn element ->
      length(element["signatures"]) == length(element["issuers"])
    end)
  end
end
