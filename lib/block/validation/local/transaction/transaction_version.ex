defmodule Block.Validation.Local.TransactionVersion do
  @moduledoc """
  Transaction's version must be equal to 10.

  https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#transactions
  """
  def valid(block) do
    Enum.all?(block["transactions"], fn element -> element["version"] == 10 end)
  end
end
