defmodule Block.Validation.Local.TransactionNumberOfLines do
  @moduledoc """
  A transaction in [compact format](https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#compact-format) cannot measure more than 100 lines

  https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#transactions
  """
  def valid(block) do
    block["transactions"]
    |> Enum.all?(fn transaction -> compact_format_length(transaction) < 100 end)
  end

  defp compact_format_length(transaction) do
    # header + blockstamp
    # issuers + signatures
    # inputs + unlocks
    2 +
      length(transaction["issuers"]) * 2 +
      length(transaction["inputs"]) * 2 +
      if(transaction["comment"] && String.length(transaction["comment"]) > 0,
        do: 1,
        else: 0
      ) + length(transaction["outputs"])
  end
end
