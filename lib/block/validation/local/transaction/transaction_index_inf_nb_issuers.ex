defmodule Block.Validation.Local.TxInfNbIssuers do
  @moduledoc """
  A transaction cannot have SIG(INDEX) unlocks with INDEX >=  issuers count.

  https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#transactions
  """
  def valid(block) do
    case block["transactions"] do
      # Since there is no transaction field, there is no transaction to check
      nil -> true
      transactions -> _checkTxList(transactions)
    end
  end

  @doc """
  Checks every element in a list of transaction to make sure they are all valid
  """
  # No transaction to check
  def _checkTxList([]) do
    true
  end

  # We have a list of transactions
  def _checkTxList([tx | tail]) do
    # We need to check the first transaction and the rest of the transactions
    _checkTx(tx) and _checkTxList(tail)
  end

  @doc """
  Checks and validates a single transaction
  """
  def _checkTx(tx) do
    issuerCount = Enum.count(tx["issuers"])
    to_unlock = tx["unlocks"]
    _checkUnlocks(issuerCount, to_unlock)
  end

  @doc """
  Checks every unlock SIG index value is below the number of issuers
  """
  def _checkUnlocks(_issuerCount, []) do
    true
  end

  def _checkUnlocks(issuerCount, [unlock | tail]) do
    _getSIGValues(unlock) |> Enum.all?(fn [sig_value] -> String.to_integer(sig_value) <= issuerCount - 1 end) and
      _checkUnlocks(issuerCount, tail)
  end

  @doc """
  Returns the index values of SIG unlocks
  """
  def _getSIGValues(unlock) do
    Regex.compile!("SIG\\((\\d+)\\)") |> Regex.scan(unlock, capture: :all_but_first)
  end
end
