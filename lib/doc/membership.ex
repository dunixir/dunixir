defmodule Doc.Membership do
  @default_document_version 10
  @doc """
  Verify format (fields and values) and signature from a membership document

  ## Example
      iex> Doc.Membership.verif(%{
      ...> "currency" => "duniter_unit_test_currency",
      ...> "issuer" => "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
      ...> "type" => "IN",
      ...> "blockstamp" => "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
      ...> "userid" => "tic",
      ...> "certts" => "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
      ...> "signature" =>
      ...>  "s2hUbokkibTAWGEwErw6hyXSWlWFQ2UWs2PWx8d/kkElAyuuWaQq4Tsonuweh1xn4AC1TVWt4yMR3WrDdkhnAw=="
      ...> })
      true
  """
  def verif(
        %{
          "currency" => _currency,
          "userid" => _userid,
          "certts" => _certts,
          "signature" => _signature
        } = membership
      )
      when (is_map_key(membership, "issuer") or is_map_key(membership, "pubkey")) and
             (is_map_key(membership, "type") or is_map_key(membership, "membership")) and
             (is_map_key(membership, "blockstamp") or is_map_key(membership, "block")) do
    cond do
      check_values(membership) ->
        membership = enforce_keys(membership)

        Crypto.verify_digital_signature(
          membership["signature"],
          raw_format(membership),
          membership["issuer"]
        )

      true ->
        false
    end
  end

  def verif(_), do: false

  @doc """
  Verify memberships found inside a block in the context of the local validation of a block

  ## Example
      iex> block = %{
      ...>    "currency" => "g1-test",
      ...>    "joiners" => [
      ...>      "5ocqzyDMMWf1V8bsoNhWb1iNwax1e9M7VTUN6navs8of:RH+aBL2YP7g90G/ujN0ww1LXc+QiHGok6/CIlrQ3WWSE1rrWQhYIw4Fas+T56kxPvC3b6PDy8RBWt74TTTTqAg==:0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855:0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855:kimamila",
      ...>      "BfkVa967WrC8YTHPN9tr49Bht8HLHv4wQMR8yu6Yk1iV:ALgSvDa6pnoU8RSKrWEVtBy73DySKzwpYkLSUeiX1+Cs6Uho4HrMD/LXJWHrN3kon+TE+7sXmz2cjm8o8sZ8Cw==:0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855:0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855:Galuel"
      ...>    ],
      ...>    "actives" => [
      ...>      "D7CYHJXjaH4j7zRdWngUbsURPnSnjsCYtvo6f8dvW3C:pJGEABRTTf/zEUliqikPiTuptLN9W31MFBgdYzwiuBYGl353JEYkGvr14bR7pPJVbxyABnk9lfzKzCgwAdLCBw==:0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855:0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855:elois"
      ...>    ],
      ...>    "leavers" => []
      ...>  }
      iex> Doc.Membership.local_validation(block["joiners"], block["actives"], block["leavers"], block["currency"])
      true
  """
  def local_validation(joiners, actives, leavers, currency) do
    local_validation(joiners, "IN", currency) and
      local_validation(actives, "IN", currency) and
      local_validation(leavers, "OUT", currency)
  end

  defp local_validation(memberships, type, currency) do
    case length(memberships) do
      0 ->
        true

      _ ->
        memberships
        |> Enum.all?(fn inline_membership ->
          inline_membership
          |> to_map(type, currency)
          |> verif
        end)
    end
  end

  defp to_map(inline_membership, type, currency) do
    [issuer, sig, blockstamp, certts, userid] = String.split(inline_membership, ":")

    %{
      "version" => @default_document_version,
      "currency" => currency,
      "issuer" => issuer,
      "type" => type,
      "blockstamp" => blockstamp,
      "userid" => userid,
      "certts" => certts,
      "signature" => sig
    }
  end

  defp regex_exact_match(regex, string) do
    Regex.compile!("^#{regex}$")
    |> Regex.match?(string)
  end

  defp check_values(membership) do
    enforced_keys_membership = enforce_keys(membership)

    (membership["version"] == nil or membership["version"] == 10) and
      regex_exact_match(Constants.currency_regexp(), enforced_keys_membership["currency"]) and
      regex_exact_match(Constants.pubkey_regexp(), enforced_keys_membership["issuer"]) and
      regex_exact_match(Constants.block_uid_regexp(), enforced_keys_membership["blockstamp"]) and
      regex_exact_match("IN|OUT", enforced_keys_membership["type"]) and
      regex_exact_match(Constants.uid_regexp(), enforced_keys_membership["userid"]) and
      regex_exact_match(Constants.block_uid_regexp(), enforced_keys_membership["certts"]) and
      regex_exact_match(Constants.signature_regexp(), enforced_keys_membership["signature"])
  end

  defp enforce_keys(membership) do
    membership
    |> Map.put_new("version", @default_document_version)
    |> Map.put_new("issuer", membership["pubkey"])
    |> Map.put_new("type", membership["membership"])
    |> Map.put_new("blockstamp", membership["block"])
  end

  defp raw_format(membership) do
    # Convert to the raw format of DUBP
    """
    Version: #{membership["version"]}
    Type: Membership
    Currency: #{membership["currency"]}
    Issuer: #{membership["issuer"]}
    Block: #{membership["blockstamp"]}
    Membership: #{membership["type"]}
    UserID: #{membership["userid"]}
    CertTS: #{membership["certts"]}
    """
  end
end
