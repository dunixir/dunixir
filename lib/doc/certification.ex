defmodule Doc.Certification do
  @default_document_version 10
  @moduledoc """
  to factorize with functions from Block.Validation.Local
  """

  @doc """
  Verify format (fields and values) and signature from a certification document

  ## Example
      iex> Doc.Certification.verif(%{
      ...> "currency" => "g1-test",
      ...> "pubkey" => "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
      ...> "buid" => "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
      ...> "sig" =>
      ...>   "wqZxPEGxLrHGv8VdEIfUGvUcf+tDdNTMXjLzVRCQ4UhlhDRahOMjfcbP7byNYr5OfIl83S1MBxF7VJgu8YasCA==",
      ...> "idty_issuer" => "mMPioknj2MQCX9KyKykdw8qMRxYR2w1u3UpdiEJHgXg",
      ...> "idty_uid" => "mmpio",
      ...> "idty_buid" => "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
      ...> "idty_sig" =>
      ...>   "SmSweUD4lEMwiZfY8ux9maBjrQQDkC85oMNsin6oSQCPdXG8sFCZ4FisUaWqKsfOlZVb/HNa+TKzD2t0Yte+DA=="
      ...> })
      true
  """
  def verif(
        %{
          "currency" => _currency,
          "sig" => _sig,
          "idty_uid" => _idty_uid,
          "idty_buid" => _idty_buid,
          "idty_sig" => _idty_sig
        } = certification
      )
      when (is_map_key(certification, "pubkey") or is_map_key(certification, "issuer") or
              is_map_key(certification, "from")) and
             (is_map_key(certification, "buid") or
                (is_map_key(certification, "block_number") and
                   is_map_key(certification, "block_hash"))) and
             (is_map_key(certification, "idty_issuer") or is_map_key(certification, "to")) do
    cond do
      check_values(certification) ->
        certification = enforce_keys(certification)

        Crypto.verify_digital_signature(
          certification["sig"],
          raw_format(certification),
          certification["pubkey"]
        )

      true ->
        false
    end
  end

  def verif(_), do: false

  defp regex_exact_match(regex, string) do
    Regex.compile!("^#{regex}$")
    |> Regex.match?(string)
  end

  defp check_values(certification) do
    enforced_keys_certification = enforce_keys(certification)

    (certification["version"] == nil or certification["version"] == 10) and
      regex_exact_match(Constants.currency_regexp(), enforced_keys_certification["currency"]) and
      regex_exact_match(Constants.pubkey_regexp(), enforced_keys_certification["pubkey"]) and
      regex_exact_match(Constants.uid_regexp(), enforced_keys_certification["idty_uid"]) and
      regex_exact_match(Constants.block_uid_regexp(), enforced_keys_certification["idty_buid"]) and
      regex_exact_match(Constants.signature_regexp(), enforced_keys_certification["idty_sig"]) and
      regex_exact_match(Constants.block_uid_regexp(), enforced_keys_certification["buid"]) and
      regex_exact_match(Constants.signature_regexp(), enforced_keys_certification["sig"])
  end

  defp enforce_keys(certification) do
    certification
    |> Map.put_new("version", @default_document_version)
    |> Map.put_new(
      "pubkey",
      certification["issuer"] || certification["from"]
    )
    |> Map.put_new(
      "buid",
      buid(certification["block_number"], certification["block_hash"])
    )
    |> Map.put_new("idty_issuer", certification["to"])
  end

  defp buid(block_number, block_hash), do: "#{block_number}-#{block_hash}"

  defp raw_format(certification) do
    # Convert to the raw format of DUBP
    """
    Version: #{certification["version"]}
    Type: Certification
    Currency: #{certification["currency"]}
    Issuer: #{certification["pubkey"]}
    IdtyIssuer: #{certification["idty_issuer"]}
    IdtyUniqueID: #{certification["idty_uid"]}
    IdtyTimestamp: #{certification["idty_buid"]}
    IdtySignature: #{certification["idty_sig"]}
    CertTimestamp: #{certification["buid"]}
    """
  end
end
