defmodule Doc.Transaction do
  @default_document_version 10
  @moduledoc """
  to factorize with functions from Block.Validation.Local
  """

  @doc """
  Verify format (fields and values) and signature from a transaction document

  ## Example
      iex> Doc.Transaction.verif(%{
      ...> "currency" => "g1",
      ...> "blockstamp" => "107702-0000017CDBE974DC9A46B89EE7DC2BEE4017C43A005359E0853026C21FB6A084",
      ...> "locktime" => 0,
      ...> "issuers" => ["Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC"],
      ...> "inputs" => [
      ...>   "1002:0:D:Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC:104937",
      ...>   "1002:0:D:Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC:105214"
      ...> ],
      ...> "unlocks" => ["0:SIG(0)", "1:SIG(0)"],
      ...> "outputs" => ["2004:0:SIG(DTgQ97AuJ8UgVXcxmNtULAs8Fg1kKC1Wr9SAS96Br9NG)"],
      ...> "comment" => "c est pour 2 mois d adhesion ressourcerie",
      ...> "signatures" => [
      ...>   "lnpuFsIymgz7qhKF/GsZ3n3W8ZauAAfWmT4W0iJQBLKJK2GFkesLWeMj/+GBfjD6kdkjreg9M6VfkwIZH+hCCQ=="
      ...> ]
      ...> })
      true
  """
  def verif(
        %{
          "currency" => _currency,
          "locktime" => _locktime,
          "blockstamp" => _blockstamp,
          "issuers" => _issuers,
          "inputs" => _inputs,
          "outputs" => _outputs,
          "unlocks" => _unlocks,
          "signatures" => _signatures
        } = transaction
      ) do
    cond do
      check_values(transaction) ->
        transaction = enforce_keys(transaction)
        raw_transaction = raw_format(transaction)

        0..(length(transaction["issuers"]) - 1)
        |> Enum.all?(fn index ->
          issuer = transaction["issuers"] |> Enum.at(index)
          signature = transaction["signatures"] |> Enum.at(index)

          Crypto.verify_digital_signature(
            signature,
            raw_transaction,
            issuer
          )
        end)

      true ->
        false
    end
  end

  def verif(_), do: false

  @doc """
  Compute the hash of a transaction

  ## Example
      iex> Doc.Transaction.hash(%{
      ...> "blockstamp" => "506140-00000006174C9FD4F4088D1CD830A1B372865977DBA7ED834AE94C6767EA42C6",
      ...> "comment" => "",
      ...> "currency" => "g1",
      ...> "inputs" => ["7500:0:T:05B64E84A7F6DECB91B8FAAE287676B121D9C3D3CCFD7EF57FD6F91B4B3E548C:1"],
      ...> "issuers" => ["7HzhXCA9Cp3JQE2GpY6JJwv89fJi6FQSBtDsq657jpj3"],
      ...> "locktime" => 0,
      ...> "outputs" => [
      ...>   "2500:0:SIG(6SMqmUyJ7zCzht5twiQg6ePLiQ6HfLd3shhihMbAq5av)",
      ...>   "5000:0:SIG(7HzhXCA9Cp3JQE2GpY6JJwv89fJi6FQSBtDsq657jpj3)"
      ...> ],
      ...> "signatures" => [
      ...>   "/FC7D8TMkDuUn+0MNKSbFM/4x2dIWo/cQkGIj4KKTHjcm2F900P/nC9YyWvNUQLrhwD5lIZ6v8iSGyFAVFiVAw=="
      ...> ],
      ...> "unlocks" => ["0:SIG(0)"],
      ...> "version" => 10
      ...> })
      "BB62BC1515CC1BE35993CC05EA8702A358763C3315DCE321C4E9D5B60E2A1D6B"
  """
  def hash(transaction) do
    :crypto.hash(:sha256, raw_format(transaction, true))
    |> Base.encode16()
    |> String.upcase()
  end

  defp regex_exact_match(regex, string) do
    Regex.compile!("^#{regex}$")
    |> Regex.match?(string)
  end

  defp check_values(transaction) do
    enforced_keys_transaction = enforce_keys(transaction)

    (transaction["version"] == nil or transaction["version"] == 10) and
      regex_exact_match(Constants.block_uid_regexp(), enforced_keys_transaction["blockstamp"]) and
      regex_exact_match(Constants.currency_regexp(), enforced_keys_transaction["currency"]) and
      regex_exact_match(
        Constants.integer_regexp(),
        enforced_keys_transaction["locktime"] |> to_string
      ) and
      Enum.all?(enforced_keys_transaction["issuers"], fn issuer ->
        regex_exact_match(Constants.pubkey_regexp(), issuer)
      end) and
      Enum.all?(enforced_keys_transaction["inputs"], fn input ->
        regex_exact_match(Constants.transaction_input_regexp(), input)
      end) and
      Enum.all?(enforced_keys_transaction["unlocks"], fn unlock ->
        regex_exact_match(Constants.transaction_unlock_regexp(), unlock)
      end) and
      Enum.all?(enforced_keys_transaction["outputs"], fn output ->
        regex_exact_match(Constants.transaction_output_regexp(), output)
      end) and
      regex_exact_match(
        Constants.transaction_comment_regexp(),
        enforced_keys_transaction["comment"]
      ) and
      Enum.all?(enforced_keys_transaction["signatures"], fn signature ->
        regex_exact_match(Constants.signature_regexp(), signature)
      end)
  end

  defp enforce_keys(transaction) do
    transaction
    |> Map.put_new("version", @default_document_version)
    |> Map.put_new("comment", "")
  end

  defp add_list_entries(raw, list) do
    Enum.reduce(list, raw, fn elt, raw ->
      raw <> "#{elt}\n"
    end)
  end

  defp add_line(raw, line) do
    raw <> "#{line}\n"
  end

  defp raw_format(transaction, signature \\ false) do
    # Convert to the raw format of DUBP
    """
    Version: #{transaction["version"]}
    Type: Transaction
    Currency: #{transaction["currency"]}
    Blockstamp: #{transaction["blockstamp"]}
    Locktime: #{transaction["locktime"]}
    """
    |> add_line("Issuers:")
    |> add_list_entries(transaction["issuers"])
    |> add_line("Inputs:")
    |> add_list_entries(transaction["inputs"])
    |> add_line("Unlocks:")
    |> add_list_entries(transaction["unlocks"])
    |> add_line("Outputs:")
    |> add_list_entries(transaction["outputs"])
    |> add_line("Comment: #{transaction["comment"]}")
    |> then(fn raw_without_signatures ->
      if signature,
        do: raw_without_signatures |> add_list_entries(transaction["signatures"]),
        else: raw_without_signatures
    end)
  end
end
