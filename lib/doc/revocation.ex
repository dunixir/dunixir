defmodule Doc.Revocation do
  @default_document_version 10
  @moduledoc """
  to factorize with functions from Block.Validation.Local
  """

  @doc """
  Verify format (fields and values) and signature from a revocation document

  ## Example
      iex> Doc.Revocation.verif(%{
      ...> "currency" => "g1",
      ...> "pubkey" => "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
      ...> "idty_uid" => "tic",
      ...> "idty_buid" => "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
      ...> "idty_sig" =>
      ...>   "1eubHHbuNfilHMM0G2bI30iZzebQ2cQ1PC7uPAw08FGMMmQCRerlF/3pc4sAcsnexsxBseA/3lY03KlONqJBAg==",
      ...> "revocation" =>
      ...>   "XXOgI++6qpY9O31ml/FcfbXCE6aixIrgkT5jL7kBle3YOMr+8wrp7Rt+z9hDVjrNfYX2gpeJsuMNfG4T/fzVDQ=="
      ...> })
      true
  """
  def verif(
        %{
          "currency" => _currency,
          "revocation" => _revocation
        } = revocation
      )
      when (is_map_key(revocation, "pubkey") or is_map_key(revocation, "issuer")) and
             (is_map_key(revocation, "idty_uid") or
                is_map_key(revocation, "uid")) and
             (is_map_key(revocation, "idty_buid") or
                is_map_key(revocation, "buid")) and
             (is_map_key(revocation, "idty_sig") or is_map_key(revocation, "sig")) do
    cond do
      check_values(revocation) ->
        revocation = enforce_keys(revocation)

        Crypto.verify_digital_signature(
          revocation["revocation"],
          raw_format(revocation),
          revocation["pubkey"]
        )

      true ->
        false
    end
  end

  def verif(_), do: false

  defp regex_exact_match(regex, string) do
    Regex.compile!("^#{regex}$")
    |> Regex.match?(string)
  end

  defp check_values(revocation) do
    enforced_keys_revocation = enforce_keys(revocation)

    (revocation["version"] == nil or revocation["version"] == 10) and
      regex_exact_match(Constants.currency_regexp(), enforced_keys_revocation["currency"]) and
      regex_exact_match(Constants.pubkey_regexp(), enforced_keys_revocation["pubkey"]) and
      regex_exact_match(Constants.uid_regexp(), enforced_keys_revocation["idty_uid"]) and
      regex_exact_match(Constants.block_uid_regexp(), enforced_keys_revocation["idty_buid"]) and
      regex_exact_match(Constants.signature_regexp(), enforced_keys_revocation["idty_sig"]) and
      regex_exact_match(Constants.signature_regexp(), enforced_keys_revocation["revocation"])
  end

  defp enforce_keys(revocation) do
    revocation
    |> Map.put_new("version", revocation["version"] || @default_document_version)
    |> Map.put_new(
      "pubkey",
      revocation["issuer"]
    )
    |> Map.put_new("idty_uid", revocation["uid"])
    |> Map.put_new("idty_buid", revocation["buid"])
    |> Map.put_new("idty_sig", revocation["sig"])
  end

  defp raw_format(revocation) do
    # Convert to the raw format of DUBP
    """
    Version: #{revocation["version"]}
    Type: Revocation
    Currency: #{revocation["currency"]}
    Issuer: #{revocation["pubkey"]}
    IdtyUniqueID: #{revocation["idty_uid"]}
    IdtyTimestamp: #{revocation["idty_buid"]}
    IdtySignature: #{revocation["idty_sig"]}
    """
  end
end
