defmodule Doc.Identity do
  @default_document_version 10
  @doc """
  Verify format (fields and values) and signature from an identity document

  ## Example
      iex> Doc.Identity.verif(%{
      ...> "buid" => "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
      ...> "currency" => "duniter_unit_test_currency",
      ...> "pubkey" => "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
      ...> "sig" => "1eubHHbuNfilHMM0G2bI30iZzebQ2cQ1PC7uPAw08FGMMmQCRerlF/3pc4sAcsnexsxBseA/3lY03KlONqJBAg==",
      ...> "uid" => "tic",
      ...> "version" => 10
      ...> })
      true
  """
  def verif(
        %{
          "currency" => _currency,
          "uid" => _uid
        } = identity
      )
      when (is_map_key(identity, "issuer") or is_map_key(identity, "pubkey") or
              is_map_key(identity, "pub")) and
             (is_map_key(identity, "signature") or is_map_key(identity, "sig")) and
             (is_map_key(identity, "buid") or is_map_key(identity, "blockstamp")) do
    cond do
      check_values(identity) ->
        identity = enforce_keys(identity)

        Crypto.verify_digital_signature(
          identity["sig"],
          raw_format(identity),
          identity["pubkey"]
        )

      true ->
        false
    end
  end

  def verif(_), do: false

  @doc """
  Verify identities found inside a block in the context of the local validation of a block

  ## Example
      iex> block = %{
      ...>    "currency" => "duniter_unit_test_currency",
      ...>    "identities" => [
      ...>      "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV:1eubHHbuNfilHMM0G2bI30iZzebQ2cQ1PC7uPAw08FGMMmQCRerlF/3pc4sAcsnexsxBseA/3lY03KlONqJBAg==:0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855:tic",
      ...>      "DKpQPUL4ckzXYdnDRvCRKAm1gNvSdmAXnTrJZ7LvM5Qo:lcekuS0eP2dpFL99imJcwvDAwx49diiDMkG8Lj7FLkC/6IJ0tgNjUzCIZgMGi7bL5tODRiWi9B49UMXb8b3MAw==:0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855:toc"
      ...>    ]
      ...>  }
      iex> Doc.Identity.local_validation(block["identities"], block["currency"])
      true
  """
  def local_validation(identities, currency) do
    case length(identities) do
      0 ->
        true

      _ ->
        identities
        |> Enum.all?(fn inline_identity ->
          inline_identity
          |> to_map(currency)
          |> verif
        end)
    end
  end

  defp to_map(inline_identity, currency) do
    [pubkey, sig, buid, uid] = String.split(inline_identity, ":")

    %{
      "version" => @default_document_version,
      "currency" => currency,
      "pubkey" => pubkey,
      "sig" => sig,
      "buid" => buid,
      "uid" => uid
    }
  end

  defp regex_exact_match(regex, string) do
    Regex.compile!("^#{regex}$")
    |> Regex.match?(string)
  end

  defp check_values(identity) do
    enforced_keys_identity = enforce_keys(identity)

    (identity["version"] == nil or identity["version"] == 10) and
      regex_exact_match(Constants.currency_regexp(), enforced_keys_identity["currency"]) and
      regex_exact_match(Constants.pubkey_regexp(), enforced_keys_identity["pubkey"]) and
      regex_exact_match(Constants.uid_regexp(), enforced_keys_identity["uid"]) and
      regex_exact_match(Constants.block_uid_regexp(), enforced_keys_identity["buid"]) and
      regex_exact_match(Constants.signature_regexp(), enforced_keys_identity["sig"])
  end

  defp enforce_keys(identity) do
    identity
    |> Map.put_new("version", @default_document_version)
    |> Map.put_new("pubkey", identity["issuer"] || identity["pub"])
    |> Map.put_new("sig", identity["signature"])
    |> Map.put_new("buid", identity["blockstamp"])
  end

  defp raw_format(identity) do
    # Convert to the raw format of DUBP
    """
    Version: #{identity["version"]}
    Type: Identity
    Currency: #{identity["currency"]}
    Issuer: #{identity["pubkey"]}
    UniqueID: #{identity["uid"]}
    Timestamp: #{identity["buid"]}
    """
  end
end
