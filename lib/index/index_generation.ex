defmodule Index.Generation do
  @moduledoc """
  GENERAL BEHAVIOR

   * for each newcomer: 2 indexes (1 iindex CREATE, 1 mindex CREATE)
   * for each comeback: 2 indexes (1 iindex UPDATE, 1 mindex UPDATE)
   * for each renewer:  1 index   (                 1 mindex UPDATE)
   * for each leaver:   1 index   (                 1 mindex UPDATE)
   * for each revoked:  1 index   (                 1 mindex UPDATE)
   * for each excluded: 1 indexes (1 iindex UPDATE)

   * for each certification: 1 index (1 cindex CREATE)

   * for each tx output: 1 index (1 sindex CREATE)
   * for each tx input:  1 index (1 sindex UPDATE)
  """
  require Logger

  @doc """
  Add a new identity
  Create iindex and mindex
  ## Parameters
    - blockstamp
    - local_iindex
    - local_mindex

  """
  def add_identity(blockstamp, local_iindex, local_mindex, %{
        "pub" => pub,
        "block_uid" => block_uid,
        "user_id" => user_id
      }) do
    :ok =
      Local.IIndex.insert(local_iindex, %{
        op: "CREATE",
        uid: user_id,
        pub: pub,
        created_on: block_uid,
        written_on: blockstamp,
        member: true,
        wasMember: true,
        kick: false
      })

    :ok =
      Local.MIndex.insert(local_mindex, %{
        op: "CREATE",
        pub: pub,
        created_on: block_uid,
        written_on: blockstamp,
        expired_on: 0,
        expires_on: Constants.medianTime() + Constants.msValidity(),
        revokes_on: Constants.medianTime() + Constants.msValidity() * 2,
        chainable_on: Constants.medianTime() + Constants.msPeriod(),
        type: "JOIN",
        revocation: nil,
        revoked_on: nil,
        leaving: false
      })

    :ok
  end

  @doc """
  add a  comeback
  Update : iindex and mindex
  ## Parameters
    - blockstamp
    - local_iindex
    - local_mindex

  """
  def add_joiner(blockstamp, local_iindex, local_mindex, %{
        "pub" => pub,
        "m_block_uid" => m_block_uid,
        "i_block_uid" => _i_block_uid,
        "user_id" => _user_id
      }) do
    match_create = !(:ets.match(local_iindex, {:"$1", %{op: "CREATE", pub: pub}}) == [])

    case match_create do
      false ->
        :ok =
          Local.IIndex.insert(local_iindex, %{
            op: "UPDATE",
            pub: pub,
            uid: nil,
            created_on: nil,
            written_on: blockstamp,
            member: true,
            wasMember: nil,
            kick: nil
          })

        :ok =
          Local.MIndex.insert(local_mindex, %{
            op: "UPDATE",
            pub: pub,
            # ambiguité de la spec (m ou i ?)
            created_on: m_block_uid,
            written_on: blockstamp,
            expired_on: 0,
            expires_on: Constants.medianTime() + Constants.msValidity(),
            revokes_on: Constants.medianTime() + Constants.msValidity() * 2,
            chainable_on: Constants.medianTime() + Constants.msPeriod(),
            type: "JOIN",
            revocation: nil,
            revoked_on: nil,
            leaving: nil
          })

        :ok

      _ ->
        :ok
    end
  end

  @doc """
  add a renewer
  Update mindex
  ## Parameters
    - blockstamp
    - local_mindex
  """
  def add_active(blockstamp, local_mindex, %{
        "pub" => pub,
        "m_block_uid" => m_block_uid,
        "i_block_uid" => _i_block_uid,
        "user_id" => _user_id
      }) do
    :ok =
      Local.MIndex.insert(local_mindex, %{
        op: "UPDATE",
        pub: pub,
        # ambiguité de la spec (m ou i ?)
        created_on: m_block_uid,
        written_on: blockstamp,
        expired_on: nil,
        expires_on: Constants.medianTime() + Constants.msValidity(),
        revokes_on: Constants.medianTime() + Constants.msValidity() * 2,
        chainable_on: Constants.medianTime() + Constants.msPeriod(),
        type: "RENEW",
        revoked_on: nil,
        revocation: nil,
        leaving: nil
      })

    :ok
  end

  @doc """
  add a leaver
  Update mindex
  ## Parameters
    - blockstamp
    - local_mindex
  """
  def add_leaver(blockstamp, local_mindex, %{
        "pub" => pub,
        "m_block_uid" => m_block_uid,
        "i_block_uid" => _i_block_uid,
        "user_id" => _user_id
      }) do
    :ok =
      Local.MIndex.insert(local_mindex, %{
        op: "UPDATE",
        pub: pub,
        # ambiguité de la spec (m ou i ?)
        created_on: m_block_uid,
        written_on: blockstamp,
        expired_on: nil,
        expires_on: nil,
        revokes_on: nil,
        chainable_on: nil,
        type: "LEAVE",
        revoked_on: nil,
        revocation: nil,
        leaving: true
      })

    :ok
  end

  @doc """
  add a revoked
  Update mindex
  ## Parameters
    - blockstamp
    - local_mindex
  """
  def add_revoked(blockstamp, local_mindex, %{"pub" => pub, "sig" => sig}) do
    :ok =
      Local.MIndex.insert(local_mindex, %{
        op: "UPDATE",
        pub: pub,
        # ambiguité de la spec (BLOCK_UID???)
        created_on: blockstamp,
        written_on: blockstamp,
        expired_on: nil,
        expires_on: nil,
        revokes_on: nil,
        chainable_on: nil,
        type: "REV",
        revoked_on: Constants.medianTime(),
        revocation: sig,
        leaving: false
      })

    :ok
  end

  @doc """
  add a excluded
  Update iindex
  ## Parameters
    - blockstamp
    - local_iindex
  """
  def add_excluded(blockstamp, local_iindex, %{"pub" => pub}) do
    :ok =
      Local.IIndex.insert(local_iindex, %{
        op: "UPDATE",
        uid: nil,
        pub: pub,
        created_on: nil,
        written_on: blockstamp,
        member: false,
        wasMember: nil,
        kick: false
      })

    :ok
  end

  @doc """
  add a certification
  Create cindex
  ## Parameters
    - blockstamp
    - local_cindex
  """
  def add_certification(blockstamp, local_cindex, %{
        "from" => from,
        "to" => to,
        "block_id" => block_id,
        "sig" => sig
      }) do
    :ok =
      Local.CIndex.insert(local_cindex, %{
        op: "CREATE",
        issuer: from,
        receiver: to,
        created_on: block_id,
        written_on: blockstamp,
        sig: sig,
        expires_on: Constants.medianTime() + Constants.sigValidity(),
        expired_on: 0,
        chainable_on: Constants.medianTime() + Constants.sigPeriod(),
        replayable_on: Constants.medianTime() + Constants.sigReplay()
      })

    :ok
  end

  @doc """
  add a tx input
  Update sindex
  ## Parameters
    - blockstamp
    - local_sindex
  """
  def add_tx_input(blockstamp, local_sindex, %{
        "tx_hash" => tx_hash,
        "input_identifier" => input_identifier,
        "input_index" => input_index,
        "tx_blockstamp" => tx_blockstamp,
        "input_amount" => input_amount,
        "input_base" => input_base
      }) do
    :ok =
      Local.SIndex.insert(local_sindex, %{
        op: "UPDATE",
        tx: tx_hash,
        identifier: input_identifier,
        pos: input_index,
        written_on: blockstamp,
        created_on: tx_blockstamp,
        written_time: nil,
        amount: input_amount,
        base: input_base,
        locktime: nil,
        conditions: nil,
        consumed: true
      })

    :ok
  end

  @doc """
  add a tx output
  Create sindex
  ## Parameters
    - blockstamp
    - local_sindex
  """
  def add_tx_output(blockstamp, local_sindex, %{
        "tx_hash" => tx_hash,
        "output_index" => output_index,
        "output_amount" => output_amount,
        "output_base" => output_base,
        "locktime" => locktime,
        "conditions" => conditions
      }) do
    :ok =
      Local.SIndex.insert(local_sindex, %{
        op: "CREATE",
        tx: tx_hash,
        identifier: tx_hash,
        pos: output_index,
        written_on: blockstamp,
        created_on: nil,
        written_time: Constants.medianTime(),
        amount: output_amount,
        base: output_base,
        locktime: locktime,
        conditions: conditions,
        consumed: false
      })

    :ok
  end

  @doc """
  Generate the [local indexes](https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#index-generation) of a block and verify some unicity constraints from the [index rules](https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#index-rules)
  """
  def process(block) do
    try do
      # should we use the set implementation ???
      local_iindex = :ets.new(:local_iindex, [:set, :protected])
      local_mindex = :ets.new(:local_mindex, [:set, :protected])
      local_cindex = :ets.new(:local_cindex, [:set, :protected])
      local_sindex = :ets.new(:local_sindex, [:set, :protected])

      blockstamp = "#{block["number"]}-#{block["hash"]}"

      block["identities"]
      |> Enum.map(fn inline_identity ->
        [pub, _sig, block_uid, user_id] = String.split(inline_identity, ":")

        add_identity(blockstamp, local_iindex, local_mindex, %{
          "pub" => pub,
          "block_uid" => block_uid,
          "user_id" => user_id
        })
      end)

      block["joiners"]
      |> Enum.map(fn inline_membership ->
        [pub, _sig, m_block_uid, i_block_uid, user_id] = String.split(inline_membership, ":")

        add_joiner(blockstamp, local_iindex, local_mindex, %{
          "pub" => pub,
          "m_block_uid" => m_block_uid,
          "i_block_uid" => i_block_uid,
          "user_id" => user_id
        })
      end)

      block["actives"]
      |> Enum.map(fn inline_membership ->
        [pub, _sig, m_block_uid, i_block_uid, user_id] = String.split(inline_membership, ":")

        add_active(blockstamp, local_mindex, %{
          "pub" => pub,
          "m_block_uid" => m_block_uid,
          "i_block_uid" => i_block_uid,
          "user_id" => user_id
        })
      end)

      block["leavers"]
      |> Enum.map(fn inline_membership ->
        [pub, _sig, m_block_uid, i_block_uid, user_id] = String.split(inline_membership, ":")

        add_leaver(blockstamp, local_mindex, %{
          "pub" => pub,
          "m_block_uid" => m_block_uid,
          "i_block_uid" => i_block_uid,
          "user_id" => user_id
        })
      end)

      block["revoked"]
      |> Enum.map(fn inline_revocation ->
        [pub, sig] = String.split(inline_revocation, ":")

        add_revoked(blockstamp, local_mindex, %{"pub" => pub, "sig" => sig})
      end)

      block["excluded"]
      |> Enum.map(fn pub ->
        add_excluded(blockstamp, local_iindex, %{"pub" => pub})
      end)

      block["certifications"]
      |> Enum.map(fn inline_certification ->
        [from, to, block_id, sig] = String.split(inline_certification, ":")

        add_certification(blockstamp, local_cindex, %{
          "from" => from,
          "to" => to,
          "block_id" => block_id,
          "sig" => sig
        })
      end)

      block["transactions"]
      |> Enum.map(fn transaction ->
        tx_hash = Doc.Transaction.hash(transaction)

        transaction["inputs"]
        |> Enum.with_index()
        |> Enum.map(fn {transaction_input, input_index} ->
          [input_amount, input_base, _type, input_identifier, _pos] =
            String.split(transaction_input, ":")

          add_tx_input(blockstamp, local_sindex, %{
            "tx_hash" => tx_hash,
            "input_identifier" => input_identifier,
            "input_index" => input_index,
            "tx_blockstamp" => transaction["blockstamp"],
            "input_amount" => input_amount,
            "input_base" => input_base
          })
        end)

        transaction["outputs"]
        |> Enum.with_index()
        |> Enum.map(fn {transaction_output, output_index} ->
          [output_amount, output_base, conditions] = String.split(transaction_output, ":")

          add_tx_output(blockstamp, local_sindex, %{
            "tx_hash" => tx_hash,
            "output_index" => output_index,
            "output_amount" => output_amount,
            "output_base" => output_base,
            "locktime" => transaction["locktime"],
            "conditions" => conditions
          })
        end)
      end)

      {:ok, [local_iindex, local_mindex, local_cindex, local_sindex]}
    rescue
      # unicity constraints are checked at insertion, and throws when they are not met
      e ->
        Logger.error(Exception.format(:error, e, __STACKTRACE__))
        {:error, Exception.format(:error, e, __STACKTRACE__)}
    end
  end
end
