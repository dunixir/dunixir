defmodule IndexRules do
  @moduledoc """
  Module to check [index rules](https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md#index-rules) except unicity constraints.
  """
  defp checkIdentitiesMatchJoin(local_iindex, local_mindex) do
    # Each local IINDEX op = 'CREATE' operation must match a single local MINDEX op = 'CREATE', pub = PUBLIC_KEY operation.
    :ets.match(local_iindex, {:_, %{op: "CREATE", pub: :"$1"}})
    |> Enum.all?(fn [pubkey] ->
      length(:ets.match(local_mindex, {:"$1", %{op: "CREATE", pub: pubkey}})) == 1
    end)
  end

  defp checkRevokedAreExcluded(local_iindex, local_mindex) do
    # Each local MINDEX ̀op = 'UPDATE', revoked_on != null operations must match a single local IINDEX op = 'UPDATE', pub = PUBLIC_KEY, member = false operation.
    :ets.match(local_mindex, {:_, %{op: "UPDATE", pub: :"$1", revoked_on: :"$2"}})
    |> Enum.all?(fn [pubkey, revoked_on] ->
      if is_nil(revoked_on) do
        true
      else
        length(:ets.match(local_iindex, {:"$1", %{op: "UPDATE", pub: pubkey, member: false}})) ==
          1
      end
    end)
  end

  defp checkCertificationIsntForLeaverOrExcluded(local_iindex, local_mindex, local_cindex) do
    # The local CINDEX must not match a MINDEX operation on PUBLIC_KEY = PUBKEY_FROM, member = false or PUBLIC_KEY = PUBKEY_FROM, leaving = true
    :ets.match(local_cindex, {:_, %{issuer: :"$1"}})
    |> Enum.all?(fn [pubkey_from] ->
      length(:ets.match(local_iindex, {:"$1", %{pub: pubkey_from, member: false}})) == 0 and
        length(:ets.match(local_mindex, {:"$1", %{pub: pubkey_from, leaving: true}})) == 0
    end)
  end

  defp checkTxAmountsValidity(local_sindex) do
    # Rule: For each OutputBase:
    # if BaseDelta > 0, then it must be inferior or equal to the sum of all preceding BaseDelta
    # Rule: The sum of all inputs in CommonBase must equal the sum of all outputs in CommonBase
    if :ets.info(local_sindex)[:size] > 0 do
      commonBase =
        :ets.match(local_sindex, {:_, %{base: :"$1"}})
        |> IO.inspect()
        |> Enum.map(fn [base] -> String.to_integer(base) end)
        |> Enum.min()

      [inputSumCommonBase, outputSumCommonBase] =
        :ets.match(local_sindex, {:_, %{amount: :"$1", base: :"$2", op: :"$3"}})
        |> Enum.reduce([0, 0], fn
          [amount, base, op], [inputSumCommonBase, outputSumCommonBase] ->
            amountCommonBase =
              String.to_integer(amount) * Integer.pow(10, String.to_integer(base) - commonBase)

            if op == "CREATE" do
              # output
              [inputSumCommonBase, outputSumCommonBase + amountCommonBase]
            else
              # input
              [inputSumCommonBase + amountCommonBase, outputSumCommonBase]
            end
        end)

      if inputSumCommonBase == outputSumCommonBase do
        allBases =
          :ets.match(local_sindex, {:_, %{base: :"$1"}})
          |> Enum.map(fn [base] -> String.to_integer(base) end)
          |> Enum.sort()
          |> Enum.uniq()

        allBases
        |> Enum.reduce_while(0, fn base, sumBaseDelta ->
          [inputBaseSum, outputBaseSum] =
            :ets.match(local_sindex, {:_, %{amount: :"$1", base: "#{base}", op: :"$2"}})
            |> Enum.reduce([0, 0], fn
              [amount, op], [inputBaseSum, outputBaseSum] ->
                if op == "CREATE" do
                  # output
                  [inputBaseSum, outputBaseSum + String.to_integer(amount)]
                else
                  # input
                  [inputBaseSum + String.to_integer(amount), outputBaseSum]
                end
            end)

          baseDelta = outputBaseSum - inputBaseSum

          if baseDelta > 0 and baseDelta > -sumBaseDelta do
            # Functionally: also, we cannot convert a superiod unit base into a lower one.
            {:halt, false}
          else
            {:cont, sumBaseDelta + baseDelta}
          end
        end)
        # enforce boolean return
        |> then(fn check_base_delta -> if check_base_delta == false, do: false, else: true end)
      else
        false
      end
    else
      true
    end
  end

  defp getTransactionDepth(local_sindex, txHash, local_depth) do
    # inputs
    :ets.match(local_sindex, {:_, %{op: "UPDATE", tx: txHash, identifier: :"$1", pos: :"$2"}})
    |> Enum.reduce(local_depth, fn [identifier, pos], depth ->
      consumed_tx_hash =
        :ets.match(
          local_sindex,
          {:_, %{op: "CREATE", identifier: identifier, pos: pos, tx: :"$1"}}
        )
        |> Enum.at(0)

      if !is_nil(consumed_tx_hash) do
        if local_depth < 5 do
          max(depth, getTransactionDepth(local_sindex, consumed_tx_hash, local_depth + 1))
        else
          depth + 1
        end
      else
        depth
      end
    end)
  end

  defp checkMaxTransactionChainingDepth(local_sindex) do
    if :ets.info(local_sindex)[:size] > 0 do
      :ets.match(local_sindex, {:_, %{tx: :"$1"}})
      |> Enum.map(fn [txHash] -> txHash end)
      |> Enum.uniq()
      |> Enum.map(fn txHash -> getTransactionDepth(local_sindex, txHash, 0) end)
      |> Enum.max() <= 5
    else
      true
    end
  end

  def check(local_iindex, local_mindex, local_cindex, local_sindex) do
    checkIdentitiesMatchJoin(local_iindex, local_mindex) and
      checkRevokedAreExcluded(local_iindex, local_mindex) and
      checkCertificationIsntForLeaverOrExcluded(local_iindex, local_mindex, local_cindex) and
      checkTxAmountsValidity(local_sindex) and
      checkMaxTransactionChainingDepth(local_sindex)
  end
end
