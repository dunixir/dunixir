defmodule Index.Augmentation do
  defmodule IndexUtility do
    @moduledoc """
    Utility functions
    """

    @doc """
    Return a list of entries that were indexed between two indexes from global_bindex.

    ## Parameters

      - start_number: Integer start index
      - end_number: Integer end index
      - global_bindex: Tab to be searched
    """
    @spec range(Integer, Integer, Tab) :: list
    def range(start_number, end_number, global_bindex) do
      # records are order by number DESC
      bindexes_records =
        DB.match(global_bindex, {:_, :"$1"})
        |> List.flatten()
        |> Enum.sort(&(&1.number > &2.number))

      end_number = min(end_number, length(bindexes_records))

      # theRange is a list contains bindex's value, its format likes [%{issuer: "a", issuersFrame: 3, number: 2},%{issuer: "b", issuersFrame: 2, number: 1}]
      if start_number == 1 do
        Enum.slice(bindexes_records, -end_number..-1)
        # Enum.reverse(the_range)
      else
        Enum.slice(bindexes_records, -end_number..(-start_number + 1))
        # Enum.reverse(the_range)
      end
    end

    @doc """
    Return the first entry of a Tab.

    ## Parameters

      - start_number: Tab to be searched
    """
    @spec find_first_local_bindex_entry(Tab) :: [tuple]
    def find_first_local_bindex_entry(local_bindex) do
      key = :ets.first(local_bindex)
      :ets.lookup(local_bindex, key)
    end

    @doc """
    Merge a list of entries so that it returns only one entry.

    ## Parameters

      - records: List
    """
    @spec duniter_reduce(List) :: Map
    def duniter_reduce(records) do
      Enum.reduce(
        records,
        %{},
        &Map.merge(&1, &2, fn _k, v1, v2 ->
          case {v1, v2} do
            {nil, nil} -> nil
            {nil, value} -> value
            {value, nil} -> value
            {value, _other} -> value
          end
        end)
      )
    end
  end

  defmodule BIndex do
    @moduledoc """
    Validation rules that apply to the Duniter BINDEX (Block index).
    See https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md
    """

    def conf, do: ConfDTO.mockConfDTO()

    @doc """
    Validation rule BR_G01
    HEAD.number
      If HEAD~1 is defined:
        HEAD.number = HEAD~1.number + 1
      Else:
        HEAD.number = 0
    """
    def number(local_bindex, global_bindex \\ 'global_bindex') do
      key = :ets.first(local_bindex)
      [{key, head}] = :ets.lookup(local_bindex, key)

      case DB.lookup(global_bindex, :last_block) do
        [] ->
          :ets.insert(local_bindex, {key, Map.merge(head, %{number: 0})})
          DB.insert(global_bindex, :last_block, 0)

        _ ->
          [{:last_block, last_block}] = DB.lookup(global_bindex, :last_block)
          :ets.insert(local_bindex, {key, Map.merge(head, %{number: last_block + 1})})
          DB.insert(global_bindex, :last_block, last_block + 1)
      end
    end

    @doc """
    Validation rule BR_G02
    HEAD.previousHash
      If HEAD.number > 0:
        HEAD.previousHash = HEAD~1.hash
      Else:
        HEAD.previousHash = null
    """
    def previousHash(local_bindex, global_bindex \\ 'global_bindex') do
      key = :ets.first(local_bindex)
      [{key, head}] = :ets.lookup(local_bindex, key)

      if head.number > 0 do
        [[previousHash]] =
          DB.match(global_bindex, {:_, %{number: head.number - 1, hash: :"$1"}})

        :ets.insert(local_bindex, {key, Map.merge(head, %{previousHash: previousHash})})
      else
        :ets.insert(local_bindex, {key, Map.merge(head, %{previousHash: nil})})
      end
    end

    @doc """
    Validation rule BR_G03
    HEAD.previousIssuer
      If HEAD.number > 0:
        HEAD.previousIssuer = HEAD~1.issuer
      Else:
        HEAD.previousIssuer = null
    """
    def previousIssuer(local_bindex, global_bindex \\ 'global_bindex') do
      # get the head key
      key = :ets.first(local_bindex)
      # get the head content
      [{key, head}] = :ets.lookup(local_bindex, key)
      # recent bloc
      if head.number > 0 do
        # get head-1's issuer
        [[previousIssuer]] =
          DB.match(global_bindex, {:_, %{number: head.number - 1, issuer: :"$1"}})

        # add Previousissuer
        :ets.insert(local_bindex, {key, Map.merge(head, %{previousIssuer: previousIssuer})})
      else
        # PI is nil
        :ets.insert(local_bindex, {key, Map.merge(head, %{previousIssuer: nil})})
      end
    end

    @doc """
    Validation rule BR_G04
    HEAD.issuersCount
      If HEAD.number == 0:
        HEAD.issuersCount = 0
      Else:
        HEAD.issuersCount = COUNT(UNIQ((HEAD~1..<HEAD~1.issuersFrame>).issuer))
    """
    def issuersCount(local_bindex, global_bindex \\ 'global_bindex') do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)

      if head.number == 0 do
        :ets.insert(local_bindex, {key, Map.merge(head, %{issuersCount: 0})})
      else
        [[issuers_frame]] =
          DB.match(global_bindex, {:_, %{number: head.number - 1, issuersFrame: :"$1"}})

        bindex_records_in_range = IndexUtility.range(1, issuers_frame, global_bindex)

        uniq_issuers_in_range =
          Enum.map(bindex_records_in_range, fn x -> x.issuer end) |> Enum.uniq()

        :ets.insert(
          local_bindex,
          {key, Map.merge(head, %{issuersCount: Enum.count(uniq_issuers_in_range)})}
        )
      end
    end

    @doc """
    Validation rule BR_G05
    HEAD.issuersFrame
      If HEAD.number == 0:
        HEAD.issuersFrame = 1
      Else if HEAD~1.issuersFrameVar > 0:
        HEAD.issuersFrame = HEAD~1.issuersFrame + 1
      Else if HEAD~1.issuersFrameVar < 0:
        HEAD.issuersFrame = HEAD~1.issuersFrame - 1
      Else:
        HEAD.issuersFrame = HEAD~1.issuersFrame
    """
    def issuersFrame(local_bindex, global_bindex \\ 'global_bindex') do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)

      if head.number == 0 do
        :ets.insert(local_bindex, {key, Map.merge(head, %{issuersFrame: 1})})
      else
        [[head_1_id]] = DB.match(global_bindex, {:"$1", %{number: head.number - 1}})
        [{_key_1, head_1}] = DB.lookup(global_bindex, head_1_id)

        cond do
          head_1.issuersFrameVar > 0 ->
            :ets.insert(
              local_bindex,
              {key, Map.merge(head, %{issuersFrame: head_1.issuersFrame + 1})}
            )

          head_1.issuersFrameVar < 0 ->
            :ets.insert(
              local_bindex,
              {key, Map.merge(head, %{issuersFrame: head_1.issuersFrame - 1})}
            )

          true ->
            :ets.insert(
              local_bindex,
              {key, Map.merge(head, %{issuersFrame: head_1.issuersFrame})}
            )
        end
      end
    end

    @doc """
    Validation rule BR_G06
    HEAD.issuersFrameVar
      If HEAD.number == 0:
        HEAD.issuersFrameVar = 0
      Else if HEAD~1.issuersFrameVar > 0:
        HEAD.issuersFrameVar = HEAD~1.issuersFrameVar + 5*(HEAD.issuersCount - HEAD~1.issuersCount) - 1
      Else if HEAD~1.issuersFrameVar < 0:
        HEAD.issuersFrameVar = HEAD~1.issuersFrameVar + 5*(HEAD.issuersCount - HEAD~1.issuersCount) + 1
      Else:
        HEAD.issuersFrameVar = HEAD~1.issuersFrameVar + 5*(HEAD.issuersCount - HEAD~1.issuersCount)
    """
    def issuersFrameVar(local_bindex, global_bindex \\ 'global_bindex') do
      key = :ets.first(local_bindex)
      [{key, head}] = :ets.lookup(local_bindex, key)

      if head.number == 0 do
        :ets.insert(local_bindex, {key, Map.merge(head, %{issuersFrameVar: 0})})
      else
        [[head_1_id]] = DB.match(global_bindex, {:"$1", %{number: head.number - 1}})
        [{_key_1, head_1}] = DB.lookup(global_bindex, head_1_id)
        variation = head_1.issuersFrameVar + 5 * (head.issuersCount - head_1.issuersCount)

        case head_1.issuersFrameVar do
          x when x > 0 -> variation - 1
          x when x < 0 -> variation + 1
          0 -> variation
        end
        |> (&:ets.insert(local_bindex, {key, Map.merge(head, %{issuersFrameVar: &1})})).()
      end
    end

    @doc """
    Validation rule BR_G10
    HEAD.membersCount
      If HEAD.number == 0:
        HEAD.membersCount = COUNT(LOCAL_IINDEX[member=true])
      Else:
        HEAD.membersCount = HEAD~1.membersCount + COUNT(LOCAL_IINDEX[member=true]) - COUNT(LOCAL_IINDEX[member=false])
    """
    def membersCount(local_iindex, local_bindex, global_bindex \\ 'global_bindex') do
      key = :ets.first(local_bindex)
      [{key, head}] = :ets.lookup(local_bindex, key)

      if head.number == 0 do
        count = length(:ets.match(local_iindex, {:"$1", %{member: true}}))
        :ets.insert(local_bindex, {key, Map.merge(head, %{membersCount: count})})
      else
        [[last_block]] =
          DB.match(global_bindex, {head.number - 1, :"$1"})

        count =
          last_block.membersCount + length(:ets.match(local_iindex, {:"$1", %{member: true}})) -
            length(:ets.match(local_iindex, {:"$1", %{member: false}}))

        :ets.insert(local_bindex, {key, Map.merge(head, %{membersCount: count})})
      end
    end

    @doc """
    Validation rule BR_G11
    HEAD.udTime and HEAD.udReevalTime
      If HEAD.number == 0:
        HEAD.udTime = udTime0
      Else if HEAD~1.udTime <= HEAD.medianTime:
        HEAD.udTime = HEAD~1.udTime + dt
      Else:
        HEAD.udTime = HEAD~1.udTime
      EndIf

      If HEAD.number == 0:
        HEAD.udReevalTime = udReevalTime0
      Else if HEAD~1.udReevalTime <= HEAD.medianTime:
        HEAD.udReevalTime = HEAD~1.udReevalTime + dtReeval
      Else:
        HEAD.udReevalTime = HEAD~1.udReevalTime
      EndIf
    """
    def udTime(local_bindex, global_bindex \\ 'global_bindex') do
      # UD production
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)

      if head.number == 0 do
        :ets.insert(local_bindex, {key, Map.merge(head, %{udTime: conf().udTime0})})
      else
        [[head_1_id]] = DB.match(global_bindex, {:"$1", %{number: head.number - 1}})
        [{_key_1, head_1}] = DB.lookup(global_bindex, head_1_id)

        if head_1.udTime <= head.medianTime do
          :ets.insert(local_bindex, {key, Map.merge(head, %{udTime: head_1.udTime + conf().dt})})
        else
          :ets.insert(local_bindex, {key, Map.merge(head, %{udTime: head_1.udTime})})
        end
      end

      # update head
      [{key, head}] = :ets.lookup(local_bindex, key)

      if head.number == 0 do
        :ets.insert(local_bindex, {key, Map.merge(head, %{udReevalTime: conf().udReevalTime0})})
      else
        [[head_1_id]] = DB.match(global_bindex, {:"$1", %{number: head.number - 1}})
        [{_key_1, head_1}] = DB.lookup(global_bindex, head_1_id)

        if head_1.udReevalTime <= head.medianTime do
          :ets.insert(
            local_bindex,
            {key, Map.merge(head, %{udReevalTime: head_1.udReevalTime + conf().dtReeval})}
          )
        else
          :ets.insert(local_bindex, {key, Map.merge(head, %{udReevalTime: head_1.udReevalTime})})
        end
      end
    end

    @doc """
    Validation rule BR_G12
    HEAD.unitBase
      If HEAD.number == 0:
        HEAD.unitBase = 0
      Else:
        HEAD.unitBase = HEAD~1.unitBase
    """
    def unitBase(local_bindex, global_bindex) do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)

      if head.number == 0 do
        :ets.insert(local_bindex, {key, Map.merge(head, %{unitBase: 0})})
      else
        [[unitBase]] =
          DB.match(global_bindex, {:_, %{number: head.number - 1, unitBase: :"$1"}})

        :ets.insert(local_bindex, {key, Map.merge(head, %{unitBase: unitBase})})
      end
    end

    @doc """
    Validation rule BR_G13
    HEAD.dividend and HEAD.new_dividend
      If HEAD.number == 0:
        HEAD.dividend = ud0
      Else If HEAD.udReevalTime != HEAD~1.udReevalTime:
        HEAD.dividend = CEIL(HEAD_1.dividend + c² * CEIL(HEAD~1.massReeval / POW(10, HEAD~1.unitbase)) / HEAD.membersCount)
      Else:
        HEAD.dividend = HEAD~1.dividend
      EndIf

      If HEAD.number == 0:
        HEAD.new_dividend = null
      Else If HEAD.udTime != HEAD~1.udTime:
        HEAD.new_dividend = HEAD.dividend
      Else:
        HEAD.new_dividend = null
    """
    def dividend(global_bindex, local_bindex) do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)
      # UD re-evaluation
      if head.number == 0 do
        :ets.insert(local_bindex, {key, Map.merge(head, %{dividend: conf().ud0})})
      else
        [[head_1_id]] = DB.match(global_bindex, {:"$1", %{number: head.number - 1}})
        [{_key_1, head_1}] = DB.lookup(global_bindex, head_1_id)

        if head.udReevalTime != head_1.udReevalTime do
          previousUB = head_1.unitBase

          dividend =
            Float.ceil(
              head_1.dividend +
                :math.pow(conf().c, 2) * Float.ceil(head_1.massReeval / :math.pow(10, previousUB)) /
                  head.membersCount /
                  (conf().dtReeval / conf().dt)
            )

          :ets.insert(local_bindex, {key, Map.merge(head, %{dividend: dividend})})
        else
          :ets.insert(local_bindex, {key, Map.merge(head, %{dividend: head_1.dividend})})
        end
      end

      [{key, head}] = :ets.lookup(local_bindex, key)

      # UD creation
      if head.number == 0 do
        :ets.insert(local_bindex, {key, Map.merge(head, %{new_dividend: nil})})
      else
        [[head_1_id]] = DB.match(global_bindex, {:"$1", %{number: head.number - 1}})
        [{_key_1, head_1}] = DB.lookup(global_bindex, head_1_id)

        if head.udTime != head_1.udTime do
          :ets.insert(local_bindex, {key, Map.merge(head, %{new_dividend: head.dividend})})
        else
          :ets.insert(local_bindex, {key, Map.merge(head, %{new_dividend: nil})})
        end
      end
    end

    @doc """
    Validation rule BR_G14
    HEAD.dividend and HEAD.unitbase and HEAD.new_dividend
      If HEAD.dividend >= 1000000    :
        HEAD.dividend = CEIL(HEAD.dividend / 10)
        HEAD.new_dividend = HEAD.dividend
        HEAD.unitBase = HEAD.unitBase + 1
    """
    def unitBase(local_bindex) do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)

      if head.dividend >= :math.pow(10, Constants.nbDigitsUD()) do
        :ets.insert(
          local_bindex,
          {key, Map.merge(head, %{dividend: Float.ceil(head.dividend / 10)})}
        )

        [{key, head}] = :ets.lookup(local_bindex, key)
        :ets.insert(local_bindex, {key, Map.merge(head, %{new_dividend: head.dividend})})
        :ets.insert(local_bindex, {key, Map.merge(head, %{unitBase: head.unitBase + 1})})
      end
    end

    @doc """
    Validation rule BR_G49
    Version
      If HEAD.number > 0:
        HEAD.version == (HEAD~1.version OR HEAD~1.version + 1)
    """
    def version(local_bindex, global_bindex \\ 'global_bindex') do
      [{_key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)

      if head.number > 0 do
        [[previousVersion]] =
          DB.match(global_bindex, {:_, %{number: head.number - 1, version: :"$1"}})

        head.version == previousVersion || head.version == previousVersion + 1
      else
        true
      end
    end

    @doc """
    Validation rule BR_G50
    Block size
      If HEAD.number > 0:
        HEAD.size < MAX(500 ; CEIL(1.10 * HEAD.avgBlockSize))
    """
    def blockSize(local_bindex, global_bindex \\ 'global_bindex') do
      [{_key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)

      if head.number > 0 do
        head.size < max(500, ceil(1.10 * head.avgBlockSize))
      else
        true
      end
    end

    @doc """
    Validation rule BR_G51
    """
    def ruleNumber(block, local_bindex) do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)
      head.number == block.number
    end

    @doc """
    Validation rule BR_G52
    """
    def rulePreviousHash(block, local_bindex) do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)
      head.previousHash == block.previousHash
    end

    @doc """
    Validation rule BR_G53
    """
    def rulePreviousIssuer(block, local_bindex) do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)
      head.previousIssuer == block.previousIssuer
    end

    @doc """
    Validation rule BR_G54
    """
    def ruleIssuersCount(block, local_bindex) do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)
      head.issuersCount == block.issuersCount
    end

    @doc """
    Validation rule BR_G55
    """
    def ruleIssuersFrame(block, local_bindex) do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)
      head.issuersFrame == block.issuersFrame
    end

    @doc """
    Validation rule BR_G56
    """
    def ruleIssuersFrameVar(block, local_bindex) do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)
      head.issuersFrameVar == block.issuersFrameVar
    end

    @doc """
    Validation rule BR_G57
    """
    def ruleMedianTime(block, local_bindex) do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)
      head.medianTime == block.medianTime
    end

    @doc """
    Validation rule BR_G58
    """
    def ruleDividend(block, local_bindex) do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)
      head.new_dividend == block.dividend
    end

    @doc """
    Validation rule BR_G59
    """
    def ruleUnitBase(block, local_bindex) do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)
      head.unitBase == block.unitBase
    end

    @doc """
    Validation rule BR_G60
    """
    def ruleMembersCount(block, local_bindex) do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)
      head.membersCount == block.membersCount
    end

    @doc """
    Validation rule BR_G61
    """
    def rulePowMin(block, local_bindex) do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)

      if head.number > 0 do
        head.powMin == block.powMin
      else
        true
      end
    end

    @doc """
    Validation rule BR_G62
    Proof-of-work
    if:
      HEAD.hash starts with at least HEAD.powZeros zeros
      HEAD.hash's HEAD.powZeros + 1th character is:

      between [0-F] if HEAD.powRemainder = 0
      between [0-E] if HEAD.powRemainder = 1
      between [0-D] if HEAD.powRemainder = 2
      between [0-C] if HEAD.powRemainder = 3
      between [0-B] if HEAD.powRemainder = 4
      between [0-A] if HEAD.powRemainder = 5
      between [0-9] if HEAD.powRemainder = 6
      between [0-8] if HEAD.powRemainder = 7
      between [0-7] if HEAD.powRemainder = 8
      between [0-6] if HEAD.powRemainder = 9
      between [0-5] if HEAD.powRemainder = 10
      between [0-4] if HEAD.powRemainder = 11
      between [0-3] if HEAD.powRemainder = 12
      between [0-2] if HEAD.powRemainder = 13
      between [0-1] if HEAD.powRemainder = 14
    """
    def proofOfWork(local_bindex) do
      [{_key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)

      # checking the number of zeros at the start of head.hash
      if String.match?(head.hash, ~r/^0{#{head.powZeros}}/) do
        # checking if the character after the zeros is correct
        last_chars = ["F", "E", "D", "C", "B", "A", "9", "8", "7", "6", "5", "4", "3", "2", "1"]
        char_to_check = Enum.at(String.graphemes(head.hash), head.powZeros)

        '0' <= char_to_check && char_to_check <= Enum.at(last_chars, head.powRemainder)
      else
        false
      end
    end

    @doc """
    Validation rule BR_G99
    HEAD.currency
      If HEAD.number > 0:
        HEAD.currency = HEAD~1.currency
      Else:
        HEAD.currency = null
    """
    def currency(local_bindex, global_bindex \\ 'global_bindex') do
      [{key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)

      if head.number > 0 do
        [[currency]] =
          DB.match(global_bindex, {:_, %{number: head.number - 1, currency: :"$1"}})

        :ets.insert(local_bindex, {key, Map.merge(head, %{currency: currency})})
      else
        :ets.insert(local_bindex, {key, Map.merge(head, %{currency: nil})})
      end
    end
  end

  defmodule IIndex do
    @moduledoc """
    Validation rules that apply to the Duniter IINDEX (Identities index).
    See https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md
    """

    @doc """
    Validation rule BR_G20
    Identity UserID unicity
    For each ENTRY in local IINDEX:
      If op = 'CREATE':
        ENTRY.uidUnique = COUNT(GLOBAL_IINDEX[uid=ENTRY.uid) == 0
      Else:
        ENTRY.uidUnique = true
    """
    def uidUnique(local_iindex, global_iindex \\ 'global_iindex') do
      :ets.match_object(local_iindex, {:"$1", :"$2"})
      |> Enum.map(fn {key, entry} ->
        # match global index entries on uid
        if DB.match(global_iindex, {:"$1", %{uid: entry.uid}}) == [] do
          :ets.insert(local_iindex, {key, Map.merge(entry, %{uidUnique: true})})
        else
          :ets.insert(local_iindex, {key, Map.merge(entry, %{uidUnique: false})})
        end
      end)
    end

    @doc """
    Validation rule BR_G21
    Identity pubkey unicity
    For each ENTRY in local IINDEX:
    If op = 'CREATE':
      ENTRY.pubUnique = COUNT(GLOBAL_IINDEX[pub=ENTRY.pub) == 0
    Else:
      ENTRY.pubUnique = true
    """
    def pubUnique(local_iindex, global_iindex \\ 'global_iindex') do
      :ets.match_object(local_iindex, {:"$1", :"$2"})
      |> Enum.map(fn {key, entry} ->
        ## match global index entries on public key and op
        if DB.match(global_iindex, {:"$1", %{pub: entry.pub, op: "CREATE"}}) == [] do
          ## Map.merge(entry,%{pubUnique: ...}) appends the entry and the new field
          :ets.insert(local_iindex, {key, Map.merge(entry, %{pubUnique: true})})
        else
          :ets.insert(local_iindex, {key, Map.merge(entry, %{pubUnique: false})})
        end
      end)
    end

    @doc """
    Validation rule BR_G33
    ENTRY.excludedIsMember
    For each ENTRY in local IINDEX where member != false:
      ENTRY.excludedIsMember = true
    For each ENTRY in local IINDEX where member == false:
      ENTRY.excludedIsMember = REDUCE(GLOBAL_IINDEX[pub=ENTRY.pub]).member
    """
    def excludedIsMember(local_iindex, global_iindex \\ 'global_iindex') do
      :ets.match_object(local_iindex, {:"$1", :"$2"})
      |> Enum.map(fn {key, entry} ->
        if entry.member != false do
          :ets.insert(local_iindex, {key, Map.merge(entry, %{excludedIsMember: true})})
        else
          list_pub = DB.match(global_iindex, {:_, %{pub: entry.pub, member: :"$2"}})

          excludedIsMember =
            is_nil(
              Enum.find_value(list_pub, fn elt ->
                [member] = elt
                !member
              end)
            )

          :ets.insert(
            local_iindex,
            {key, Map.merge(entry, %{excludedIsMember: excludedIsMember})}
          )
        end
      end)
    end

    @doc """
    Validation rule BR_G35
    ENTRY.isBeingKicked
    For each ENTRY in local IINDEX where member != false:
      ENTRY.isBeingKicked = false
    For each ENTRY in local IINDEX where member == false:
      ENTRY.isBeingKicked = true
    """
    def isBeingKicked(local_iindex) do
      # Get the entry waiting to be verified
      :ets.match_object(local_iindex, {:"$1", :"$2"})
      |> Enum.map(fn {key, entry} ->
        :ets.insert(local_iindex, {key, Map.merge(entry, %{isBeingKicked: !entry.member})})
      end)
    end

    @doc """
    Validation rule BR_G63
    Identity writability
      ENTRY.age <= [idtyWindow]
    """
    def identityWritability(local_iindex) do
      :ets.match(local_iindex, {:_, :"$1"})
      |> Enum.reduce(true, fn [entry], acc ->
        if entry[:age] != nil do
          entry.age <= Constants.Contract.idtyWindow() && acc
        else
          acc
        end
      end)
    end

    @doc """
    Validation rule BR_G73
    Identity UserID unicity
      ENTRY.uidUnique == true
    """
    def identityUserIdUnicity(local_iindex) do
      :ets.match(local_iindex, {:_, :"$1"})
      |> Enum.reduce(true, fn [entry], acc ->
        if entry[:uidUnique] != nil do
          entry[:uidUnique] && acc
        else
          acc
        end
      end)
    end

    @doc """
    Validation rule BR_G74
    Identity pubkey unicity
      ENTRY.pubUnique == true
    """
    def identityPubkeyUnicity(local_iindex) do
      :ets.match(local_iindex, {:_, :"$1"})
      |> Enum.reduce(true, fn [entry], acc ->
        if entry[:pubUnique] != nil do
          entry[:pubUnique] && acc
        else
          acc
        end
      end)
    end
  end

  defmodule MIndex do
    @moduledoc """
    Validation rules that apply to the Duniter MINDEX (Members index).
    See https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md
    """

    @doc """
    Validation rule BR_G34
    For each ENTRY in local MINDEX where revoked_on == null:
      ENTRY.isBeingRevoked = false
    """
    def isBeingRevoked(local_mindex, key) do
      # Get the entry waiting to be verified
      [{key, entry}] = :ets.lookup(local_mindex, key)

      :ets.insert(
        local_mindex,
        {key,
         Map.merge(entry, %{
           isBeingRevoked: Map.has_key?(entry, :revoked_on) && !is_nil(entry.revoked_on)
         })}
      )
    end

    @doc """
    Validation rule BR_G64
    Rule:
      ENTRY.age <= [msWindow]
    """
    def membershipWritability(local_mindex) do
      keys_entries = :ets.match_object(local_mindex, {:"$1", :"$2"})

      if keys_entries == [] do
        true
      else
        Enum.map(keys_entries, fn {_key, entry} -> entry.age <= Constants.Contract.msWindow() end)
        |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
      end
    end

    @doc """
    Validation rule BR_G75
    Rule:
      ENTRY.numberFollowing == true
    """
    def membershipSuccession(local_mindex) do
      keys_entries = :ets.match_object(local_mindex, {:"$1", :"$2"})

      if keys_entries == [] do
        true
      else
        Enum.map(keys_entries, fn {_key, entry} -> entry.numberFollowing == true end)
        |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
      end
    end

    @doc """
    Validation rule BR_G77
    Rule:
     ENTRY.onRevoked == false
    """
    def membershipOnRevoked(local_mindex) do
      keys_entries = :ets.match_object(local_mindex, {:"$1", :"$2"})

      if keys_entries == [] do
        true
      else
        Enum.map(keys_entries, fn {_key, entry} -> entry.onRevoked == false end)
        |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
      end
    end

    @doc """
    Validation rule BR_G78
    Rule:
      ENTRY.joinsTwice == false
    """
    def membershipJoinsTwice(local_mindex) do
      keys_entries = :ets.match_object(local_mindex, {:"$1", :"$2"})

      if keys_entries == [] do
        true
      else
        Enum.map(keys_entries, fn {_key, entry} -> entry.joinsTwice == false end)
        |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
      end
    end

    @doc """
    Validation rule BR_G79
    Rule:
      ENTRY.enoughCerts == true
    """
    def membershipEnoughCertifications(local_mindex) do
      keys_entries = :ets.match_object(local_mindex, {:"$1", :"$2"})

      if keys_entries == [] do
        true
      else
        Enum.map(keys_entries, fn {_key, entry} -> entry.enoughCerts == true end)
        |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
      end
    end

    @doc """
    Validation rule BR_G80
    Rule:
      ENTRY.leaverIsMember == true
    """
    def membershipLeaver(local_mindex) do
      keys_entries = :ets.match_object(local_mindex, {:"$1", :"$2"})

      if keys_entries == [] do
        true
      else
        Enum.map(keys_entries, fn {_key, entry} -> entry.leaverIsMember == true end)
        |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
      end
    end

    @doc """
    Validation rule BR_G81
    Rule:
      ENTRY.activeIsMember == true
    """
    def membershipActive(local_mindex) do
      keys_entries = :ets.match_object(local_mindex, {:"$1", :"$2"})

      if keys_entries == [] do
        true
      else
        Enum.map(keys_entries, fn {_key, entry} -> entry.activeIsMember == true end)
        |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
      end
    end

    @doc """
    Validation rule BR_G82
    Rule:
      ENTRY.revokedIsMember == true
    """
    def revocationByMember(local_mindex) do
      keys_entries = :ets.match_object(local_mindex, {:"$1", :"$2"})

      if keys_entries == [] do
        true
      else
        Enum.map(keys_entries, fn {_key, entry} -> entry.revokedIsMember == true end)
        |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
      end
    end

    @doc """
    Validation rule BR_G83
    Rule:
      ENTRY.alreadyRevoked == false
    """
    def revocationSingleton(local_mindex) do
      keys_entries = :ets.match_object(local_mindex, {:"$1", :"$2"})

      if keys_entries == [] do
        true
      else
        Enum.map(keys_entries, fn {_key, entry} -> entry.alreadyRevoked == false end)
        |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
      end
    end

    @doc """
    Validation rule BR_G84
    Rule:
      ENTRY.revocationSigOK == true
    """
    def revocationSignature(local_mindex) do
      keys_entries = :ets.match_object(local_mindex, {:"$1", :"$2"})

      if keys_entries == [] do
        true
      else
        Enum.map(keys_entries, fn {_key, entry} -> entry.revocationSigOK == true end)
        |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
      end
    end

    @doc """
    Validation rule BR_G85
    Rule:
      ENTRY.excludedIsMember == true
    """
    def excludedIsMember(local_mindex) do
      keys_entries = :ets.match_object(local_mindex, {:"$1", :"$2"})

      if keys_entries == [] do
        true
      else
        Enum.map(keys_entries, fn {_key, entry} -> entry.excludedIsMember == true end)
        |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
      end
    end
  end

  defmodule CIndex do
    @moduledoc """
    Validation rules that apply to the Duniter CINDEX (Certification index).
    See https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md
    """

    @doc """
    Validation rule BR_G42
    ENTRY.toNewcomer
      ENTRY.toNewcomer = COUNT(LOCAL_IINDEX[member=true,pub=ENTRY.receiver]) > 0
    """
    def toNewcomer(local_cindex, local_iindex, key) do
      # Get the entry waiting to be verified
      [{key, entry}] = :ets.lookup(local_cindex, key)

      :ets.match(local_iindex, {:"$1", %{member: true, pub: entry.receiver}})
      |> Enum.count()
      |> (fn x -> Map.merge(entry, %{toNewcomer: x != 0}) end).()
      |> (&:ets.insert(local_cindex, {key, &1})).()
    end

    @doc """
    Validation rule BR_G65
    Certification writability
      ENTRY.age <= [sigWindow]
    """
    def certificationWritability(local_cindex) do
      keys_entries = :ets.match_object(local_cindex, {:"$1", :"$2"})

      if keys_entries == [] do
        true
      else
        Enum.map(keys_entries, fn {_key, entry} -> entry.age <= Constants.Contract.sigWindow() end)
        |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
      end
    end

    @doc """
    Validation rule BR_G66
    Certification stock
      ENTRY.stock <= sigStock
    """
    def certificationStock(local_cindex) do
      keys_entries = :ets.match_object(local_cindex, {:"$1", :"$2"})

      if keys_entries == [] do
        true
      else
        Enum.map(keys_entries, fn {_key, entry} ->
          entry.stock <= Constants.Contract.sigStock()
        end)
        |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
      end
    end

    @doc """
    Validation rule BR_G40
    ENTRY.fromMember
      ENTRY.fromMember = REDUCE(GLOBAL_IINDEX[pub=ENTRY.issuer]).member
    """
    def fromMember(key, local_cindex, global_iindex \\ 'global_iindex') do
      # Get the entry waiting to be verified
      [{key, entry}] = :ets.lookup(local_cindex, key)

      DB.match(global_iindex, :"$1")
      fromMember_data =
        DB.match(global_iindex, {:"$1", %{pub: entry.issuer}})
        |> Enum.map(fn [u] ->
          [{_k, a}] = DB.lookup(global_iindex, u)
          a
        end)
        |> IndexUtility.duniter_reduce()
        |> Map.get(:member)

      :ets.insert(local_cindex, {key, Map.merge(entry, %{fromMember: fromMember_data})})
    end

    @doc """
    Validation rule BR_G67
    Certification period
    ENTRY.unchainables == 0
    """
    def certificationPeriod(local_cindex) do
      keys_entries = :ets.match_object(local_cindex, {:"$1", :"$2"})

      if keys_entries == [] do
        true
      else
        Enum.map(keys_entries, fn {_key, entry} -> entry.unchainables == 0 end)
        |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
      end
    end

    @doc """
    Validation rule BR_G68
    If HEAD.number > 0:
      ENTRY.fromMember == true
    """
    def certificationFromMember(local_bindex, local_cindex) do
      [{_key, head}] = IndexUtility.find_first_local_bindex_entry(local_bindex)

      if head.number > 0 do
        keys_entries = :ets.match_object(local_cindex, {:"$1", :"$2"})

        if keys_entries == [] do
          true
        else
          Enum.map(keys_entries, fn {_key, entry} -> entry.fromMember == true end)
          |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
        end
      else
        true
      end
    end

    @doc """
    Validation rule BR_G71
    Certification replay
    ENTRY.isReplay == false || ENTRY.isReplayable == true
    """
    def certificationReplay(local_cindex) do
      keys_entries = :ets.match_object(local_cindex, {:"$1", :"$2"})

      if keys_entries == [] do
        true
      else
        Enum.map(keys_entries, fn {_key, entry} ->
          entry.isReplay == false || entry.isReplayable == true
        end)
        |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
      end
    end

    @doc """
    Validation rule BR_G72
    Certification signature
    ENTRY.sigOK == true
    """
    def certificationSignature(local_cindex) do
      keys_entries = :ets.match_object(local_cindex, {:"$1", :"$2"})

      if keys_entries == [] do
        true
      else
        Enum.map(keys_entries, fn {_key, entry} -> entry.sigOK == true end)
        |> Enum.reduce(fn comparison0, comparison1 -> comparison0 && comparison1 end)
      end
    end

    @doc """
    Validation rule BR_G41
    ENTRY.toMember
      ENTRY.toMember = REDUCE(GLOBAL_IINDEX[pub=ENTRY.receiver]).member
    """
    def toMember(key, local_cindex, global_iindex \\ 'global_iindex') do
      # Get the entry waiting to be verified
      [{key, entry}] = :ets.lookup(local_cindex, key)

      toMember_data =
        DB.match(global_iindex, {:"$1", %{pub: entry.issuer}})
        |> Enum.map(fn [u] ->
          [{_k, a}] = DB.lookup(global_iindex, u)
          a
        end)
        |> IndexUtility.duniter_reduce()
        |> Map.get(:member)

      :ets.insert(local_cindex, {key, Map.merge(entry, %{toMember: toMember_data})})
    end

    @doc """
    Validation rule BR_G43
    ENTRY.toLeaver
      ENTRY.toLeaver = REDUCE(GLOBAL_MINDEX[pub=ENTRY.receiver]).leaving
    """
    def toLeaver(local_cindex, local_mindex, key) do
      # Get the entry waiting to be verified
      [{key, entry}] = :ets.lookup(local_cindex, key)

      toLeaver_data =
        :ets.match(local_mindex, {:"$1", %{pub: entry.receiver}})
        |> Enum.map(fn [u] ->
          [{_k, a}] = :ets.lookup(local_mindex, u)
          a
        end)
        |> IndexUtility.duniter_reduce()
        |> Map.get(:leaving)

      :ets.insert(local_cindex, {key, Map.merge(entry, %{toLeaver: toLeaver_data})})
    end

    @doc """
    Validation rule BR_G70
    Certification to non-leaver
    ENTRY.toLeaver == false
    """
    def certificationToNonLeaver(local_cindex) do
      :ets.match(local_cindex, {:_, :"$1"})
      |> Enum.reduce(true, fn [entry], acc ->
        if entry[:toLeaver] != nil do
          entry[:toLeaver] == false && acc
        else
          acc
        end
      end)
    end

    defp helper(bool_or_nil) do
      case bool_or_nil do
        nil -> false
        _ -> bool_or_nil
      end
    end

    @doc """
    Validation rule BR_G69
    Certification to member or newcomer
    ENTRY.toMember == true
    OR
    ENTRY.toNewcomer == true

    """
    def certifToMemberOrNewcomer(local_cindex) do
      entries = :ets.match(local_cindex, {:_, :"$2"})

      Enum.reduce(entries, true, fn entry, acc ->
        acc and
          (helper(Map.get(Enum.at(entry, 0), :toMember)) or
             helper(Map.get(Enum.at(entry, 0), :toNewcomer)))
      end)
    end
  end

  defmodule SIndex do
    @moduledoc """
    Validation rules that apply to the Duniter SINDEX (Signatures index).
    See https://git.duniter.org/documents/rfcs/-/blob/master/rfc/0010_Duniter_Blockchain_Protocol_V12.md
    """

    @doc """
    Validation rule BR_G87
    For each LOCAL_SINDEX[op='UPDATE'] as ENTRY:
    Rule:
      ENTRY.available == true
    """
    def checkInputIsAvailable(local_sindex) do
      # Get all the entries that have 'op' as value "UPDATE"
      entries = :ets.match(local_sindex, {:_, :"$1"})
      # Verify that all the entries have 'available' = true
      Enum.reduce_while(entries, true, fn [entry], acc ->
        if Map.get(entry, :op) == "UPDATE" && Map.get(entry, :available) != true do
          {:halt, false}
        else
          {:cont, acc}
        end
      end)
    end

    @doc """
    Validation rule BR_G88
    For each LOCAL_SINDEX[op='UPDATE'] as ENTRY:
    Rule:
      ENTRY.isLocked == false
    """
    def checkInputIsUnlocked(local_sindex) do
      # Get all the entries that have 'op' as value "UPDATE"
      entries = :ets.match(local_sindex, {:_, :"$1"})
      # Verify that all the entries have 'isLocked' = false
      Enum.reduce_while(entries, true, fn [entry], acc ->
        if Map.get(entry, :op) == "UPDATE" && Map.get(entry, :isLocked) != false do
          {:halt, false}
        else
          {:cont, acc}
        end
      end)
    end

    @doc """
    Validation rule BR_G89
    For each LOCAL_SINDEX[op='UPDATE'] as ENTRY:
    Rule:
      ENTRY.isLocked == false
    """
    def checkInputIsTimeUnlocked(local_sindex) do
      # Get all the entries that have 'op' as value "UPDATE"
      entries = :ets.match(local_sindex, {:_, :"$1"})
      # Verify that all the entries have 'isTimeLocked' = false
      Enum.reduce_while(entries, true, fn [entry], acc ->
        if Map.get(entry, :op) == "UPDATE" && Map.get(entry, :isTimeLocked) != false do
          {:halt, false}
        else
          {:cont, acc}
        end
      end)
    end

    @doc """
    Validation rule BR_G90
    For each LOCAL_SINDEX[op='UPDATE'] as ENTRY:
    Rule:
      ENTRY.isTimeLocked == false
    """
    def outputBase(local_sindex, global_bindex) do
      # Get the head~1 node

      [last_block: head_nb] = DB.lookup(global_bindex, :last_block)
      [[head_1_id]] = DB.match(global_bindex, {:"$1", %{number: head_nb.number - 1}})
      [{_key_1, head_1}] = DB.lookup(global_bindex, head_1_id)

      :ets.match(local_sindex, {{"CREATE", :_, :_}, :"$1"})
      |> Enum.map(fn [entry] ->
        entry.base <= head_1.base
      end)
      |> Enum.all?()
    end
  end
end
