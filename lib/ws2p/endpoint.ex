defmodule WS2P.Endpoint do
  @moduledoc """
  Provides some functions for managing connection between nodes (WS2P).
  """

  require Logger

  defp start_connection!(args) do
    {:ok, connection} =
      DynamicSupervisor.start_child(
        WS2P.ConnectionSupervisor,
        {WS2P.Connection, args}
      )

    connection
  end

  @doc """
  Add a new child connection to the WS2P `DynamicSupervisor`.

  ## Parameters

    - address_and_port: Tuple that contains the address and port of the destination node
    - socket_options: Keyword list that contains the options of the socket

  ## Examples

      iex> WS2P.Endpoint.connect!({“127.0.0.1”, 20900}, [])
  """
  @spec connect!(address_and_port :: {String, Integer}, socket_options :: Keyword.t()) :: pid
  def connect!(address_and_port, socket_options \\ []) do
    start_connection!({:connect, address_and_port, socket_options})
  end

  @doc """
   Listen to the port for connection and wait for incoming connections. If a connection is detected, add a new child connection to the WS2P `DynamicSupervisor`.

  ## Parameters

  - port: Integer which is the port of server
  - path: String which is the path of server

  ## Examples

      iex> WS2P.Endpoint.accept_connection!(20900, ‘/’)
  """
  @spec accept_connections!(port :: Integer, path :: String) :: none
  def accept_connections!(port, path) do
    websocket_listener = Socket.Web.listen!(port)
    Logger.info("Listening for websocket connections on port #{port} at #{path}")
    loop(websocket_listener, path)
  end

  defp loop(websocket_listener, path) do
    client_socket = Socket.Web.accept!(websocket_listener)

    if client_socket.path == path do
      Socket.Web.accept!(client_socket)
      start_connection!({:accept, client_socket})
    end

    loop(websocket_listener, path)
  end
end
