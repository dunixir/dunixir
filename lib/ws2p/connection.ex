defmodule WS2P.Connection do
  @moduledoc """
  The communication between nodes (WS2P).

  Provides some functions for nodes to handle (receive and send) the packets from/to other nodes.
  """

  use GenServer, restart: :transient
  require Logger

  @keypair Ed25519.generate_key_pair()
  @currency_name "g1-test"

  @doc """
  If `:connect`, start the `GenServer` which is responsible to manage connections with nodes.

  If `:accept`, accept the connection and start the `GenServer` if it is necessary.

  ## Parameters

    - address_and_port: Tuple that contains the address and port of the node which needs to connect
    - socket_options: Keyword list that contains the options of the socket
    - socket: `t:Socket.Web.t()/0` that contains the socket of the other node

  ## Examples

      iex> WS2P.Connection.start_link({:connect, {“localhost”, 20900}, []})
      iex> WS2P.Connection.start_link({:accept, %Socket.Web{...})
  """
  @spec start_link({:accept, Socket.Web.t()} | {:connect, {String, Integer}, Keyword.t()}) ::
          :ignore | {:error, any} | {:ok, pid}
  def start_link({:connect, address_and_port, socket_options}) do
    GenServer.start_link(__MODULE__, {:connect, address_and_port, socket_options},
      name: {:via, Registry, {WS2P.Connection.Registry, address_and_port}}
    )
  end

  def start_link({:accept, socket}) do
    address_and_port = Socket.Web.remote!(socket)

    GenServer.start_link(__MODULE__, {:accept, address_and_port, socket},
      name: {:via, Registry, {WS2P.Connection.Registry, address_and_port}}
    )
  end

  @doc """
  If `:connect`, start Websocket connection and initialize the `GenServer` with some initial datas.

  If `:accept`, initialize the `GenServer` with some initial datas and existing socket. Accept socket from the address and port given.

  ## Parameters

    - address_and_port: Tuple that contains the address and port of the node which needs to connect
    - socket_options: Keyword list that contains the options of the socket
    - socket: `t:Socket.Web.t()/0` Existing socket


  ## Examples

      iex> WS2P.Connection.init({:connect, {“127.0.0.1”, 20900}, []})
      iex> WS2P.Connection.init({:accept, {“127.0.0.1”, 20900}, []})

  """
  @spec init(
          {:accept | :connect, address_and_port :: {String, Integer},
           socket_options :: Keyword.t()}
        ) ::
          {:ok,
           %{
             address_and_port: {String, Integer},
             currency_name: <<_::56>>,
             keypair: {<<_::256>>, <<_::256>>},
             socket: Socket.Web.t()
           }, {:continue, :send_connect_object}}
  def init({:connect, address_and_port, socket_options}) do
    socket = Socket.Web.connect!(address_and_port, socket_options)
    Logger.info("Connected socket to #{inspect(address_and_port)}")
    schedule_poll_remote_packets()

    {:ok,
     %{
       socket: socket,
       keypair: @keypair,
       currency_name: @currency_name,
       address_and_port: address_and_port
     }, {:continue, :send_connect_object}}
  end

  def init({:accept, address_and_port, socket}) do
    Logger.info("Accepted socket from #{inspect(address_and_port)}")
    schedule_poll_remote_packets()

    {:ok,
     %{
       socket: socket,
       keypair: @keypair,
       currency_name: @currency_name,
       address_and_port: address_and_port
     }, {:continue, :send_connect_object}}
  end

  @doc """
    Finish the initialization of `GenServer`: generate the signatures and the informations of connection.
  """
  @spec handle_continue(:send_connect_object, %{
          :address_and_port => {String, Integer},
          :currency_name => <<_::56>>,
          :keypair => {binary, binary},
          optional(any) => any
        }) :: {:noreply, %{:socket => Socket.Web.t(), optional(any) => any}}
  def handle_continue(
        :send_connect_object,
        %{
          address_and_port: address_and_port,
          keypair: {sec, pub},
          currency_name: currency_name
        } = state
      ) do
    local_challenge = UUID.uuid4() <> UUID.uuid4()
    pub_as_base58 = pub |> Base58.encode()
    raw_format = "WS2P:CONNECT:#{currency_name}:#{pub_as_base58}:#{local_challenge}"
    signature = Ed25519.signature(raw_format, sec, pub)

    connect_object = %{
      "auth" => "CONNECT",
      "pub" => pub_as_base58,
      "challenge" => local_challenge,
      "sig" => signature |> Base.encode64()
    }

    Logger.info("Sending CONNECT to node #{inspect(address_and_port)}\n#{raw_format}")
    handle_local_object(connect_object, Map.put_new(state, :local_challenge, local_challenge))
  end

  defp schedule_poll_remote_packets do
    Process.send_after(self(), :poll_remote_packets, 1000)
  end

  def handle_info(:poll_remote_packets, %{socket: socket} = state) do
    poll_remote_packets(socket)
    |> Enum.map(fn packet ->
      GenServer.cast(self(), {:remote_packet, packet})
    end)

    schedule_poll_remote_packets()
    {:noreply, state}
  end

  defp poll_remote_packets(socket, remote_packets \\ []) do
    case Socket.Web.recv(socket, timeout: 0) do
      {:ok, packet} ->
        poll_remote_packets(socket, [packet | remote_packets])

      {:error, :timeout} ->
        remote_packets

      {:error, code} ->
        Logger.error(code)
        raise Socket.Error, reason: code
    end
  end

  defp handle_local_packet(packet, %{socket: socket} = state) do
    Socket.Web.send!(socket, packet)
    {:noreply, state}
  end

  defp handle_local_msg(msg, state) do
    handle_local_packet({:text, msg}, state)
  end

  defp handle_local_object(object, state) do
    msg = Poison.encode!(object, pretty: true)
    handle_local_msg(msg, state)
  end

  def handle_cast({:local_msg, msg}, state) do
    handle_local_msg(msg, state)
  end

  def handle_cast({:local_object, object}, state) do
    handle_local_object(object, state)
  end

  def handle_cast({:remote_packet, packet}, state) do
    handle_remote_packet(packet, state)
  end

  defp handle_remote_object(
         %{
           "auth" => "CONNECT",
           "challenge" => challenge,
           "pub" => _pub,
           "sig" => _sig
         },
         %{
           keypair: {my_sec, my_pub},
           currency_name: currency_name,
           address_and_port: address_and_port
         } = state
       ) do
    Logger.info("Received CONNECT from node #{inspect(address_and_port)}")

    my_pub_as_base58 = my_pub |> Base58.encode()
    raw_format = "WS2P:ACK:#{currency_name}:#{my_pub_as_base58}:#{challenge}"
    signature = Ed25519.signature(raw_format, my_sec, my_pub)

    ack_object = %{
      "auth" => "ACK",
      "pub" => my_pub_as_base58,
      "sig" => signature |> Base.encode64()
    }

    Logger.info("Sending ACK to node #{inspect(address_and_port)}\n#{raw_format}")
    handle_local_object(ack_object, state)
  end

  defp handle_remote_object(
         %{
           "auth" => "ACK",
           "pub" => _pub,
           "sig" => _sig
         },
         %{
           keypair: {my_sec, my_pub},
           currency_name: currency_name,
           local_challenge: local_challenge,
           address_and_port: address_and_port
         } = state
       ) do
    Logger.info("Received ACK from node #{inspect(address_and_port)}")

    my_pub_as_base58 = my_pub |> Base58.encode()
    raw_format = "WS2P:OK:#{currency_name}:#{my_pub_as_base58}:#{local_challenge}"
    signature = Ed25519.signature(raw_format, my_sec, my_pub)

    ok_object = %{
      "auth" => "OK",
      "sig" => signature |> Base.encode64()
    }

    Logger.info("Sent OK to node #{inspect(address_and_port)}\n#{raw_format}")
    handle_local_object(ok_object, state)
  end

  defp handle_remote_object(
         %{
           "auth" => "OK",
           "sig" => _sig
         },
         %{
           address_and_port: address_and_port
         } = state
       ) do
    Logger.info("Received OK from node #{inspect(address_and_port)}")
    {:noreply, state}
  end

  defp handle_remote_object(object, state) do
    Logger.debug("Received remote object: #{inspect(object)}")

    if object["reqId"] do
      # TODO handling WS2P Requests
      # https://git.duniter.org/nodes/common/doc/-/blob/ws2p_v2/rfc/0004_ws2p_v1.md#ws2p-requests
      case object do
        # TODO getCurrent
        %{"reqId" => reqId, "body" => %{"name" => "CURRENT", "params" => %{}}} ->
          Logger.warn("Unhandled WS2P Request: #{inspect(object)}")

        # TODO getBlock
        %{
          "reqId" => reqId,
          "body" => %{"name" => "BLOCK_BY_NUMBER", "params" => %{"number" => block_number}}
        } ->
          Logger.warn("Unhandled WS2P Request: #{inspect(object)}")

        # TODO getBlocks
        %{
          "reqId" => reqId,
          "body" => %{
            "name" => "BLOCKS_CHUNK",
            "params" => %{"count" => nb_blocks_requested, "fromNumber" => first_requested_block}
          }
        } ->
          Logger.warn("Unhandled WS2P Request: #{inspect(object)}")

        # TODO getRequirementsPending
        %{
          "reqId" => reqId,
          "body" => %{
            "name" => "WOT_REQUIREMENTS_OF_PENDING",
            "params" => %{"minCert" => minCert}
          }
        } ->
          Logger.warn("Unhandled WS2P Request: #{inspect(object)}")

        # TODO handling answer of a WS2P request (not here, an anwser does not have a reqId rather a resId)
        _ ->
          Logger.warn("Unhandled WS2P Answer: #{inspect(object)}")
      end
    else
      # TODO handling received document
      # https://git.duniter.org/nodes/common/doc/-/blob/ws2p_v2/rfc/0004_ws2p_v1.md#documents-rebound-policy
      case object["body"] do
        # TODO
        %{"name" => "PEER", "peer" => doc} ->
          Logger.warn("Unhandled peer document: #{inspect(object)}")

        %{"name" => "TRANSACTION", "transaction" => doc} ->
          if Doc.Mempool.can_add?(doc) and Doc.Transaction.verif(doc) do
            Doc.Mempool.add(doc)
            WS2P.ConnectionSupervisor.bounce({:transaction, doc})
          else
            Logger.info("Discarded remote document: #{inspect(object)}")
          end

        %{"name" => "MEMBERSHIP", "membership" => doc} ->
          if Doc.Mempool.can_add?(doc) and Doc.Membership.verif(doc) do
            Doc.Mempool.add(doc)
            WS2P.ConnectionSupervisor.bounce({:membership, doc})
          else
            Logger.info("Discarded remote document: #{inspect(object)}")
          end

        %{"name" => "CERTIFICATION", "certification" => doc} ->
          if Doc.Mempool.can_add?(doc) and Doc.Certification.verif(doc) do
            Doc.Mempool.add(doc)
            WS2P.ConnectionSupervisor.bounce({:certification, doc})
          else
            Logger.info("Discarded remote document: #{inspect(object)}")
          end

        %{"name" => "IDENTITY", "identity" => doc} ->
          if Doc.Mempool.can_add?(doc) and Doc.Identity.verif(doc) do
            Doc.Mempool.add(doc)
            WS2P.ConnectionSupervisor.bounce({:identity, doc})
          else
            Logger.info("Discarded remote document: #{inspect(object)}")
          end

        %{"name" => "REVOCATION", "revocation" => doc} ->
          if Doc.Mempool.can_add?(doc) and Doc.Revocation.verif(doc) do
            Doc.Mempool.add(doc)
            WS2P.ConnectionSupervisor.bounce({:revocation, doc})
          else
            Logger.info("Discarded remote document: #{inspect(object)}")
          end

        # TODO
        %{"name" => "BLOCK", "block" => block} ->
          existing_block =
            DB.lookup('block', block["number"]) != [] and
              DB.lookup('global_bindex', block["number"]) != []

          # simple check, could be improved see comment here https://gitlab.imt-atlantique.fr/dunixir/dunixir/-/merge_requests/45#note_5266
          if !existing_block do
            case Block.Validation.Local.valid(block) do
              {:ok, [local_iindex, local_mindex, local_cindex, local_sindex]} ->
                # TODO global validation
                Logger.warn("Unhandled block: #{inspect(object)}")

              {:error, error} ->
                Logger.info("Discarded remote block - #{error}: #{inspect(object)}")
            end
          else
            Logger.info("Discarded remote block that is already known: #{inspect(object)}")
          end

        # TODO handling received heads
        # https://git.duniter.org/nodes/common/doc/-/blob/ws2p_v2/rfc/0004_ws2p_v1.md#rebound-policy
        # GenServer.call(WS2P.Cluster, {:heads_received, heads})
        %{"name" => "HEAD", "heads" => heads} ->
          Logger.warn("Unhandled heads: #{inspect(object)}")
      end
    end

    {:noreply, state}
  end

  defp handle_remote_packet({:text, msg}, state) do
    case Poison.decode(msg) do
      {:ok, object} ->
        handle_remote_object(object, state)

      {:error, error} ->
        Logger.warn("Could not decode message:\n#{inspect(error, pretty_print: true)}")
        {:noreply, state}
    end
  end

  defp handle_remote_packet({type, msg}, %{socket: socket} = state) do
    Logger.debug("Received message of type #{inspect(type)} from #{socket.origin}:\n#{msg}")
    {:noreply, state}
  end

  defp handle_remote_packet(:close, %{socket: socket, address_and_port: address_and_port} = state) do
    Logger.info("Remote #{inspect(address_and_port)} closed socket connection")
    Socket.Web.close(socket)
    {:stop, :shutdown, state}
  end

  defp handle_remote_packet(
         {:close, code, reason},
         %{socket: socket, address_and_port: address_and_port} = state
       ) do
    Logger.debug(
      "Remote #{inspect(address_and_port)} closed socket connection with code \"#{code}\""
    )

    Logger.debug("Reason: \"#{reason}\"")
    Socket.Web.close(socket)
    {:stop, :shutdown, state}
  end
end
