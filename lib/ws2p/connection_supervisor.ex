defmodule WS2P.ConnectionSupervisor do
  # Automatically defines child_spec/1
  use DynamicSupervisor
  require Logger

  def start_link(init_arg) do
    DynamicSupervisor.start_link(__MODULE__, init_arg, name: __MODULE__)
  end

  @impl true
  def init(_init_arg) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end

  @doc """
  Transmit a document to all active WS2P connections

  ## Example
      iex> WS2P.ConnectionSupervisor.bounce({:identity,
      ...> %{
      ...> "buid" => "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
      ...> "currency" => "duniter_unit_test_currency",
      ...> "pubkey" => "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
      ...> "sig" => "1eubHHbuNfilHMM0G2bI30iZzebQ2cQ1PC7uPAw08FGMMmQCRerlF/3pc4sAcsnexsxBseA/3lY03KlONqJBAg==",
      ...> "uid" => "tic",
      ...> "version" => 10
      ...> }
      ...> })
  """
  def bounce({:transaction, document}) do
    %{"body" => %{"name" => "TRANSACTION", "transaction" => document}} |> bounce()
  end

  def bounce({:membership, document}) do
    %{"body" => %{"name" => "MEMBERSHIP", "membership" => document}} |> bounce()
  end

  def bounce({:certification, document}) do
    %{"body" => %{"name" => "CERTIFICATION", "certification" => document}} |> bounce()
  end

  def bounce({:identity, document}) do
    %{"body" => %{"name" => "IDENTITY", "identity" => document}} |> bounce()
  end

  def bounce({:revocation, document}) do
    %{"body" => %{"name" => "REVOCATION", "revocation" => document}} |> bounce()
  end

  def bounce(document) do
    DynamicSupervisor.which_children(__MODULE__)
    |> Enum.map(fn {:undefined, pid, :worker, [_module]} ->
      GenServer.cast(pid, {:local_object, document})
    end)

    Logger.info("Bounced document #{inspect(document)}")
  end

  @doc """
  List the PIDs of all active WS2P connections
  """
  def list_children do
    DynamicSupervisor.which_children(__MODULE__)
    |> Enum.map(fn {:undefined, pid, :worker, [_module]} ->
      pid
    end)
  end
end
