defmodule BMA.Blockchain.Block do
 @moduledoc """
Function related to one block
  """

  require Logger

  @doc """
Get one block choosen by its number from de db

  ## Parameters
    - number of the block
  ## Examples
      iex> BMA.Blockchain.Block.get("1")
  """
  def get(number) do
    {n, _} = :string.to_integer(number)

    case DB.lookup('block', n) do
      [] -> Poison.encode!(%{"ucode" => 2011, "message" => "Block not found"})
      [{n, block}] -> JSON.encode!(block)
    end
  end
end
