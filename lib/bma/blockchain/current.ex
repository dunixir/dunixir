defmodule BMA.Blockchain.Current do
  @moduledoc """
  Function related to multiple blocks
  """

  @doc """
  Return the return last accepted block

  ## Parameters

  ## Examples
      iex> BMA.Blockchain.Current.get
  """
  def get do
    # get the size of the database
    size = DB.size('block')
    [{_, block_current}] = DB.lookup('block', size - 1)
    Poison.encode!(block_current, pretty: true)
  end
end
