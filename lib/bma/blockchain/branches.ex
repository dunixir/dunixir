defmodule BMA.Blockchain.Branches do
 @moduledoc """
Function related to the current branche
  """
  require Logger

  @doc """
GET current branches of the node.
return : Top block of each branch, i.e. the last received block of each branch. An array of 4 blocks would mean the node has 4 branches, 3 would mean 3 branches, and so on.
  ## Parameters
    - none
  ## Examples
      iex> BMA.Blockchain.Branches.get
  """
  def get do
    Poison.encode!("#TODO")
  end
end
