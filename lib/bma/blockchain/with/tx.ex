defmodule BMA.Blockchain.With.Tx do

         @moduledoc """
Function related to block containing transactions.
  """
  require Logger
      @doc """
 GET the block numbers containing transactions.
return Block numbers
  ## Parameters
  ## Examples
      iex> BMA.Blockchain.With.Tx.get
  """


  def get do
    result = []

    result =
      result ++
        DB.traverse(
          'block',
          fn {_, value} ->
            if(length(value["transactions"]) > 0) do
              {:continue, value["number"]}
            else
              :continue
            end
          end
        )

    JSON.encode!(result: [blocks: result])
  end
end
