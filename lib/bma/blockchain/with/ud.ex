defmodule BMA.Blockchain.With.Ud do
         @moduledoc """
Function related to block containing Universal Dividend.
  """
  require Logger
      @doc """
GET the block numbers containing Universal Dividend.
return Block numbers
  ## Parameters
  ## Examples
      iex> BMA.Blockchain.With.Ud.get
  """

  def get do
    result = []

    result =
      result ++
        DB.traverse(
          'block',
          fn {_, value} ->
            if(value["dividend"] != nil) do
              {:continue, value["number"]}
            else
              :continue
            end
          end
        )

    JSON.encode!(result: [blocks: result])
  end
end
