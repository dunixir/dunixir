defmodule BMA.Blockchain.With.Revoked do
       @moduledoc """
Function related to block containing revoked members.
  """

      @doc """
  GET the block numbers containing revoked members.
return Block numbers
  ## Parameters
  ## Examples
      iex> BMA.Blockchain.With.Revoked.get
  """
  def get do
    result = []

    result =
      result ++
        DB.traverse(
          'block',
          fn {_, value} ->
            if(value["revoked"] != []) do
              {:continue, value["number"]}
            else
              :continue
            end
          end
        )

    JSON.encode!(result: [blocks: result])
  end
end
