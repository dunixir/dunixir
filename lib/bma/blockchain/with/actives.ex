defmodule BMA.Blockchain.With.Actives do
 @moduledoc """
Function related to blocs with actives (members updating their membership)
  """


  @doc """
GET the block numbers containing actives.
return Block numbers
  ## Parameters
  ## Examples
      iex> BMA.Blockchain.With.Actives.get
  """
  def get do
    result = []

    result =
      result ++
        DB.traverse(
          'block',
          fn {_, value} ->
            if(length(value["actives"]) > 0) do
              {:continue, value["number"]}
            else
              :continue
            end
          end
        )

    JSON.encode!(result: [blocks: result])
  end
end
