defmodule BMA.Blockchain.With.Newcomers do
     @moduledoc """
Function related to block containing newcomers (new identities)
  """

      @doc """
  Get a list of block ID of blocks with non-empty identitis
return Block numbers
  ## Parameters
  ## Examples
      iex> BMA.Blockchain.With.Newcomers.get
  """
  def get do

    list =
      DB.traverse(
        'block',
        fn {_, block} ->
          if block["identities"] != [] do
            {:continue, block["number"]}
          else
            :continue
          end
        end
      )

    Poison.encode!(%{result: %{blocks: list}})
  end
end
