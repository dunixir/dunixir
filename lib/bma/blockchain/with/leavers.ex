defmodule BMA.Blockchain.With.Leavers do
   @moduledoc """
Function related to blocs with leavers (members leaving definitely the currency).
  """


  @doc """
GET the block numbers containing leavers.
return Block numbers
  ## Parameters
  ## Examples
      iex> BMA.Blockchain.With.Leavers.get
  """
  def get do
    result = []

    result =
      result ++
        DB.traverse(
          'block',
          fn {_, value} ->
            if(value["leavers"] != []) do
              {:continue, value["number"]}
            else
              :continue
            end
          end
        )

    JSON.encode!(result: [blocks: result])
  end
end
