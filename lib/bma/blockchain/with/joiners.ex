defmodule BMA.Blockchain.With.Joiners do

   @moduledoc """
Function related to blocks with Joiners (newcomers or people coming back after exclusion).
  """

  require Logger

    @doc """
GET the block numbers containing joiners.
return Block numbers
  ## Parameters
  ## Examples
      iex> BMA.Blockchain.With.Joiners.get
  """
  def get do
    result = []

    result =
      result ++
        DB.traverse(
          'block',
          fn {_, value} ->
            if(length(value["joiners"]) > 0) do
              {:continue, value["number"]}
            else
              :continue
            end
          end
        )

    JSON.encode!(result: [blocks: result])
  end
end
