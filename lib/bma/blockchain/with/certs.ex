defmodule BMA.Blockchain.With.Certs do
   @moduledoc """
Function related to block containing certifications.
  """
  require Logger
    @doc """
GET the block numbers containing certifications.
return Block numbers
  ## Parameters
  ## Examples
      iex> BMA.Blockchain.With.Certs.get
  """
  def get do
    result = []

    result =
      result ++
        DB.traverse(
          'block',
          fn {_, value} ->
            if(length(value["certifications"]) > 0) do
              {:continue, value["number"]}
            else
              :continue
            end
          end
        )

    JSON.encode!(result: [blocks: result])
  end
end
