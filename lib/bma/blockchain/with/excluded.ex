defmodule BMA.Blockchain.With.Excluded do
   @moduledoc """
Function related to excluded blocs
  """
  require Logger
    @doc """
GET the block numbers containing excluded members.
return Block numbers
  ## Parameters
  ## Examples
      iex> BMA.Blockchain.With.Excluded.get
  """
  def get do
    result = []

    result =
      result ++
        DB.traverse(
          'block',
          fn {_, value} ->
            if(length(value["excluded"]) > 0) do
              {:continue, value["number"]}
            else
              :continue
            end
          end
        )

    JSON.encode!(result: [blocks: result])
  end
end
