defmodule BMA.Blockchain.Parameters do
  @moduledoc """
  Provides function to get parameters
  """

  @doc """
  GET the blockchain parameters used by this node.
  Return The synchronization parameters.

  ## Examples
      iex> BMA.Blockchain.Parameters.get
  """
  def get do
    [{0, blocks0}] = DB.lookup('block', 0)
    paras = String.split(blocks0["parameters"], ":")
    # parameters' order :
    # c:dt:ud0:sigPeriod:sigStock:sigWindow:sigValidity:sigQty:idtyWindow:msWindow:xpercent:msValidity
    # :stepMax:medianTimeBlocks:avgGenTime:dtDiffEval:percentRot:udTime0:udReevalTime0:dtReeval

    paras_map = %{
      currency: blocks0["currency"],
      c: String.to_float(Enum.at(paras, 0)),
      dt: String.to_integer(Enum.at(paras, 1)),
      ud0: String.to_integer(Enum.at(paras, 2)),
      sigPeriod: String.to_integer(Enum.at(paras, 3)),
      sigStock: String.to_integer(Enum.at(paras, 4)),
      sigWindow: String.to_integer(Enum.at(paras, 5)),
      sigValidity: String.to_integer(Enum.at(paras, 6)),
      sigQty: String.to_integer(Enum.at(paras, 7)),
      idtyWindow: String.to_integer(Enum.at(paras, 8)),
      msWindow: String.to_integer(Enum.at(paras, 9)),
      xpercent: String.to_float(Enum.at(paras, 10)),
      msValidity: String.to_integer(Enum.at(paras, 11)),
      stepMax: String.to_integer(Enum.at(paras, 12)),
      medianTimeBlocks: String.to_integer(Enum.at(paras, 13)),
      avgGenTime: String.to_integer(Enum.at(paras, 14)),
      dtDiffEval: String.to_integer(Enum.at(paras, 15)),
      percentRot: String.to_float(Enum.at(paras, 16)),
      udTime0: String.to_integer(Enum.at(paras, 17)),
      udReevalTime0: String.to_integer(Enum.at(paras, 18)),
      dtReeval: String.to_integer(Enum.at(paras, 19)),
      msPeriod: Constants.msPeriod(),
      sigReplay: Constants.sigReplay()
    }

    Poison.encode!(paras_map, pretty: true)
  end
end
