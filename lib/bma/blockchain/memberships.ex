defmodule BMA.Blockchain.Memberships do
  @moduledoc """
  Provides some functions to request the mindex
  """

  @doc """
  GET Membership data written for a member.
  return : A list of memberships issued by the member and written in the blockchain.

  ## Parameters

    - global_mindex :
    - pubkey : Public key or uid of a member we want see the memberships.
  """
  def mindex_search(global_mindex, pubkey) do
    DB.match(
      global_mindex,
      {:_, %{pub: pubkey, type: :"$1", created_on: :"$2", written_on: :"$3"}}
    )
    |> Enum.map(fn [type, created_on, written_on] ->
      %{
        version: 10,
        currency: "g1-test",
        membership:
          if type == "LEAVE" or type == "REV" do
            "OUT"
          else
            "IN"
          end,
        blockNumber: String.to_integer(List.first(String.split(created_on, "-"))),
        blockHash: List.last(String.split(created_on, "-")),
        written: String.to_integer(List.first(String.split(written_on, "-")))
      }
    end)
  end


  @doc """
  Return the research of a member into the global_mindex

  ## Parameters

    - search :
    - db_global_iindex
    - db_global_mindex
  """
  def get(
        search,
        db_global_iindex \\ 'global_iindex',
        db_global_mindex \\ 'global_mindex'
      ) do

    # iindex
    # pubkey: pub
    # uid: uid
    # sigDate: written_on

    # mindex
    # version: 10
    # currency: "g1-test"
    # membership: depends on op
    # blockNumber: created_on before "-"
    # blockHash: created_on after "-"
    # "written": written_on before "_"

    if DB.match(db_global_iindex, {:_, %{pub: search, uid: :"$1"}}) != [] do
      # if String.length(search) == 43 or String.length(search) == 44 do
      pubkey = search

      [[uid, sigDate]] =
        DB.match(
          db_global_iindex,
          {:_, %{pub: search, uid: :"$1", written_on: :"$2"}}
        )

      res = %{
        pubkey: pubkey,
        uid: uid,
        sigDate: sigDate,
        memberships: mindex_search(db_global_mindex, pubkey)
      }

      Poison.encode!(res, pretty: true)
    else
      uid = search

      [[pubkey, sigDate]] =
        DB.match(
          db_global_iindex,
          {:_, %{pub: :"$1", uid: search, written_on: :"$2"}}
        )

      res = %{
        pubkey: pubkey,
        uid: uid,
        sigDate: sigDate,
        memberships: mindex_search(db_global_mindex, pubkey)
      }

      Poison.encode!(res, pretty: true)
    end
  end
end
