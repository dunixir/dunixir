defmodule BMA.Blockchain.Difficulties do
   @moduledoc """
Function related to the difficuelties
  """

    @doc """
GET hardship level for all member's uid for writing next block.
Return The respective difficulty of each member in the last IssuersFrame blocks for current block.

  ## Parameters
 - None
  ## Examples
      iex> BMA.Blockchain.Difficulties.get
  """
  def get do
    Poison.encode!("#TODO")
  end
end
