defmodule BMA.Blockchain.Blocks do
  @moduledoc """
  Function related to multiple blocks
  """

  @doc """
  Get N blocks (order by number) from a start number (inclusive)

  ## Parameters
    - count_string : Number of blocks to get
    - from_string : number of the first block to get

  ## Examples
      iex> BMA.Blockchain.Blocks.get("10", "1")
  """
  def get(count_string, from_string) do
    {count, _} = :string.to_integer(count_string)
    {from, _} = :string.to_integer(from_string)

    result =
      Enum.map(from..(from + count - 1), fn n ->
        case DB.lookup('block', n) do
          [] -> nil
          [{n, block}] -> block
        end
      end)

    case hd(result) do
      nil -> {:ko, JSON.encode!([])}
      _ -> {:ok, JSON.encode!(blocks: result)}
    end
  end
end
