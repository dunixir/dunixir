defmodule BMA.Blockchain.Hardship do
  @moduledoc """
  Module to calculate the hardship for a specific issuer.
  Implementation based on https://duniter.org//wiki/duniter/2018-11-27-duniter-proof-of-work/
  """

  @doc """
  Calculate and return the hardship (difficulty to author the next block) of the issuer given in search with its public key
  Return The hardship value (level) + block number.
  """
  def get(search, db_global_bindex \\ 'global_bindex') do
    # last block of the blockchain
    [{:last_block, last_block_id}] = DB.lookup(db_global_bindex, :last_block)
    [{^last_block_id, last_block}] = DB.lookup(db_global_bindex, last_block_id)

    powMin = last_block[:powMin]
    exFact = getExFact(last_block, search, db_global_bindex)
    handicap = getHandicap(last_block, search, db_global_bindex)

    Poison.encode!(getHardship(exFact, powMin, handicap, last_block_id + 1))
  end

  # return the hardship map
  defp getHardship(exFact, powMin, handicap, next_block) do
    # level of the member
    level = trunc(powMin * exFact + handicap)

    %{
      block: next_block,
      level: level
    }
  end

  # return the exclusion factor value with the last block of the blockchain (last_block) for the searched public key (search)
  defp getExFact(last_block, search, global_bindex) do
    case DB.match(global_bindex, {:"$1", %{issuer: search}}) do
      [] ->
        1

      block_ids_issued_by_searched_member = [_ | _] ->
        # exclusion factor calculation
        last_block_id_issued_by_searched_member =
          block_ids_issued_by_searched_member
          |> Enum.flat_map(fn a -> a end)
          |> Enum.sort()
          |> List.last()

        [{^last_block_id_issued_by_searched_member, last_block_issued_by_searched_member}] =
          DB.lookup(global_bindex, last_block_id_issued_by_searched_member)

        nbPreviousIssuers = last_block_issued_by_searched_member.issuersCount
        nbBlocksSince = last_block.number - last_block_issued_by_searched_member.number
        Enum.max([1, Float.floor(0.67 * nbPreviousIssuers / (1 + nbBlocksSince))])
    end
  end

  # return the handicap value with the last block of the blockchain (last_block) and the issuer (pubKey)
  defp getHandicap(last_block, pubKey, global_bindex) do
    window_size = last_block.issuersFrame
    blocks_frame = getBlocksFrame(window_size, last_block.number, global_bindex)
    nbPersonalBlocksFrame = nbBlocksWithPubKey(blocks_frame, pubKey)
    medianPubKeyFrame = calculateMedianPubKey(blocks_frame)

    Float.floor(
      :math.log2(Enum.max([1, (nbPersonalBlocksFrame + 1) / medianPubKeyFrame])) /
        :math.log2(1.189)
    )
  end

  # return a list of blocks in a specified window size (count) from from_key, the key of the first block
  defp getBlocksFrame(window_size, from_key, global_bindex) do
    Enum.map(from_key..(from_key - window_size + 1), fn n ->
      case DB.lookup(global_bindex, n) do
        [] -> nil
        [{_n, block}] -> block
      end
    end)
  end

  # return the number of occurrences the public key (pubkey) appears in the list of blocks (blocks)
  defp nbBlocksWithPubKey(blocks, pubkey) do
    Enum.reduce(blocks, 0, fn block, acc ->
      if(block.issuer == pubkey) do
        acc + 1
      else
        acc
      end
    end)
  end

  # calculate the occurence number of the different public keys and return the median number
  defp calculateMedianPubKey(blocks) do
    nbBlocksByKey =
      Enum.reduce(blocks, %{}, fn block, acc ->
        {_old_value, new_acc} =
          Map.get_and_update(acc, block.issuer, fn current_value ->
            if current_value == nil do
              {current_value, 1}
            else
              {current_value, current_value + 1}
            end
          end)

        new_acc
      end)

    # pubKey list structure :
    # [
    # {"public key value 1", number of occurences} ,
    # {"public key value 2", number of occurences}
    # ]
    pubKeyList = Map.to_list(nbBlocksByKey)

    # sort the list by ascending number of occurences
    sorted_list = Enum.sort(pubKeyList, &(elem(&1, 1) <= elem(&2, 1)))

    nbElt = Enum.count(sorted_list)

    if(rem(nbElt, 2) == 0) do
      # if the number of elements in the list of occurences is even
      # return the average of the 2 central element occurence of the list
      first = Enum.at(sorted_list, trunc(nbElt / 2) - 1)
      sec = Enum.at(sorted_list, trunc(nbElt / 2))
      ceil(1 / 2 * (elem(first, 1) + elem(sec, 1)))
    else
      # if the number of elements in the list of occurences is odd
      # return the central element occurence of the list
      indice = trunc(nbElt / 2)
      elem(Enum.at(sorted_list, indice), 1)
    end
  end
end
