defmodule BMA.Network.Peers do
     @moduledoc """
Function related to the  peers.
  """

    @doc """
GET the exhaustive list of peers known by the node.
List of peering entries.
  ## Parameters
 - None
  ## Examples
      iex> BMA.Network.Peers.get
  """
  def get do
    Poison.encode!("#TODO")
  end
end
