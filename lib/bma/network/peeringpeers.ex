defmodule BMA.Network.Peering.Peers do
     @moduledoc """
Function related to peering entries of every node inside the currency network.
  """

    @doc """
Merkle URL refering to peering entries of every node inside the currency network.
Return : Merkle URL result.
  ## Parameters
 - None
  ## Examples
      iex> BMA.Network.Peering.Peers.get
  """
  def get do
    Poison.encode!("#TODO")
  end
end
