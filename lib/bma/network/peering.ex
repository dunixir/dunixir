defmodule BMA.Network.Peering do
     @moduledoc """
Function related to the  peering.
  """

    @doc """
GET the peering informations of this node.
Return : Peering entry of the node.
  ## Parameters
 - None
  ## Examples
      iex> BMA.Network.Peering.get
  """
  def get do
    Poison.encode!("#TODO")
  end
end
