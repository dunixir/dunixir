defmodule BMA.Wot.Members do
  @moduledoc """
  Function related to the Trust members.
  """

  @doc """
  GET the list of current Web of Trust members.
  Returns : A list of public key + uid.
  ## Parameters
  - None
  ## Examples
  iex> BMA.Wot.Members.get()
  """
  def get(db \\ 'global_iindex') do
    # Requête pour recupérer les membres actuelles sur le IIndex
    entries = DB.match(db, {:_, :"$2"}) |> Enum.map(fn x -> List.first(x) end)

    # On récupère la dernière occurence des éléments de Iindex par pub
    unique_pubkeys =
      entries
      |> Enum.reduce([], fn entry, acc -> [Map.get(entry, :pub) | acc] end)
      |> MapSet.new()

    reversed_entries = Enum.reverse(entries)

    entries_last =
      Enum.reduce(unique_pubkeys, [], fn pub, acc ->
        [Enum.find(reversed_entries, fn entry -> Map.get(entry, :pub) == pub end) | acc]
      end)

    list_id =
      Enum.reduce(entries_last, [], fn entry, acc ->
        case Map.get(entry, :member) do
          true ->
            [
              %{
                pubKey: Map.get(entry, :pub),
                uid: Map.get(entry, :uid)
              }
              | acc
            ]

          false ->
            acc
        end
      end)

    :dets.close(:iindex)
    Poison.encode!(%{result: list_id}, pretty: true)
  end
end
