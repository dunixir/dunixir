defmodule BMA.Wot.CertifiersOf do
      @moduledoc """
Function related to Certification data over a member.
  """

require Logger

  defp get_id_entry(pub, entries_id) do
    Enum.reduce(entries_id, nil, fn entry, acc ->
      case Map.get(entry, :pub) == pub do
        true -> entry
        false -> acc
      end
    end)
  end
    @doc """
GET Certification data over a member.
Returns : A list of certifications issued to the member by other members (or who used to be members), with written data indicating wether the certification is written in the blockchain or not.

Each certification also has:

    - a isMember field to indicate wether the issuer of the certification is still a member or not.
    - a written field to indicate the block where the certification was written (or null if not written yet).

  ## Parameters
 - search : Public key or uid of a member (or someone who was a member) we want see the certifications.
  """
  def get(
        search,
        db_id \\ 'global_iindex',
        db_cert \\ 'global_cindex',
        db_block \\ 'global_bindex'
      ) do
    entries_id = DB.match(db_id, {:_, :"$2"}) |> Enum.map(fn x -> List.first(x) end)

    entries_cert = DB.match(db_cert, {:_, :"$2"}) |> Enum.map(fn x -> List.first(x) end)

    entries_block = DB.match(db_block, {:_, :"$2"}) |> Enum.map(fn x -> List.first(x) end)

    # entry de iindex correspondant à search
    entry_id =
      Enum.reduce(entries_id, nil, fn entry, acc ->
        case Map.get(entry, :pub) == search or Map.get(entry, :id) == search do
          true -> entry
          false -> acc
        end
      end)

    # Logger.info("Entry_id : #{inspect(entry_id)}")
    # liste des certifications dont search est le receiver
    certs =
      Enum.filter(entries_cert, fn cert -> Map.get(cert, :receiver) == Map.get(entry_id, :pub) end)

    # Logger.info("certs : #{inspect(certs)}")

    # creation de la liste du champ "certifications" de la requete
    list_certs =
      Enum.reduce(certs, [], fn cert, acc ->
        # entry de iindex correspondant au issuer de la certification
        entry_id_ = get_id_entry(Map.get(cert, :issuer), entries_id)
        [number, hash] = String.split(Map.get(cert, :written_on), "-")
        # Logger.info("Entry_id_ : #{inspect(entry_id_)}")

        # numéro de block sur lequel est crée la certification
        block_cert_time =
          String.split(Map.get(cert, :created_on), "-") |> List.first() |> String.to_integer()

        # medianTime block sur lequel est crée la certification
        medianTime =
          Enum.reduce(entries_block, nil, fn block, acc ->
            case Map.get(block, :number) == block_cert_time do
              true -> Map.get(block, :medianTime)
              _ -> acc
            end
          end)

        new_acc = [
          %{
            pubKey: Map.get(cert, :issuer),
            uid: Map.get(entry_id_, :uid),
            isMember: Map.get(entry_id_, :member),
            wasMember: Map.get(entry_id_, :wasMember),
            sigDate: Map.get(entry_id_, :written_on),
            written: %{number: String.to_integer(number), hash: hash},
            cert_time: %{block: block_cert_time, medianTime: medianTime},
            signature: Map.get(cert, :sig)
          }
          | acc
        ]

        # Logger.info("#{inspect(new_acc)}")
        new_acc
      end)

    map_to_return = %{
      pubKey: Map.get(entry_id, :pub),
      uid: Map.get(entry_id, :uid),
      sigDate: Map.get(entry_id, :written_on),
      isMember: Map.get(entry_id, :member),
      certifications: list_certs
    }

    Poison.encode(map_to_return, pretty: true)
  end
end
