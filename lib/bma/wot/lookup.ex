defmodule BMA.Wot.Lookup do
     @moduledoc """
Function related to Public key data.
  """

    @doc """
GET Public key data.
Returns : A list of public key data matching search string (may not return all results, check partial flag which is set to false if all results are here, true otherwise).
  ## Parameters
 - search : A string of data to look for (public key, uid).
  """
  def get(search) do
    Poison.encode!("#TODO " <> search)
  end
end
