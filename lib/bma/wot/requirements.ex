defmodule BMA.Wot.Requirements do
    @moduledoc """
Function related to requirements to become a member.
  """

    @doc """
GET requirements to be filled by pubkey to become a member.
Returns : A list of identities matching this pubkey and the requirement
If the pubkey is matching a member, only one identity may be displayed: the one which is a member.
  ## Parameters
 - search : Public key to check.
  """
  def get(search) do
    Poison.encode!("#TODO " <> search)
  end
end
