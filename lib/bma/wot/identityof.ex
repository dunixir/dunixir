defmodule BMA.Wot.IdentityOf do
  @moduledoc """
Function related to identity data.
  """

  @doc """
GET identity data written for a member.
Returns : Identity data written in the blockchain.
    ## Parameters
 - search : Public key or uid of a member we want see the attached identity.
  """
  def get(search) do
    Poison.encode!("#TODO " <> search)
  end
end
