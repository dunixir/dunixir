defmodule BMA.Wot.CertifiersBy do
  @moduledoc """
Function related to certification data over a member.
  """

  @doc """
GET Certification data over a member.
Returns : A list of certifications issued by the member to other members (or who used to be members), with written data indicating wether the certification is written in the blockchain or not.

Each certification also has:

    - a isMember field to indicate wether the issuer of the certification is still a member or not.
    - a written field to indicate the block where the certification was written (or null if not written yet).

    ## Parameters
 - search : Public key or uid of a member (or someone who was a member) we want see the certifications.
  """
  def get(search) do
    Poison.encode!("#TODO " <> search)
  end
end
