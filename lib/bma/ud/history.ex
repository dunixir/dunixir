defmodule BMA.Ud.History do
     @moduledoc """
Function related to wallet universal dividend.
  """

    @doc """
Get the wallet universal dividend history
Return : The universal dividend history for the given pubkey and between the given from and to blocks.
  ## Parameters
 - pubkey (URL) : Wallet public key.
  """
  def get(pubkey) do
    Poison.encode!("#TODO")
  end
end
