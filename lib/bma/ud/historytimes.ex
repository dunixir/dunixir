defmodule BMA.Ud.History.Times do
     @moduledoc """
Function related to  wallet universal dividend.
  """

    @doc """
Get the  wallet universal dividend
Return : The universal dividend for the given pubkey and between the given from and to dates.
  ## Parameters
 - pubkey (URL) : Wallet public key.
 - from : The starting timestamp limit. (optionnal)
 - to : The ending timestamp. (optionnal)
  """
  def get(pubkey, from, to) do
    Poison.encode!("#TODO")
  end
end
