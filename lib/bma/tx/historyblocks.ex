defmodule BMA.Tx.History.Blocks do
     @moduledoc """
Function related to wallet transaction history.
  """

    @doc """
Get the wallet transaction history
Return : The transaction history for the given pubkey and between the given from and to blocks.
  ## Parameters
 - pubkey (URL) : Wallet public key.
 - from : The starting block.
 - to : The ending block.
  """
  def get(pubkey, from, to) do
    Poison.encode!("#TODO")
  end
end
