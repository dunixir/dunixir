defmodule BMA.Tx.History.Times do
     @moduledoc """
Function related to wallet transaction history.
  """

    @doc """
Get the wallet transaction history
Return : The transaction history for the given pubkey and between the given from and to dates.
  ## Parameters
 - pubkey (URL) : Wallet public key.
 - from : The starting timestamp limit. (optionnal)
 - to : The ending timestamp. (optionnal)
  """
  def get(pubkey, from, to) do
    Poison.encode!("#TODO")
  end
end
