defmodule BMA.Tx.Sources do
     @moduledoc """
Function related to available sources.
  """
    @doc """
GET a list of available sources.
Return : A list of available sources for the given pubkey.
  ## Parameters
 - Pubkey (URL) : Owner of the coins' pubkey.
  """
  def get(pubkey) do
    Poison.encode!("#TODO")
  end
end
