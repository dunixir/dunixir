defmodule BMA.Tx.History do
     @moduledoc """
Function related to wallet transaction history.
  """

    @doc """
Get the wallet transaction history
Return : The full transaction history for the given pubkey.
  ## Parameters
 - pubkey (URL) : Wallet public key.
  """
  def get(pubkey) do
    Poison.encode!("#TODO")
  end
end
