defmodule BMA.Tx.Pending do
     @moduledoc """
Function related to peering entry document.
  """
    @doc """
POST a peering entry document.
Return : The posted entry.
  ## Parameters
 - peer (URL) : The peering entry document.
  """
  def get do
    Poison.encode!("#TODO")
  end
end
