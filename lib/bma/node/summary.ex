defmodule BMA.Node.Summary do
 @moduledoc """
Function related to Technical informations about identities, membership and transactions sandboxes..
  """
  @doc """
GET filling and capacity of indentities, membership and transactions sandboxes of the requested peer.
Return : Technical informations about identities, membership and transactions sandboxes.
  ## Parameters
    - None
  ## Examples
      iex> BMA.Node.Summary.get
  """
  def get do
    Poison.encode!(%{
      duniter: %{
        software: "duniter",
        version: Constants.version(),
        forkWindowSize: Constants.forkWindowSize()
      }
    })
  end
end
