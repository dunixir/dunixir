defmodule BMA.Node.Sandboxes do
 @moduledoc """
Function related to technical informations about peers.
  """
  @doc """
GET technical informations about this peer.
Return : Technical informations about the node.
  ## Parameters
    - None
  ## Examples
      iex> BMA.Node.Sandboxes.get
  """
  def get do
    Poison.encode!("#TODO")
  end
end
