defmodule DB.Create do
  @moduledoc """
  Supervisor that manages the different databases.

  Creates and restart them when needed.
  """
  use Supervisor

  def start_link(_) do
    # Supervisor.child_spec : used to name the process from the point of view of the supervisor.
    # Without that, they all have the same name and the application crashes.
    children = [
      Supervisor.child_spec({DB, ['global_iindex']}, id: :global_iindex),
      Supervisor.child_spec({DB, ['global_bindex']}, id: :global_bindex),
      Supervisor.child_spec({DB, ['global_mindex']}, id: :global_mindex),
      Supervisor.child_spec({DB, ['global_cindex']}, id: :global_cindex),
      Supervisor.child_spec({DB, ['global_sindex']}, id: :global_sindex),
      Supervisor.child_spec({DB, ['block']}, id: :block),
      Supervisor.child_spec({DB, ['wallet']}, id: :wallet),
      Supervisor.child_spec({DB, ['meta']}, id: :meta),
      Supervisor.child_spec({DB, ['peer']}, id: :peer)
    ]

    opts = [strategy: :one_for_one, name: DB.Supervisor]

    Supervisor.start_link(children, opts)
  end
end
