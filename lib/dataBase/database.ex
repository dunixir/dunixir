defmodule DB do
  @moduledoc """
  :dets wrapper to skirt its 2GB limitation.
  Is is possible to instantiate several database by passing the name as a charlist to start_link function.
  """

  @doc """
  Insert a (key, value) object in the given database.

  ## Parameters

    - db: charlist to name the database : string with '',
    - key: key of the :dets entry,
    - value: value of the :dets entry.
  """
  require Logger

  @base_url 'https://g1-test.duniter.org'

  def insert(db, key, value) do
    GenServer.cast(:"DB_#{db}", {:insert, key, value})
  end

  @doc """
  Delete a :dets entry given its key.
  """
  def delete(db, key) do
    GenServer.cast(:"DB_#{db}", {:delete, key})
  end

  @doc """
  Look for a :dets entry, given its key.

  Returns the object associated to the key.
  """
  def lookup(db, key) do
    GenServer.call(:"DB_#{db}", {:lookup, key})
  end

  @doc """
  Look for matches in a database, given a pattern.
  See [:dets doc](https://www.erlang.org/doc/man/dets.html#match-2) for more information about this matching.

  Returns a list of matched objects.
  """
  def match(db, pattern) do
    GenServer.call(:"DB_#{db}", {:match, pattern})
  end

  @doc """
  Applies Fun to each object stored in table Name in some unspecified order.
  Different actions are taken depending on the return value of Fun.
  See [:dets doc](https://www.erlang.org/doc/man/dets.html#traverse-2) for more information about this.

  Returns a list of selected objects.
  """
  def traverse(db, function) do
    GenServer.call(:"DB_#{db}", {:traverse, function})
  end

  @doc """
  Returns the size of the given database
  """
  def size(db) do
    GenServer.call(:"DB_#{db}", :size)
  end

  @doc """
  Delete all the files of the database.

  This is implemented for test purpose only.
  """
  def delete_db(db) do
    GenServer.cast(:"DB_#{db}", :delete_all_db)
  end



  use GenServer, restart: :transient

  def start_link([dbName]) do
    GenServer.start_link(__MODULE__, dbName, name: :"DB_#{dbName}")
  end

  def init(dbName) do
    # Creating database directory if it doesnét exist
    File.mkdir_p('data/' ++ dbName)

    # If the given database already exists, GenServer must know the number of existing files
    dbNumber = Path.wildcard('data/' ++ dbName ++ '/*') |> Enum.count()

    dbNumber =
      if dbNumber == 0 do
        1
      else
        dbNumber
      end

    # If the database doesn't exists, creating at least one file.
    fileName = 'data/' ++ dbName ++ '/' ++ to_charlist(dbNumber)
    {:ok, db} = :dets.open_file(:"#{fileName}", [{:file, fileName}, {:type, :set}])
    :dets.close(db)

    if dbName == 'block' do
      spawn(fn -> fill_with_block() end)
    end


    {:ok, {dbName, dbNumber}}
  end

  def handle_cast({:insert, key, value}, {dbName, dbNumber}) do
    # Get database size, to know if a new file must be created
    fileName = 'data/' ++ dbName ++ '/' ++ to_charlist(dbNumber)
    {:ok, db} = :dets.open_file(:"#{fileName}", [{:file, fileName}, {:type, :set}])
    dbSize = :dets.info(db)[:size]
    :dets.close(db)

    # Search database : if entry already exists, we replace it
    existsInFile =
      Path.wildcard('data/' ++ dbName ++ '/*')
      |> Enum.find(fn file ->
        {:ok, db} = :dets.open_file(:"#{file}", [{:file, to_charlist(file)}, {:type, :set}])
        elt = :dets.lookup(db, key)
        :dets.close(db)

        elt != []
      end)

    {newFile, fileName} =
      cond do
        # If the entry already exists, the file to open is the one found earlier, and we don't create new file.
        existsInFile != nil ->
          {0, to_charlist(existsInFile)}

        # The size of a :dets table is limited to 500_000 entries
        dbSize < 500_000 ->
          {0, 'data/' ++ dbName ++ '/' ++ to_charlist(dbNumber)}

        # If the number of entries is too big, a nex file is created
        true ->
          {1, 'data/' ++ dbName ++ '/' ++ to_charlist(dbNumber + 1)}
      end

    # True insertion at the right place
    {:ok, db} = :dets.open_file(:"#{fileName}", [{:file, fileName}, {:type, :set}])
    :dets.insert(db, {key, value})
    :dets.close(db)

    {:noreply, {dbName, dbNumber + newFile}}
  end

  def handle_cast({:delete, key}, {dbName, dbNumber}) do
    # Search in all possible :dets files the element to delete and delete it when found
    Path.wildcard('data/' ++ dbName ++ '/*')
    |> Enum.map(fn file ->
      {:ok, db} = :dets.open_file(:"#{file}", [{:file, to_charlist(file)}, {:type, :set}])
      :dets.delete(db, key)
      :dets.close(db)
    end)

    {:noreply, {dbName, dbNumber}}
  end

  def handle_cast(:delete_all_db, {dbName, dbNumber}) do
    # Remove the database folder, with all the :dets tables
    File.rm_rf('data/' ++ dbName)

    {:stop, :normal, {dbName, dbNumber}}
  end

  def handle_call({:lookup, key}, _from, {dbName, dbNumber}) do
    # Search the key in all possible :dets tables, return it when found
    foundElement =
      Path.wildcard('data/' ++ dbName ++ '/*')
      |> Enum.find_value(fn file ->
        {:ok, db} = :dets.open_file(:"#{file}", [{:file, to_charlist(file)}, {:type, :set}])
        elt = :dets.lookup(db, key)
        :dets.close(db)

        if elt != [] do
          elt
        else
          false
        end
      end)

    # Shaping the function response to match the original :dets response
    foundElement =
      if foundElement == nil do
        []
      else
        foundElement
      end

    {:reply, foundElement, {dbName, dbNumber}}
  end

  def handle_call({:match, pattern}, _from, {dbName, dbNumber}) do
    # Search matches in all possible files, and concatenate all the results
    foundMatches =
      Path.wildcard('data/' ++ dbName ++ '/*')
      |> Enum.reduce([], fn file, acc ->
        {:ok, db} = :dets.open_file(:"#{file}", [{:file, to_charlist(file)}, {:type, :set}])
        matchRes = :dets.match(db, pattern)
        :dets.close(db)

        matchRes ++ acc
      end)

    {:reply, foundMatches, {dbName, dbNumber}}
  end

  def handle_call({:traverse, function}, _from, {dbName, dbNumber}) do
    traverseResult = Path.wildcard('data/' ++ dbName ++ '/*')
    |> Enum.reduce([], fn file, acc ->
      {:ok, db} = :dets.open_file(:"#{file}", [{:file, to_charlist(file)}, {:type, :set}])
      res = :dets.traverse(db, function)
      :dets.close(db)

      res ++ acc
    end)

    {:reply, traverseResult, {dbName, dbNumber}}
  end

  def handle_call(:size, _from, {dbName, dbNumber}) do
    dbSize = get_size(dbName)

    {:reply, dbSize, {dbName, dbNumber}}
  end



  defp fill_with_block() do
    # Getting current block from the remote node of the blockchain
    # currentRemoteBlock = (case :httpc.request(:get, {@base_url ++ '/blockchain/current', []}, [], []) do
    #   {:ok, {{_, 200, 'OK'}, _headers, body}} -> Poison.decode!(body)

    #   _ -> Logger.info('Filling DB : Failing when getting current block')
    # end)["number"]
    currentRemoteBlock = 49

    dbSize = get_size('block')
    currentLocalBlock = if dbSize > currentRemoteBlock do
      currentRemoteBlock
    else
      dbSize
    end

    currentLocalBlock..currentRemoteBlock # TODO : Fetch all the blocks
    |> Enum.chunk_every(5000)
    |> Enum.map(fn chk ->
      # Putting the new blocks in the local database
      case :httpc.request(:get, {@base_url ++ '/blockchain/blocks/' ++ to_charlist(length(chk)) ++ '/' ++ to_charlist(List.first(chk)), []}, [], []) do
        {:ok, {{_, 200, 'OK'}, _headers, '[]'}} ->
          Logger.info('Filling DB : No blocks to get from remote g1-test node')

        {:ok, {{_, 200, 'OK'}, _headers, body}} ->
          Logger.info('Filling DB : Successfully got blocks '
            ++ to_charlist(List.first(chk))
            ++ ' to '
            ++ to_charlist(List.last(chk))
            ++ ' from '
            ++ @base_url)
          Poison.decode!(body) |> Enum.map(fn b -> DB.insert('block', b["number"], b); :timer.sleep(50) end)

        _ ->
          Logger.info('---Failed when filling DataBase---')
      end

      :timer.sleep(1000)
    end)
  end

  defp get_size(dbName) do
    Path.wildcard('data/' ++ dbName ++ '/*')
    |> Enum.reduce(0, fn file, acc ->
      {:ok, db} = :dets.open_file(:"#{file}", [{:file, to_charlist(file)}, {:type, :set}])

      # :dets.info returns (example) [type: :set, keypos: 1, size: 50, file_size: 138552, filename: 'data/block']
      info = :dets.info(db)
      :dets.close(db)

      acc + info[:size]
    end)
  end
end
