defmodule Certification.Augmentation.CertificationPeriodTest do
  @moduledoc """
  Test of validation rule BR_G67
  """
  use ExUnit.Case
  doctest Index.Augmentation.CIndex

  setup_all do
    local_cindex = :ets.new(:"test/local_cindex", [:set, :protected])
    cpTestEmpty = Index.Augmentation.CIndex.certificationPeriod(local_cindex)
    :ets.insert(local_cindex, {0, %{unchainables: 0}})
    :ets.insert(local_cindex, {1, %{unchainables: 0}})
    cpTest0 = Index.Augmentation.CIndex.certificationPeriod(local_cindex)

    :ets.insert(local_cindex, {2, %{unchainables: 1}})
    cpTest1 = Index.Augmentation.CIndex.certificationPeriod(local_cindex)

    {:ok, cpTestEmpty: cpTestEmpty, cpTest0: cpTest0, cpTest1: cpTest1}
  end

  test "BR_G67 - Certification Period", state do
    assert state.cpTestEmpty
    assert state.cpTest0
    assert !state.cpTest1
  end
end
