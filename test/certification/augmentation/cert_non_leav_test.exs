defmodule Certification.Augmentation.CertNonLeavTest do
  @moduledoc """
  Test of validation rule BR_G70
  """
  use ExUnit.Case

  @certNonLeaverValid %{
    op: 'CREATE',
    issuer: "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
    receiver: "mMPioknj2MQCX9KyKykdw8qMRxYR2w1u3UpdiEJHgXg",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    written_on: "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
    sig:
      "wqZxPEGxLrHGv8VdEIfUGvUcf+tDdNTMXjLzVRCQ4UhlhDRahOMjfcbP7byNYr5OfIl83S1MBxF7VJgu8YasCA==",
    expires_on: 1_489_677_041,
    chainable_on: 1_489_677_041,
    replayable_on: 1_489_677_041,
    expired_on: 0,
    toLeaver: false
  }

  @certNonLeaverInvalid %{
    op: 'CREATE',
    issuer: "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
    receiver: "mMPioknj2MQCX9KyKykdw8qMRxYR2w1u3UpdiEJHgXg",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0B",
    written_on: "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
    sig:
      "wqZxPEGxLrHGv8VdEIfUGvUcf+tDdNTMXjLzVRCQ4UhlhDRahOMjfcbP7byNYr5OfIl83S1MBxF7VJgu8YasCA==",
    expires_on: 1_489_677_041,
    chainable_on: 1_489_677_041,
    replayable_on: 1_489_677_041,
    expired_on: 0,
    toLeaver: true
  }

  @certNonLeaverWithoutToLeaver %{
    op: 'CREATE',
    issuer: "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
    receiver: "mMPioknj2MQCX9KyKykdw8qMRxYR2w1u3UpdiEJHgXg",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0B",
    written_on: "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
    sig:
      "wqZxPEGxLrHGv8VdEIfUGvUcf+tDdNTMXjLzVRCQ4UhlhDRahOMjfcbP7byNYr5OfIl83S1MBxF7VJgu8YasCA==",
    expires_on: 1_489_677_041,
    chainable_on: 1_489_677_041,
    replayable_on: 1_489_677_041,
    expired_on: 0
  }

  doctest Index.Augmentation.CIndex

  setup_all do
    # test with a toLeaver true
    local_cindex = :ets.new(:"test/local_cindex", [:set, :protected])
    :ets.insert(local_cindex, {0, @certNonLeaverValid})
    certToNonLeaverOneEltTrue = Index.Augmentation.CIndex.certificationToNonLeaver(local_cindex)
    :file.delete("test/local_cindex")

    # test with a toLeaver true
    local_cindex = :ets.new(:"test/local_cindex", [:set, :protected])
    :ets.insert(local_cindex, {0, @certNonLeaverValid})
    :ets.insert(local_cindex, {1, @certNonLeaverValid})
    :ets.insert(local_cindex, {2, @certNonLeaverWithoutToLeaver})
    certToNonLeaverManyEltTrue = Index.Augmentation.CIndex.certificationToNonLeaver(local_cindex)
    :file.delete("test/local_cindex")

    # test with a toLeaver true
    local_cindex = :ets.new(:"test/local_cindex", [:set, :protected])
    :ets.insert(local_cindex, {0, @certNonLeaverWithoutToLeaver})

    certToNonLeaverOneEltWithoutFieldTrue =
      Index.Augmentation.CIndex.certificationToNonLeaver(local_cindex)

    :file.delete("test/local_cindex")

    # test with a toLeaver false
    local_cindex = :ets.new(:"test/local_cindex", [:set, :protected])
    :ets.insert(local_cindex, {0, @certNonLeaverInvalid})
    certToNonLeaverOneEltFalse = Index.Augmentation.CIndex.certificationToNonLeaver(local_cindex)
    :file.delete("test/local_cindex")

    # test with a toLeaver false
    local_cindex = :ets.new(:"test/local_cindex", [:set, :protected])
    :ets.insert(local_cindex, {0, @certNonLeaverWithoutToLeaver})
    :ets.insert(local_cindex, {1, @certNonLeaverInvalid})
    :ets.insert(local_cindex, {2, @certNonLeaverValid})
    certToNonLeaverManyEltFalse = Index.Augmentation.CIndex.certificationToNonLeaver(local_cindex)
    :file.delete("test/local_cindex")

    {:ok,
     certToNonLeaverOneEltTrue: certToNonLeaverOneEltTrue,
     certToNonLeaverManyEltTrue: certToNonLeaverManyEltTrue,
     certToNonLeaverOneEltWithoutFieldTrue: certToNonLeaverOneEltWithoutFieldTrue,
     certToNonLeaverOneEltFalse: certToNonLeaverOneEltFalse,
     certToNonLeaverManyEltFalse: certToNonLeaverManyEltFalse}
  end

  test "check certNonLeaver", state do
    assert state.certToNonLeaverOneEltTrue == true
    assert state.certToNonLeaverManyEltTrue == true
    assert state.certToNonLeaverOneEltWithoutFieldTrue == true
    assert state.certToNonLeaverOneEltFalse == false
    assert state.certToNonLeaverManyEltFalse == false
  end
end
