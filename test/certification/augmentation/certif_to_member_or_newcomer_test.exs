defmodule Certification.Augmentation.CertifToMemberOrNewcomerTest do
  @moduledoc """
  Test of validation rule BR_G69
  """
  use ExUnit.Case
  doctest Index.Augmentation.CIndex

  setup_all do
    local_cindex1 = :ets.new(:"test/local_cindex1", [:set, :protected])
    :ets.insert(local_cindex1, {0, %{receiver: "XYZ_pkey", toMember: true, toNewcomer: false}})
    :ets.insert(local_cindex1, {1, %{receiver: "XYA_pkey", toMember: false, toNewcomer: true}})
    :ets.insert(local_cindex1, {2, %{receiver: "XYE_pkey", toMember: false, toNewcomer: true}})
    :ets.insert(local_cindex1, {3, %{receiver: "XYR_pkey", toMember: true}})
    :ets.insert(local_cindex1, {4, %{receiver: "XYT_pkey", toNewcomer: true}})
    true1 = Index.Augmentation.CIndex.certifToMemberOrNewcomer(local_cindex1)
    :file.delete("test/local_cindex1")

    local_cindex2 = :ets.new(:"test/local_cindex2", [:set, :protected])
    :ets.insert(local_cindex2, {0, %{receiver: "XYZ_pkey", toMember: true, toNewcomer: false}})
    :ets.insert(local_cindex2, {1, %{receiver: "XYA_pkey", toMember: false, toNewcomer: false}})
    :ets.insert(local_cindex2, {2, %{receiver: "XYE_pkey", toMember: false, toNewcomer: true}})
    :ets.insert(local_cindex2, {3, %{receiver: "XYR_pkey", toMember: true}})
    :ets.insert(local_cindex2, {4, %{receiver: "XYT_pkey", toNewcomer: true}})
    false1 = Index.Augmentation.CIndex.certifToMemberOrNewcomer(local_cindex2)
    :file.delete("test/local_cindex2")

    local_cindex3 = :ets.new(:"test/local_cindex3", [:set, :protected])
    :ets.insert(local_cindex3, {0, %{receiver: "XYZ_pkey", toMember: true, toNewcomer: false}})
    :ets.insert(local_cindex3, {1, %{receiver: "XYA_pkey"}})
    :ets.insert(local_cindex3, {2, %{receiver: "XYE_pkey", toMember: false, toNewcomer: true}})
    :ets.insert(local_cindex3, {3, %{receiver: "XYR_pkey", toMember: true}})
    :ets.insert(local_cindex3, {4, %{receiver: "XYT_pkey", toNewcomer: true}})
    false2 = Index.Augmentation.CIndex.certifToMemberOrNewcomer(local_cindex3)
    :file.delete("test/local_cindex3")

    {:ok, true1: true1, false1: false1, false2: false2}
  end

  test "check certifToMemberOrNewcomer", state do
    assert state.true1 == true
    assert state.false1 == false
    assert state.false2 == false
  end
end
