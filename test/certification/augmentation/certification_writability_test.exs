defmodule Certification.Augmentation.CertificationWritabilityTest do
  @moduledoc """
  Test of validation rule BR_G65
  """
  use ExUnit.Case
  doctest Index.Augmentation.CIndex

  setup_all do
    local_cindex = :ets.new(:"test/local_cindex", [:set, :protected])
    cwTestEmpty = Index.Augmentation.CIndex.certificationWritability(local_cindex)
    :ets.insert(local_cindex, {0, %{age: Constants.Contract.sigWindow() - 10}})
    :ets.insert(local_cindex, {1, %{age: Constants.Contract.sigWindow() - 20}})
    cwTest0 = Index.Augmentation.CIndex.certificationWritability(local_cindex)
    :ets.insert(local_cindex, {2, %{age: Constants.Contract.sigWindow() + 10}})
    cwTest1 = Index.Augmentation.CIndex.certificationWritability(local_cindex)

    {:ok, cwTestEmpty: cwTestEmpty, cwTest0: cwTest0, cwTest1: cwTest1}
  end

  test "BR_G65 - Certification Writability", state do
    assert state.cwTestEmpty
    assert state.cwTest0
    assert !state.cwTest1
  end
end
