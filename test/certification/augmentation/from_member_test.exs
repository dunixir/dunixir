defmodule Certification.Augmentation.FromMemberTest do
  @moduledoc """
  Test of validation rule BR_G40
  """
  use ExUnit.Case
  doctest Index.Augmentation.CIndex

  setup_all do
    local_cindex = :ets.new(:"test/local_cindex", [:set, :protected])
    DB.start_link(['global_iindex_test_brg40'])

    :ets.insert(local_cindex, {0, %{issuer: "XYZ_pkey"}})
    DB.insert('global_iindex_test_brg40', 0, %{member: nil, pub: "XYZ_pkey"})
    DB.insert('global_iindex_test_brg40', 1, %{member: true, pub: "XYZ_pkey"})
    DB.insert('global_iindex_test_brg40', 2, %{member: false, pub: "XYA_pkey"})
    DB.insert('global_iindex_test_brg40', 3, %{member: nil, pub: "XYZ_pkey"})
    Index.Augmentation.CIndex.fromMember(0, local_cindex, 'global_iindex_test_brg40')
    [{0, entry}] = :ets.lookup(local_cindex, 0)
    fromMemberTrue = entry.fromMember


    local_cindex = :ets.new(:"test/local_cindex", [:set, :protected])

    :ets.insert(local_cindex, {0, %{issuer: "XYZ_pkey"}})
    DB.delete('global_iindex_test_brg40', 0)
    DB.delete('global_iindex_test_brg40', 1)
    DB.delete('global_iindex_test_brg40', 2)
    DB.delete('global_iindex_test_brg40', 3)
    DB.insert('global_iindex_test_brg40', 0, %{member: nil, pub: "XYZ_pkey"})
    DB.insert('global_iindex_test_brg40', 1, %{member: false, pub: "XYZ_pkey"})
    Index.Augmentation.CIndex.fromMember(0, local_cindex, 'global_iindex_test_brg40')
    [{0, entry}] = :ets.lookup(local_cindex, 0)
    fromMemberFalse = entry.fromMember

    DB.delete_db('global_iindex_test_brg40')

    {:ok, fromMemberTrue: fromMemberTrue, fromMemberFalse: fromMemberFalse}
  end

  test "check fromMember", state do
    assert state.fromMemberTrue == true
    assert state.fromMemberFalse == false
  end
end
