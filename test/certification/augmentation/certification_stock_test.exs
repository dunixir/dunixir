defmodule Certification.Augmentation.CertificationStockTest do
  @moduledoc """
  Test of validation rule BR_G66
  """
  use ExUnit.Case
  doctest Index.Augmentation.CIndex

  setup_all do
    local_cindex = :ets.new(:"test/local_cindex", [:set, :protected])
    cwTestEmpty = Index.Augmentation.CIndex.certificationStock(local_cindex)
    :ets.insert(local_cindex, {0, %{stock: Constants.Contract.sigStock() - 10}})
    :ets.insert(local_cindex, {1, %{stock: Constants.Contract.sigStock() - 20}})
    cwTest0 = Index.Augmentation.CIndex.certificationStock(local_cindex)

    :ets.insert(local_cindex, {2, %{stock: Constants.Contract.sigStock() + 10}})
    cwTest1 = Index.Augmentation.CIndex.certificationStock(local_cindex)

    {:ok, cwTestEmpty: cwTestEmpty, cwTest0: cwTest0, cwTest1: cwTest1}
  end

  test "BR_G66 - Certification Stock", state do
    assert state.cwTestEmpty
    assert state.cwTest0
    assert !state.cwTest1
  end
end
