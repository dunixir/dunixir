defmodule Certification.Augmentation.CertificationFromMemberTest do
  @moduledoc """
  Test for validation rule BR_G68
  """
  use ExUnit.Case
  doctest Index.Augmentation.CIndex

  setup_all do
    local_cindex = :ets.new(:"test/local_cindex", [:set, :protected])
    local_bindex = :ets.new(:"test/local_bindex", [:set, :protected])

    :ets.insert(local_bindex, {0, %{number: 0}})
    cfmTestZero = Index.Augmentation.CIndex.certificationFromMember(local_bindex, local_cindex)

    :ets.insert(local_bindex, {0, %{number: 1}})
    cfmTestEmpty = Index.Augmentation.CIndex.certificationFromMember(local_bindex, local_cindex)

    :ets.insert(local_cindex, {0, %{fromMember: true}})
    :ets.insert(local_cindex, {1, %{fromMember: true}})
    cfmTest0 = Index.Augmentation.CIndex.certificationFromMember(local_bindex, local_cindex)

    :ets.insert(local_cindex, {2, %{fromMember: false}})
    cfmTest1 = Index.Augmentation.CIndex.certificationFromMember(local_bindex, local_cindex)

    {:ok,
     cfmTestZero: cfmTestZero, cfmTestEmpty: cfmTestEmpty, cfmTest0: cfmTest0, cfmTest1: cfmTest1}
  end

  test "BR_G68 - Certification from member", state do
    assert state.cfmTestZero
    assert state.cfmTestEmpty
    assert state.cfmTest0
    assert !state.cfmTest1
  end
end
