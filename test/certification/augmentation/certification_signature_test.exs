defmodule Certification.Augmentation.CertificationSignatureTest do
  @moduledoc """
  Test of validation rule BR_G72
  """
  use ExUnit.Case
  doctest Index.Augmentation.CIndex

  setup_all do
    local_cindex = :ets.new(:"test/local_cindex", [:set, :protected])
    csTestEmpty = Index.Augmentation.CIndex.certificationSignature(local_cindex)
    :ets.insert(local_cindex, {0, %{sigOK: true}})
    :ets.insert(local_cindex, {1, %{sigOK: true}})
    csTest0 = Index.Augmentation.CIndex.certificationSignature(local_cindex)

    :ets.insert(local_cindex, {2, %{sigOK: false}})
    csTest1 = Index.Augmentation.CIndex.certificationSignature(local_cindex)

    {:ok, csTestEmpty: csTestEmpty, csTest0: csTest0, csTest1: csTest1}
  end

  test "BR_G72 - Certification Signature", state do
    assert state.csTestEmpty
    assert state.csTest0
    assert !state.csTest1
  end
end
