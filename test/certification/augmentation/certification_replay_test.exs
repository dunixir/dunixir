defmodule Certification.Augmentation.CertificationReplayTest do
  @moduledoc """
  Test of validation rule BR_G71
  """
  use ExUnit.Case
  doctest Index.Augmentation.CIndex

  setup_all do
    local_cindex = :ets.new(:"test/local_cindex", [:set, :protected])
    crTestEmpty = Index.Augmentation.CIndex.certificationReplay(local_cindex)
    :ets.insert(local_cindex, {0, %{isReplay: false}})
    :ets.insert(local_cindex, {1, %{isReplay: true, isReplayable: true}})
    crTest0 = Index.Augmentation.CIndex.certificationReplay(local_cindex)
    :ets.insert(local_cindex, {2, %{isReplay: true, isReplayable: false}})
    crTest1 = Index.Augmentation.CIndex.certificationReplay(local_cindex)

    {:ok, crTestEmpty: crTestEmpty, crTest0: crTest0, crTest1: crTest1}
  end

  test "BR_G71 - Certification replay", state do
    assert state.crTestEmpty
    assert state.crTest0
    assert !state.crTest1
  end
end
