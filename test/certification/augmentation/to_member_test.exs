defmodule Certification.Augmentation.ToMemberTest do
  @moduledoc """
  Test of validation rule BR_G41
  """
  use ExUnit.Case
  doctest Index.Augmentation.CIndex

  setup_all do
    local_cindex = :ets.new(:"test/local_cindex", [:set, :protected])
    DB.start_link(['global_iindex_test_brg41'])

    :ets.insert(local_cindex, {0, %{issuer: "XYZ_pkey"}})
    DB.insert('global_iindex_test_brg41', 0, %{member: nil, pub: "XYZ_pkey"})
    DB.insert('global_iindex_test_brg41', 1, %{member: true, pub: "XYZ_pkey"})
    DB.insert('global_iindex_test_brg41', 2, %{member: false, pub: "XYA_pkey"})
    DB.insert('global_iindex_test_brg41', 3, %{member: nil, pub: "XYZ_pkey"})
    Index.Augmentation.CIndex.toMember(0, local_cindex, 'global_iindex_test_brg41')
    [{0, entry}] = :ets.lookup(local_cindex, 0)
    toMemberTrue = entry.toMember


    local_cindex = :ets.new(:"test/local_cindex", [:set, :protected])

    :ets.insert(local_cindex, {0, %{issuer: "XYZ_pkey"}})
    DB.delete('global_iindex_test_brg41', 0)
    DB.delete('global_iindex_test_brg41', 1)
    DB.delete('global_iindex_test_brg41', 2)
    DB.delete('global_iindex_test_brg41', 3)
    DB.insert('global_iindex_test_brg41', 0, %{member: nil, pub: "XYZ_pkey"})
    DB.insert('global_iindex_test_brg41', 1, %{member: false, pub: "XYZ_pkey"})
    Index.Augmentation.CIndex.toMember(0, local_cindex, 'global_iindex_test_brg41')
    [{0, entry}] = :ets.lookup(local_cindex, 0)
    toMemberFalse = entry.toMember

    DB.delete_db('global_iindex_test_brg41')

    {:ok, toMemberTrue: toMemberTrue, toMemberFalse: toMemberFalse}
  end

  test "check toMember", state do
    assert state.toMemberTrue == true
    assert state.toMemberFalse == false
  end
end
