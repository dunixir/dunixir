defmodule Membership.Augmentation.RevocationSignatureTest do
  @moduledoc """
  Test of validation rule BR_G84
  """
  use ExUnit.Case
  doctest Index.Augmentation.MIndex

  setup_all do
    local_mindex = :ets.new(:"test/local_mindex", [:set, :protected])
    rsTestEmpty = Index.Augmentation.MIndex.revocationSignature(local_mindex)
    :ets.insert(local_mindex, {0, %{revocationSigOK: true}})
    :ets.insert(local_mindex, {1, %{revocationSigOK: true}})
    rsTest0 = Index.Augmentation.MIndex.revocationSignature(local_mindex)

    :ets.insert(local_mindex, {2, %{revocationSigOK: false}})
    rsTest1 = Index.Augmentation.MIndex.revocationSignature(local_mindex)

    {:ok, rsTestEmpty: rsTestEmpty, rsTest0: rsTest0, rsTest1: rsTest1}
  end

  test "BR_G84 - Revocation Signature", state do
    assert state.rsTestEmpty
    assert state.rsTest0
    assert !state.rsTest1
  end
end
