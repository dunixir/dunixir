defmodule Membership.Augmentation.MembershipEnoughCertificationsTest do
  @moduledoc """
  Test of validation rule BR_G79
  """
  use ExUnit.Case
  doctest Index.Augmentation.MIndex

  setup_all do
    local_mindex = :ets.new(:"test/local_mindex", [:set, :protected])
    ecTestEmpty = Index.Augmentation.MIndex.membershipEnoughCertifications(local_mindex)
    :ets.insert(local_mindex, {0, %{enoughCerts: true}})
    :ets.insert(local_mindex, {1, %{enoughCerts: true}})
    ecTest0 = Index.Augmentation.MIndex.membershipEnoughCertifications(local_mindex)

    :ets.insert(local_mindex, {2, %{enoughCerts: false}})
    ecTest1 = Index.Augmentation.MIndex.membershipEnoughCertifications(local_mindex)

    {:ok, ecTestEmpty: ecTestEmpty, ecTest0: ecTest0, ecTest1: ecTest1}
  end

  test "BR_G79 - Membership enough certifications", state do
    assert state.ecTestEmpty
    assert state.ecTest0
    assert !state.ecTest1
  end
end
