defmodule Membership.Augmentation.MembershipActiveTest do
  @moduledoc """
  Test of validation rule BR_G81
  """
  use ExUnit.Case
  doctest Index.Augmentation.MIndex

  setup_all do
    local_mindex = :ets.new(:"test/local_mindex", [:set, :protected])
    maTestEmpty = Index.Augmentation.MIndex.membershipActive(local_mindex)
    :ets.insert(local_mindex, {0, %{activeIsMember: true}})
    :ets.insert(local_mindex, {1, %{activeIsMember: true}})
    maTest0 = Index.Augmentation.MIndex.membershipActive(local_mindex)

    :ets.insert(local_mindex, {2, %{activeIsMember: false}})
    maTest1 = Index.Augmentation.MIndex.membershipActive(local_mindex)

    {:ok, maTestEmpty: maTestEmpty, maTest0: maTest0, maTest1: maTest1}
  end

  test "BR_G81 - Membership active", state do
    assert state.maTestEmpty
    assert state.maTest0
    assert !state.maTest1
  end
end
