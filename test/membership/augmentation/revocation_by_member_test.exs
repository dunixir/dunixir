defmodule Membership.Augmentation.RevocationByMemberTest do
  @moduledoc """
  Test of validation rule BR_G82
  """
  use ExUnit.Case
  doctest Index.Augmentation.MIndex

  setup_all do
    local_mindex = :ets.new(:"test/local_mindex", [:set, :protected])
    rbmTestEmpty = Index.Augmentation.MIndex.revocationByMember(local_mindex)
    :ets.insert(local_mindex, {0, %{revokedIsMember: true}})
    :ets.insert(local_mindex, {1, %{revokedIsMember: true}})
    rbmTest0 = Index.Augmentation.MIndex.revocationByMember(local_mindex)

    :ets.insert(local_mindex, {2, %{revokedIsMember: false}})
    rbmTest1 = Index.Augmentation.MIndex.revocationByMember(local_mindex)

    {:ok, rbmTestEmpty: rbmTestEmpty, rbmTest0: rbmTest0, rbmTest1: rbmTest1}
  end

  test "BR_G82 - Revocation by a member", state do
    assert state.rbmTestEmpty
    assert state.rbmTest0
    assert !state.rbmTest1
  end
end
