defmodule Membership.Augmentation.MembershipWritabilityTest do
  @moduledoc """
  Test of validation rule BR_G64
  """
  use ExUnit.Case
  doctest Index.Augmentation.MIndex

  setup_all do
    local_mindex = :ets.new(:"test/local_mindex", [:set, :protected])
    mwTestEmpty = Index.Augmentation.MIndex.membershipWritability(local_mindex)
    :ets.insert(local_mindex, {0, %{age: Constants.Contract.msWindow() - 10}})
    :ets.insert(local_mindex, {1, %{age: Constants.Contract.msWindow() - 20}})
    mwTest0 = Index.Augmentation.MIndex.membershipWritability(local_mindex)

    :ets.insert(local_mindex, {2, %{age: Constants.Contract.msWindow() + 10}})
    mwTest1 = Index.Augmentation.MIndex.membershipWritability(local_mindex)

    {:ok, mwTestEmpty: mwTestEmpty, mwTest0: mwTest0, mwTest1: mwTest1}
  end

  test "BR_G64 - Membership Writability", state do
    assert state.mwTestEmpty
    assert state.mwTest0
    assert !state.mwTest1
  end
end
