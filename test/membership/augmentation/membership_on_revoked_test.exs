defmodule Membership.Augmentation.MembershipOnRevokedTest do
  @moduledoc """
  Test of validation rule BR_G77
  """
  use ExUnit.Case
  doctest Index.Augmentation.MIndex

  setup_all do
    local_mindex = :ets.new(:"test/local_mindex", [:set, :protected])
    morTestEmpty = Index.Augmentation.MIndex.membershipOnRevoked(local_mindex)
    :ets.insert(local_mindex, {0, %{onRevoked: false}})
    :ets.insert(local_mindex, {1, %{onRevoked: false}})
    morTest0 = Index.Augmentation.MIndex.membershipOnRevoked(local_mindex)

    :ets.insert(local_mindex, {2, %{onRevoked: true}})
    morTest1 = Index.Augmentation.MIndex.membershipOnRevoked(local_mindex)

    {:ok, morTestEmpty: morTestEmpty, morTest0: morTest0, morTest1: morTest1}
  end

  test "BR_G77 - Membership on revoked", state do
    assert state.morTestEmpty
    assert state.morTest0
    assert !state.morTest1
  end
end
