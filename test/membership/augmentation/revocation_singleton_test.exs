defmodule Membership.Augmentation.RevocationSingletonTest do
  @moduledoc """
  Test of validation rule BR_G83
  """
  use ExUnit.Case
  doctest Index.Augmentation.MIndex

  setup_all do
    local_mindex = :ets.new(:"test/local_mindex", [:set, :protected])
    rsTestEmpty = Index.Augmentation.MIndex.revocationSingleton(local_mindex)
    :ets.insert(local_mindex, {0, %{alreadyRevoked: false}})
    :ets.insert(local_mindex, {1, %{alreadyRevoked: false}})
    rsTest0 = Index.Augmentation.MIndex.revocationSingleton(local_mindex)

    :ets.insert(local_mindex, {2, %{alreadyRevoked: true}})
    rsTest1 = Index.Augmentation.MIndex.revocationSingleton(local_mindex)

    {:ok, rsTestEmpty: rsTestEmpty, rsTest0: rsTest0, rsTest1: rsTest1}
  end

  test "BR_G83 - Revocation singleton", state do
    assert state.rsTestEmpty
    assert state.rsTest0
    assert !state.rsTest1
  end
end
