defmodule Membership.Augmentation.MembershipSuccessionTest do
  @moduledoc """
  Test of validation rule BR_G75
  """
  use ExUnit.Case
  doctest Index.Augmentation.MIndex

  setup_all do
    local_mindex = :ets.new(:"test/local_mindex", [:set, :protected])
    msTestEmpty = Index.Augmentation.MIndex.membershipSuccession(local_mindex)
    :ets.insert(local_mindex, {0, %{numberFollowing: true}})
    :ets.insert(local_mindex, {1, %{numberFollowing: true}})
    msTest0 = Index.Augmentation.MIndex.membershipSuccession(local_mindex)

    :ets.insert(local_mindex, {2, %{numberFollowing: false}})
    msTest1 = Index.Augmentation.MIndex.membershipSuccession(local_mindex)

    {:ok, msTestEmpty: msTestEmpty, msTest0: msTest0, msTest1: msTest1}
  end

  test "BR_G75 - Membership Succession", state do
    assert state.msTestEmpty
    assert state.msTest0
    assert !state.msTest1
  end
end
