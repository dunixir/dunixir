defmodule Membership.Augmentation.ExcludedIsMemberTest do
  @moduledoc """
  Test of validation rule BR_G85
  """
  use ExUnit.Case
  doctest Index.Augmentation.MIndex

  setup_all do
    local_mindex = :ets.new(:"test/local_mindex", [:set, :protected])
    eimTestEmpty = Index.Augmentation.MIndex.excludedIsMember(local_mindex)
    :ets.insert(local_mindex, {0, %{excludedIsMember: true}})
    :ets.insert(local_mindex, {1, %{excludedIsMember: true}})
    eimTest0 = Index.Augmentation.MIndex.excludedIsMember(local_mindex)

    :ets.insert(local_mindex, {2, %{excludedIsMember: false}})
    eimTest1 = Index.Augmentation.MIndex.excludedIsMember(local_mindex)

    {:ok, eimTestEmpty: eimTestEmpty, eimTest0: eimTest0, eimTest1: eimTest1}
  end

  test "BR_G85 - Excluded is a member", state do
    assert state.eimTestEmpty
    assert state.eimTest0
    assert !state.eimTest1
  end
end
