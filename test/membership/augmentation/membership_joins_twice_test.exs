defmodule Membership.Augmentation.MembershipJoinsTwiceTest do
  @moduledoc """
  Test of validation rule BR_G78
  """
  use ExUnit.Case
  doctest Index.Augmentation.MIndex

  setup_all do
    local_mindex = :ets.new(:"test/local_mindex", [:set, :protected])
    mjtTestEmpty = Index.Augmentation.MIndex.membershipJoinsTwice(local_mindex)
    :ets.insert(local_mindex, {0, %{joinsTwice: false}})
    :ets.insert(local_mindex, {1, %{joinsTwice: false}})
    mjtTest0 = Index.Augmentation.MIndex.membershipJoinsTwice(local_mindex)

    :ets.insert(local_mindex, {2, %{joinsTwice: true}})
    mjtTest1 = Index.Augmentation.MIndex.membershipJoinsTwice(local_mindex)

    {:ok, mjtTestEmpty: mjtTestEmpty, mjtTest0: mjtTest0, mjtTest1: mjtTest1}
  end

  test "BR_G78 - Membership Joins Twice", state do
    assert state.mjtTestEmpty
    assert state.mjtTest0
    assert !state.mjtTest1
  end
end
