defmodule Membership.Augmentation.MembershipLeaverTest do
  @moduledoc """
  Test of validation rule BR_G80
  """
  use ExUnit.Case
  doctest Index.Augmentation.MIndex

  setup_all do
    local_mindex = :ets.new(:"test/local_mindex", [:set, :protected])
    mlTestEmpty = Index.Augmentation.MIndex.membershipLeaver(local_mindex)
    :ets.insert(local_mindex, {0, %{leaverIsMember: true}})
    :ets.insert(local_mindex, {1, %{leaverIsMember: true}})
    mlTest0 = Index.Augmentation.MIndex.membershipLeaver(local_mindex)

    :ets.insert(local_mindex, {2, %{leaverIsMember: false}})
    mlTest1 = Index.Augmentation.MIndex.membershipLeaver(local_mindex)

    {:ok, mlTestEmpty: mlTestEmpty, mlTest0: mlTest0, mlTest1: mlTest1}
  end

  test "BR_G80 - Membership Leaver", state do
    assert state.mlTestEmpty
    assert state.mlTest0
    assert !state.mlTest1
  end
end
