defmodule Identity.Augmentation.IsBeingKickedTest do
  @moduledoc """
  Test of validation rule BR_G35
  """
  use ExUnit.Case

  doctest Index.Augmentation.IIndex

  setup_all do
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])

    :ets.insert(local_iindex, {0, %{member: false}})
    :ets.insert(local_iindex, {1, %{member: true}})
    Index.Augmentation.IIndex.isBeingKicked(local_iindex)

    [{0, entry}] = :ets.lookup(local_iindex, 0)
    isKicked = entry.isBeingKicked
    [{1, entry}] = :ets.lookup(local_iindex, 1)
    isNotKicked = entry.isBeingKicked


    {:ok, isKicked: isKicked, isNotKicked: isNotKicked}
  end

  test "check isBeingKicked", state do
    assert state.isKicked
    assert !state.isNotKicked
  end
end
