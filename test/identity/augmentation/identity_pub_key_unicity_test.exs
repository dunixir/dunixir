defmodule Identity.Augmentation.IdentityPubKeyUnicityTest do
  @moduledoc """
    Test of validation rule BR_G74
  """
  use ExUnit.Case

  @identPubKeyUnique %{
    op: 'CREATE',
    uid: "tic",
    pub: "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    written_on: "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
    member: true,
    wasMember: true,
    kick: false,
    pubUnique: true
  }

  @identPubKeyNonUnique %{
    op: 'CREATE',
    uid: "tac",
    pub: "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    written_on: "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
    member: true,
    wasMember: true,
    kick: false,
    pubUnique: false
  }

  @identPubKeyWithoutField %{
    op: 'CREATE',
    uid: "tac",
    pub: "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    written_on: "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
    member: true,
    wasMember: true,
    kick: false
  }

  doctest Index.Augmentation.IIndex

  setup_all do
    # test with a uid unique
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])
    :ets.insert(local_iindex, {0, @identPubKeyUnique})
    pubKeyUniqueOneElt = Index.Augmentation.IIndex.identityPubkeyUnicity(local_iindex)
    :file.delete("test/local_iindex")

    # test with a uid unique
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])
    :ets.insert(local_iindex, {0, @identPubKeyWithoutField})
    pubKeyUniqueOneEltWithoutField = Index.Augmentation.IIndex.identityPubkeyUnicity(local_iindex)
    :file.delete("test/local_iindex")

    # test with a uid unique
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])
    :ets.insert(local_iindex, {0, @identPubKeyWithoutField})
    :ets.insert(local_iindex, {1, @identPubKeyUnique})
    pubKeyUniqueManyElt = Index.Augmentation.IIndex.identityPubkeyUnicity(local_iindex)
    :file.delete("test/local_iindex")

    # test with a uid non unique
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])
    :ets.insert(local_iindex, {0, @identPubKeyNonUnique})
    pubKeyNonUniqueOneElt = Index.Augmentation.IIndex.identityPubkeyUnicity(local_iindex)
    :file.delete("test/local_iindex")

    # test with a uid non unique
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])
    :ets.insert(local_iindex, {0, @identPubKeyWithoutField})
    :ets.insert(local_iindex, {1, @identPubKeyNonUnique})
    :ets.insert(local_iindex, {2, @identPubKeyUnique})
    pubKeyNonUniqueManyElt = Index.Augmentation.IIndex.identityPubkeyUnicity(local_iindex)
    :file.delete("test/local_iindex")

    {:ok,
     pubKeyUniqueOneElt: pubKeyUniqueOneElt,
     pubKeyUniqueOneEltWithoutField: pubKeyUniqueOneEltWithoutField,
     pubKeyUniqueManyElt: pubKeyUniqueManyElt,
     pubKeyNonUniqueOneElt: pubKeyNonUniqueOneElt,
     pubKeyNonUniqueManyElt: pubKeyNonUniqueManyElt}
  end

  test "check pubUnique", state do
    assert state.pubKeyUniqueOneElt == true
    assert state.pubKeyUniqueOneEltWithoutField == true
    assert state.pubKeyUniqueManyElt == true
    assert state.pubKeyNonUniqueOneElt == false
    assert state.pubKeyNonUniqueManyElt == false
  end
end
