defmodule Identity.Augmentation.IdentityUserIdUnicityTest do
  @moduledoc """
    Test of validation rule BR_G73
  """

  use ExUnit.Case

  @identUserIdUnique %{
    op: 'CREATE',
    uid: "tic",
    pub: "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    written_on: "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
    member: true,
    wasMember: true,
    kick: false,
    uidUnique: true
  }

  @identUserIdNonUnique %{
    op: 'CREATE',
    uid: "tac",
    pub: "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    written_on: "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
    member: true,
    wasMember: true,
    kick: false,
    uidUnique: false
  }

  @identUserIdWithoutField %{
    op: 'CREATE',
    uid: "tac",
    pub: "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    written_on: "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
    member: true,
    wasMember: true,
    kick: false
  }

  doctest Index.Augmentation.IIndex

  setup_all do
    # test with a uid unique
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])
    :ets.insert(local_iindex, {0, @identUserIdUnique})
    userIdUniqueOneElt = Index.Augmentation.IIndex.identityUserIdUnicity(local_iindex)
    :file.delete("test/local_iindex")

    # test with a uid unique
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])
    :ets.insert(local_iindex, {0, @identUserIdWithoutField})
    userIdUniqueOneEltWithoutField = Index.Augmentation.IIndex.identityUserIdUnicity(local_iindex)
    :file.delete("test/local_iindex")

    # test with a uid unique
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])
    :ets.insert(local_iindex, {0, @identUserIdWithoutField})
    :ets.insert(local_iindex, {1, @identUserIdUnique})
    userIdUniqueManyElt = Index.Augmentation.IIndex.identityUserIdUnicity(local_iindex)
    :file.delete("test/local_iindex")

    # test with a uid non unique
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])
    :ets.insert(local_iindex, {0, @identUserIdNonUnique})
    userIdNonUniqueOneElt = Index.Augmentation.IIndex.identityUserIdUnicity(local_iindex)
    :file.delete("test/local_iindex")

    # test with a uid non unique
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])
    :ets.insert(local_iindex, {0, @identUserIdWithoutField})
    :ets.insert(local_iindex, {1, @identUserIdNonUnique})
    :ets.insert(local_iindex, {2, @identUserIdUnique})
    userIdNonUniqueManyElt = Index.Augmentation.IIndex.identityUserIdUnicity(local_iindex)
    :file.delete("test/local_iindex")

    {:ok,
     userIdUniqueOneElt: userIdUniqueOneElt,
     userIdUniqueOneEltWithoutField: userIdUniqueOneEltWithoutField,
     userIdUniqueManyElt: userIdUniqueManyElt,
     userIdNonUniqueOneElt: userIdNonUniqueOneElt,
     userIdNonUniqueManyElt: userIdNonUniqueManyElt}
  end

  test "check IdentityUserIdUnicity", state do
    assert state.userIdUniqueOneElt == true
    assert state.userIdUniqueOneEltWithoutField == true
    assert state.userIdUniqueManyElt == true
    assert state.userIdNonUniqueOneElt == false
    assert state.userIdNonUniqueManyElt == false
  end
end
