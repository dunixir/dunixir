defmodule Identity.Augmentation.IdentityWritabilityTest do
  @moduledoc """
    Test of validation rule BR_G63
  """
  use ExUnit.Case

  @identWritabilityOk %{
    op: 'CREATE',
    uid: "tic",
    pub: "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    written_on: "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
    member: true,
    wasMember: true,
    kick: false,
    age: Constants.Contract.idtyWindow() - 10
  }

  @identWritabilityNok %{
    op: 'CREATE',
    uid: "tic",
    pub: "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    written_on: "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
    member: true,
    wasMember: true,
    kick: false,
    age: Constants.Contract.idtyWindow() + 10
  }

  @identWritabilityWithoutField %{
    op: 'CREATE',
    uid: "tic",
    pub: "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    written_on: "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
    member: true,
    wasMember: true,
    kick: false
  }

  doctest Index.Augmentation.IIndex

  setup_all do
    # test with a valid age
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])
    :ets.insert(local_iindex, {0, @identWritabilityOk})
    writabilityOneEltOk = Index.Augmentation.IIndex.identityWritability(local_iindex)
    :file.delete("test/local_iindex")

    # test with a valid age
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])
    :ets.insert(local_iindex, {0, @identWritabilityWithoutField})
    writabilityOneEltWithoutFieldOK = Index.Augmentation.IIndex.identityWritability(local_iindex)
    :file.delete("test/local_iindex")

    # test with a valid age
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])
    :ets.insert(local_iindex, {0, @identWritabilityWithoutField})
    :ets.insert(local_iindex, {1, @identWritabilityOk})
    writabilityManyEltOk = Index.Augmentation.IIndex.identityWritability(local_iindex)
    :file.delete("test/local_iindex")

    # test with an invalid age
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])
    :ets.insert(local_iindex, {0, @identWritabilityNok})
    writabilityOneEltNok = Index.Augmentation.IIndex.identityWritability(local_iindex)
    :file.delete("test/local_iindex")

    # test with an invalid age
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])
    :ets.insert(local_iindex, {0, @identWritabilityWithoutField})
    :ets.insert(local_iindex, {1, @identWritabilityNok})
    :ets.insert(local_iindex, {2, @identWritabilityOk})
    writabilityManyEltNok = Index.Augmentation.IIndex.identityWritability(local_iindex)
    :file.delete("test/local_iindex")

    {:ok,
     writabilityOneEltOk: writabilityOneEltOk,
     writabilityOneEltWithoutFieldOK: writabilityOneEltWithoutFieldOK,
     writabilityManyEltOk: writabilityManyEltOk,
     writabilityOneEltNok: writabilityOneEltNok,
     writabilityManyEltNok: writabilityManyEltNok}
  end

  test "check identity writability", state do
    assert state.writabilityOneEltOk == true
    assert state.writabilityOneEltWithoutFieldOK == true
    assert state.writabilityManyEltOk == true
    assert state.writabilityOneEltNok == false
    assert state.writabilityManyEltNok == false
  end
end
