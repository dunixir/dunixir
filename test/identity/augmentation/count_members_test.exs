defmodule Identity.Augmentation.CountMembersTest do
  @moduledoc """
  Test of validation rule BR_G10
  """
  use ExUnit.Case

  @iindex_member_true %{
    op: 'CREATE',
    uid: "totoUser",
    pub: "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    written_on: "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
    member: true,
    wasMember: true,
    kick: false,
    age: 7,
    uidUnique: false,
    pubUnique: false,
    excludedIsMember: false,
    idBeingKicked: false,
    hasToBeExcluded: false,
  }

  @iindex_member_false %{
    op: 'CREATE',
    uid: "totoUser",
    pub: "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    written_on: "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
    member: false,
    wasMember: true,
    kick: false,
    age: 7,
    uidUnique: false,
    pubUnique: false,
    excludedIsMember: false,
    idBeingKicked: false,
    hasToBeExcluded: false,
  }

  doctest Index.Augmentation.IIndex

  setup_all do
    # Test with HEAD.number == 0
    local_iindex = :ets.new(:local_iindex, [:set, :protected])
    local_bindex = :ets.new(:local_bindex, [:set, :protected])

    :ets.insert(local_iindex, {0, @iindex_member_true})
    :ets.insert(local_iindex, {1, @iindex_member_true})
    :ets.insert(local_iindex, {2, @iindex_member_false})

    :ets.insert(local_bindex, {1, %{number: 0}})

    DB.start_link(['global_bindex_test_brg10'])
    DB.insert('global_bindex_test_brg10', 0, %{number: 0, membersCount: 3})

    Index.Augmentation.BIndex.membersCount(local_iindex, local_bindex, 'global_bindex_test_brg10')

    key = :ets.first(local_bindex)
    [{_key, head0}] = :ets.lookup(local_bindex, key)


    # Test with HEAD.number == 1
    local_iindex = :ets.new(:local_iindex, [:set, :protected])
    local_bindex = :ets.new(:local_bindex, [:set, :protected])

    :ets.insert(local_iindex, {0, @iindex_member_true})
    :ets.insert(local_iindex, {1, @iindex_member_true})
    :ets.insert(local_iindex, {2, @iindex_member_true})
    :ets.insert(local_iindex, {3, @iindex_member_false})
    :ets.insert(local_iindex, {4, @iindex_member_false})

    :ets.insert(local_bindex, {1, %{number: 1}})

    Index.Augmentation.BIndex.membersCount(local_iindex, local_bindex, 'global_bindex_test_brg10')

    key = :ets.first(local_bindex)
    [{_key, head1}] = :ets.lookup(local_bindex, key)


    # Test with HEAD.number == 15
    local_iindex = :ets.new(:local_iindex, [:set, :protected])
    local_bindex = :ets.new(:local_bindex, [:set, :protected])

    :ets.insert(local_iindex, {0, @iindex_member_true})
    :ets.insert(local_iindex, {1, @iindex_member_true})
    :ets.insert(local_iindex, {2, @iindex_member_true})
    :ets.insert(local_iindex, {3, @iindex_member_false})
    :ets.insert(local_iindex, {4, @iindex_member_false})
    :ets.insert(local_iindex, {5, @iindex_member_false})
    :ets.insert(local_iindex, {6, @iindex_member_false})
    :ets.insert(local_iindex, {7, @iindex_member_false})

    :ets.insert(local_bindex, {1, %{number: 1}})

    Index.Augmentation.BIndex.membersCount(local_iindex, local_bindex, 'global_bindex_test_brg10')

    key = :ets.first(local_bindex)
    [{_key, head2}] = :ets.lookup(local_bindex, key)

    DB.delete_db('global_bindex_test_brg10')
    {:ok, membersCount0: head0.membersCount, membersCount1: head1.membersCount, membersCount2: head2.membersCount}
  end

  test "check membersCount, HEAD.number == 0", state do
    assert state.membersCount0 == 2
  end

  test "check membersCount, HEAD.number == 1", state do
    assert state.membersCount1 == 4
  end

  test "check membersCount, HEAD.number == 15", state do
    assert state.membersCount2 == 1
  end
end
