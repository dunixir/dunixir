defmodule Identity.Augmentation.PubUniqueTest do
  @moduledoc """
  Test of validation rule BR_G21
  """
  use ExUnit.Case

  doctest Index.Augmentation.IIndex

  setup_all do
    local_iindex = :ets.new(:"test/local_iindex", [:set, :protected])
    local_mindex = :ets.new(:"test/local_mindex", [:set, :protected])

    :ets.insert(
      local_iindex,
      {"pub_toto",
       %{
         created_on: "block_uid_test",
         kick: false,
         member: true,
         op: "CREATE",
         pub: "pub_toto",
         uid: "user_id_toto",
         wasMember: true,
         written_on: "blockstamp_test"
       }}
    )

    :ets.insert(
      local_mindex,
      {"pub_toto",
       %{
         chainable_on: 20,
         created_on: "block_uid_test",
         expired_on: 0,
         expires_on: 20,
         leaving: false,
         op: "CREATE",
         pub: "pub_toto",
         revocation: nil,
         revoked_on: nil,
         revokes_on: 30,
         type: "JOIN",
         written_on: "blockstamp_test"
       }}
    )

    :ets.insert(
      local_iindex,
      {"pub_tata",
       %{
         created_on: "block_uid_test",
         kick: false,
         member: true,
         op: "CREATE",
         pub: "pub_tata",
         uid: "user_id_tata",
         wasMember: true,
         written_on: "blockstamp_test"
       }}
    )

    :ets.insert(
      local_mindex,
      {"pub_tata",
       %{
         chainable_on: 20,
         created_on: "block_uid_test",
         expired_on: 0,
         expires_on: 20,
         leaving: false,
         op: "CREATE",
         pub: "pub_tata",
         revocation: nil,
         revoked_on: nil,
         revokes_on: 30,
         type: "JOIN",
         written_on: "blockstamp_test"
       }}
    )

    DB.start_link(['global_iindex_test_brg21'])
    :ets.match(local_iindex, :"$1")
    |> Enum.map(fn [{key, value}] ->
      DB.insert('global_iindex_test_brg21', key, value)
    end)

    [{"pub_toto", elt}] = DB.lookup('global_iindex_test_brg21', "pub_toto")
    DB.delete('global_iindex_test_brg21', "pub_toto")
    new_elt = Map.replace!(elt, :pub, "pub_tutu")
    DB.insert('global_iindex_test_brg21', "pub_tutu", new_elt)

    Index.Augmentation.IIndex.pubUnique(local_iindex, 'global_iindex_test_brg21')
    Index.Augmentation.IIndex.pubUnique(local_iindex, 'global_iindex_test_brg21')
    [{"pub_toto", toto}] = :ets.lookup(local_iindex, "pub_toto")
    [{"pub_tata", tata}] = :ets.lookup(local_iindex, "pub_tata")

    DB.delete_db('global_iindex_test_brg21')

    {:ok, pub_unique_toto: toto.pubUnique, pub_unique_tata: tata.pubUnique}
  end

  test "check pubUnique is the right value", state do
    assert state.pub_unique_toto
    refute state.pub_unique_tata
  end
end
