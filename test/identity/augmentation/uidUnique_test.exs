defmodule Identity.Augmentation.UidUniqueTest do
  @moduledoc """
  Test of validation rule BR_G20
  """
  use ExUnit.Case

  doctest Index.Augmentation.IIndex

  setup_all do
    local_iindex_a = :ets.new(:"test/local_iindex", [:set, :protected])
    local_iindex_b = :ets.new(:"test/local_iindex", [:set, :protected])
    local_mindex_a = :ets.new(:"test/local_mindex", [:set, :protected])
    local_mindex_b = :ets.new(:"test/local_mindex", [:set, :protected])

    new_entry = %{
      op: "CREATE",
      uid: "user_id_test",
      pub: "pub_toto",
      created_on: "block_uid_test",
      written_on: "blockstamp_test",
      member: true,
      wasMember: true,
      kick: false
    }

    :ets.insert(local_iindex_a, {"pub_toto", new_entry})
    :ets.insert(local_iindex_b, {"pub_toto", new_entry})

    new_entry_1 = %{
      op: "CREATE",
      pub: "pub_toto",
      created_on: "block_uid_test",
      written_on: "blockstamp_test",
      expired_on: 0,
      expires_on: Constants.medianTime() + Constants.msValidity(),
      revokes_on: Constants.medianTime() + Constants.msValidity() * 2,
      chainable_on: Constants.medianTime() + Constants.msPeriod(),
      type: "JOIN",
      revocation: nil,
      revoked_on: nil,
      leaving: false
    }

    :ets.insert(local_mindex_a, {"pub_toto", new_entry_1})
    :ets.insert(local_mindex_b, {"pub_toto", new_entry_1})

    DB.start_link(['global_iindex_test_brg20_a'])
    DB.start_link(['global_iindex_test_brg20_b'])

    :ets.match(local_iindex_a, :"$1")
    |> Enum.map(fn [{key, value}] ->
      DB.insert('global_iindex_test_brg20_a', key, value)
    end)

    :ets.match(local_iindex_b, :"$1")
    |> Enum.map(fn [{key, value}] ->
      DB.insert('global_iindex_test_brg20_b', key, value)
    end)

    [{"pub_toto", elt}] = DB.lookup('global_iindex_test_brg20_b', "pub_toto")
    new_elt = Map.replace!(elt, :uid, "user_id_test_2")
    DB.insert('global_iindex_test_brg20_b', "pub_toto", new_elt)

    Index.Augmentation.IIndex.uidUnique(local_iindex_a, 'global_iindex_test_brg20_a')
    Index.Augmentation.IIndex.uidUnique(local_iindex_b, 'global_iindex_test_brg20_b')
    [{"pub_toto", a}] = :ets.lookup(local_iindex_a, "pub_toto")
    [{"pub_toto", b}] = :ets.lookup(local_iindex_b, "pub_toto")

    DB.delete_db('global_iindex_test_brg20_a')
    DB.delete_db('global_iindex_test_brg20_b')

    {:ok, uid_unique_a: a.uidUnique, uid_unique_b: b.uidUnique}
  end

  test "check uidUnique is the right value", state do
    assert state.uid_unique_b
    refute state.uid_unique_a
  end
end
