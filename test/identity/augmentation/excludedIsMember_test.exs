defmodule Identity.Augmentation.ExcludedIsMemberTest do
  @moduledoc """
  Test of validation rule BR_G33
  """
  use ExUnit.Case

  @iindex_member_true %{
    op: 'CREATE',
    uid: "totoUser",
    pub: "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    written_on: "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
    member: true,
    wasMember: true,
    kick: false,
    age: 7,
    uidUnique: false,
    pubUnique: false,
    excludedIsMember: false,
    idBeingKicked: false,
    hasToBeExcluded: false
  }

  @iindex_member_false %{
    op: 'CREATE',
    uid: "totoUser",
    pub: "Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    written_on: "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
    member: false,
    wasMember: true,
    kick: false,
    age: 7,
    uidUnique: false,
    pubUnique: false,
    excludedIsMember: false,
    idBeingKicked: false,
    hasToBeExcluded: false
  }

  @iindex_0 %{
    op: 'CREATE',
    uid: "totoUser",
    pub: "Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    written_on: "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
    member: false,
    wasMember: true,
    kick: false,
    age: 7,
    uidUnique: false,
    pubUnique: false,
    excludedIsMember: false,
    idBeingKicked: false,
    hasToBeExcluded: false
  }

  @iindex_1 %{
    op: 'CREATE',
    uid: "totoUser",
    pub: "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
    created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    written_on: "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
    member: false,
    wasMember: true,
    kick: false,
    age: 7,
    uidUnique: false,
    pubUnique: false,
    excludedIsMember: false,
    idBeingKicked: false,
    hasToBeExcluded: false
  }

  doctest Index.Augmentation.IIndex

  setup_all do
    # Test with 5 entries in ets, ENTRY.member = true
    local_iindex_test = :ets.new(:local_iindex, [:set, :protected])

    DB.start_link(['global_iindex_test_brg33'])

    :ets.insert(local_iindex_test, {0, @iindex_member_true})
    :ets.insert(local_iindex_test, {1, @iindex_member_true})
    :ets.insert(local_iindex_test, {2, @iindex_member_true})
    :ets.insert(local_iindex_test, {3, @iindex_member_true})
    :ets.insert(local_iindex_test, {4, @iindex_member_true})

    DB.insert('global_iindex_test_brg33', 0, %{pub: "zerjklzejkrjlekzjrlk"})
    DB.insert('global_iindex_test_brg33', 1, %{pub: "zerjklzejkrjlekzjrlk"})

    Index.Augmentation.IIndex.excludedIsMember(local_iindex_test, :global_iindex_test_brg33)

    allExcludedIsMember0 =
      :ets.match(local_iindex_test, {:_, :"$1"})
      |> Enum.reduce(true, fn [entry], acc ->
        entry.excludedIsMember && acc
      end)


    # Test with 1 entry in ets, 3 entries in dets, ENTRY.member = false
    local_iindex_test = :ets.new(:local_iindex, [:set, :protected])

    :ets.insert(local_iindex_test, {0, @iindex_member_false})

    DB.insert('global_iindex_test_brg33', 0, @iindex_member_true)
    DB.insert('global_iindex_test_brg33', 1, @iindex_0)
    DB.insert('global_iindex_test_brg33', 2, @iindex_1)

    Index.Augmentation.IIndex.excludedIsMember(local_iindex_test, 'global_iindex_test_brg33')

    allExcludedIsMember1 =
      :ets.match(local_iindex_test, {:_, :"$1"})
      |> Enum.reduce(true, fn [entry], acc ->
        if entry.member do
          entry.excludedIsMember && acc
        else
          [[first] | _] = DB.match('global_iindex_test_brg33', {:_, %{pub: entry.pub, member: :"$2"}})
          acc && first == entry.excludedIsMember
        end
      end)


    # Test with several entries in ets, several entries in dets, ENTRY.member = false
    local_iindex_test = :ets.new(:local_iindex, [:set, :protected])

    :ets.insert(local_iindex_test, {0, @iindex_member_false})
    :ets.insert(local_iindex_test, {1, @iindex_member_false})
    :ets.insert(local_iindex_test, {2, @iindex_member_true})
    :ets.insert(local_iindex_test, {3, @iindex_member_true})
    :ets.insert(local_iindex_test, {4, @iindex_member_false})

    DB.insert('global_iindex_test_brg33', 0, @iindex_member_true)
    DB.insert('global_iindex_test_brg33', 1, @iindex_0)
    DB.insert('global_iindex_test_brg33', 2, @iindex_1)

    Index.Augmentation.IIndex.excludedIsMember(local_iindex_test, 'global_iindex_test_brg33')

    allExcludedIsMember2 =
      :ets.match(local_iindex_test, {:_, :"$1"})
      |> Enum.reduce(true, fn [entry], acc ->
        if entry.member do
          entry.excludedIsMember && acc
        else
          [[first] | _] = DB.match('global_iindex_test_brg33', {:_, %{pub: entry.pub, member: :"$2"}})
          acc && first == entry.excludedIsMember
        end
      end)

    DB.delete_db('global_iindex_test_brg33')

    {:ok, test0: allExcludedIsMember0, test1: allExcludedIsMember1, test2: allExcludedIsMember2}
  end

  test "Test excludedIsMember : 5 entries in ets, ENTRY.member = true", state do
    assert state.test0
  end

  test "Test excludedIsMember : 1 entry in ets, 3 entries in dets, ENTRY.member = false", state do
    assert state.test1
  end

  test "Test excludedIsMember : several entries in ets, several entries in dets, ENTRY.member = false",
       state do
    assert state.test2
  end
end
