defmodule Transaction.Augmentation.CheckInputUnlockedTest do
  @moduledoc """
  Test of validation rule BR_G88
  """
  use ExUnit.Case
  doctest Index.Augmentation.SIndex

  alias Index.Augmentation.SIndex, as: SIndex

  setup_all do
    :file.delete("test/local_sindex")
    local_sindex = :ets.new(:"test/local_sindex", [:set, :protected])

    # Insert one block with valid unitBase of 0 into local sindex
    new_entry = %{
      amount: 90,
      base: 0,
      conditions: nil,
      consumed: true,
      created_on: "blockstamp",
      identifier: "input_identifier",
      locktime: nil,
      op: "UPDATE",
      pos: "input_index_test",
      tx: "tx_hash_test_a",
      written_on: "blockstamp_test",
      written_time: nil
    }

    key_a = {"UPDATE", "input_identifier", "input_index_test"}
    :ets.insert(local_sindex, {key_a, new_entry})
    [{key_a, entry}] = :ets.lookup(local_sindex, key_a)
    :ets.delete(local_sindex, key_a)
    :ets.insert(local_sindex, {key_a, Map.merge(entry, %{isLocked: false})})

    :file.delete("test/local_sindex_incorrect")

    local_sindex_incorrect = :ets.new(:"test/local_sindex_incorrect", [:set, :protected])
    # Insert one block with valid unitBase of 0 into local sindex
    new_entry_b = %{
      amount: 90,
      base: 0,
      conditions: nil,
      consumed: true,
      created_on: "blockstamp",
      identifier: "input_identifier_b",
      locktime: nil,
      op: "UPDATE",
      pos: "input_index_test_b",
      tx: "tx_hash_test_b",
      written_on: "blockstamp_test",
      written_time: nil
    }

    key_b = {"UPDATE", "input_identifier_b", "input_index_test_b"}
    :ets.insert(local_sindex_incorrect, {key_b, new_entry_b})
    [{key_b, entry}] = :ets.lookup(local_sindex_incorrect, key_b)
    :ets.delete(local_sindex_incorrect, key_b)
    :ets.insert(local_sindex_incorrect, {key_b, Map.merge(entry, %{isLocked: true})})

    {:ok, local_sindex: local_sindex, local_sindex_incorrect: local_sindex_incorrect}
  end

  test "check input is unlocked", state do
    assert SIndex.checkInputIsUnlocked(state[:local_sindex])
    refute SIndex.checkInputIsUnlocked(state[:local_sindex_incorrect])
  end
end
