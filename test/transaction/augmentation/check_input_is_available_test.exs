defmodule Transaction.Augmentation.CheckInputIsAvailableTest do
  @moduledoc """
  Test of validation rule BR_G87
  """
  use ExUnit.Case

  doctest Index.Augmentation.SIndex

  alias Index.Augmentation.SIndex, as: SIndex

  setup_all do
    :file.delete("test/local_sindex")
    local_sindex = :ets.new(:"test/local_sindex", [:set, :protected])

    # Insert one correct temp index with op as UPDATE
    new_entry = %{
      amount: 90,
      base: 0,
      conditions: nil,
      consumed: true,
      created_on: "blockstamp",
      identifier: "input_identifier_a_test",
      locktime: nil,
      op: "UPDATE",
      pos: "input_index_a_test",
      tx: "tx_hash_test_a",
      written_on: "blockstamp_test",
      written_time: nil
    }

    key_a = {"UPDATE", "input_identifier_a_test", "input_index_a_test"}
    :ets.insert(local_sindex, {key_a, new_entry})
    # Get and store the temp index
    [{key_a, entry}] = :ets.lookup(local_sindex, key_a)
    # Delete the temp_index
    :ets.delete(local_sindex, key_a)
    # Reinsert the index that has now the "avaiable" attribut
    :ets.insert(local_sindex, {key_a, Map.merge(entry, %{available: true})})

    local_sindex_incorrect = :ets.new(:"test/local_sindex", [:set, :protected])

    # Insert one correct temp index with op as UPDATE
    new_entry_b = %{
      amount: 90,
      base: 0,
      conditions: nil,
      consumed: true,
      created_on: "blockstamp",
      identifier: "input_identifier_b_test",
      locktime: nil,
      op: "UPDATE",
      pos: "input_index_b_test",
      tx: "tx_hash_test_b",
      written_on: "blockstamp_test",
      written_time: nil
    }

    # Get and store the temp index
    key_b = {"UPDATE", "input_identifier_b_test", "input_index_b_test"}
    :ets.insert(local_sindex_incorrect, {key_b, new_entry_b})
    [{key_b, entry}] = :ets.lookup(local_sindex_incorrect, key_b)

    # Delete the temp_index
    :ets.delete(local_sindex_incorrect, key_b)
    # Reinsert the index that has now the "avaiable" attribut
    :ets.insert(local_sindex_incorrect, {key_b, Map.merge(entry, %{availgitable: false})})

    {:ok, local_sindex: local_sindex, local_sindex_incorrect: local_sindex_incorrect}
  end

  test "check BR_G87 - Version", state do
    assert SIndex.checkInputIsAvailable(state.local_sindex)
    refute SIndex.checkInputIsAvailable(state.local_sindex_incorrect)
  end
end
