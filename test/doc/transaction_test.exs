defmodule Doc.TransactionTest do
  use ExUnit.Case
  doctest Doc.Transaction

  @transaction_valid %{
    "currency" => "g1",
    "blockstamp" => "107702-0000017CDBE974DC9A46B89EE7DC2BEE4017C43A005359E0853026C21FB6A084",
    "locktime" => 0,
    "issuers" => ["Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC"],
    "inputs" => [
      "1002:0:D:Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC:104937",
      "1002:0:D:Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC:105214"
    ],
    "unlocks" => ["0:SIG(0)", "1:SIG(0)"],
    "outputs" => ["2004:0:SIG(DTgQ97AuJ8UgVXcxmNtULAs8Fg1kKC1Wr9SAS96Br9NG)"],
    "comment" => "c est pour 2 mois d adhesion ressourcerie",
    "signatures" => [
      "lnpuFsIymgz7qhKF/GsZ3n3W8ZauAAfWmT4W0iJQBLKJK2GFkesLWeMj/+GBfjD6kdkjreg9M6VfkwIZH+hCCQ=="
    ]
  }

  @transaction_invalid %{
    "currency" => "g1sdqsds",
    "blockstamp" => "107702-0000017CDBE974DC9A46B89EE7DC2BEE4017C43A005359E0853026C21FB6A084",
    "locktime" => 0,
    "issuers" => ["Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC"],
    "inputs" => [
      "1002:0:D:Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC:104937",
      "1002:0:D:Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC:105214"
    ],
    "unlocks" => ["0:SIG(0)", "1:SIG(0)"],
    "outputs" => ["2004:0:SIG(DTgQ97AuJ8UgVXcxmNtULAs8Fg1kKC1Wr9SAS96Br9NG)"],
    "comment" => "c est pour 2 mois d adhesion ressourcerie",
    "signatures" => [
      "lnpuFsIymgz7qhKF/GsZ3n3W8ZauAAfWmT4W0iJQBLKJK2GFkesLWeMj/+GBfjD6kdkjreg9M6VfkwIZH+hCCQ=="
    ]
  }

  @transaction_hash %{
    "blockstamp" => "506140-00000006174C9FD4F4088D1CD830A1B372865977DBA7ED834AE94C6767EA42C6",
    "blockstampTime" => 1_646_604_540,
    "comment" => "",
    "currency" => "g1",
    "hash" => "BB62BC1515CC1BE35993CC05EA8702A358763C3315DCE321C4E9D5B60E2A1D6B",
    "inputs" => ["7500:0:T:05B64E84A7F6DECB91B8FAAE287676B121D9C3D3CCFD7EF57FD6F91B4B3E548C:1"],
    "issuers" => ["7HzhXCA9Cp3JQE2GpY6JJwv89fJi6FQSBtDsq657jpj3"],
    "locktime" => 0,
    "outputs" => [
      "2500:0:SIG(6SMqmUyJ7zCzht5twiQg6ePLiQ6HfLd3shhihMbAq5av)",
      "5000:0:SIG(7HzhXCA9Cp3JQE2GpY6JJwv89fJi6FQSBtDsq657jpj3)"
    ],
    "signatures" => [
      "/FC7D8TMkDuUn+0MNKSbFM/4x2dIWo/cQkGIj4KKTHjcm2F900P/nC9YyWvNUQLrhwD5lIZ6v8iSGyFAVFiVAw=="
    ],
    "unlocks" => ["0:SIG(0)"],
    "version" => 10
  }

  def replaceKey(map, old_name, new_name) do
    {value, updated_map} = Map.pop(map, old_name)
    Map.put(updated_map, new_name, value)
  end

  test "signature - basic case" do
    assert Doc.Transaction.verif(@transaction_valid)
    refute Doc.Transaction.verif(@transaction_invalid)
  end

  test "wrong fields" do
    refute Doc.Transaction.verif(@transaction_valid |> replaceKey("signatures", "signatu"))
    refute Doc.Transaction.verif(@transaction_valid |> replaceKey("currency", "curency"))
  end

  test "wrong values format" do
    refute Doc.Transaction.verif(@transaction_valid |> Map.put("currency", "$currency$"))
    refute Doc.Transaction.verif(@transaction_valid |> Map.put("issuers", ["$pubkey$"]))

    refute Doc.Transaction.verif(
             @transaction_valid
             |> Map.put("locktime", -3)
           )

    refute Doc.Transaction.verif(
             @transaction_valid
             |> Map.put("inputs", [
               "1002:0:DSDSD:Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC:104937",
               "1002:0:DT:Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC:105214"
             ])
           )

    refute Doc.Transaction.verif(
             @transaction_valid
             |> Map.put("unlocks", ["0:SIGSIG(0)", "1:SIG(RER)"])
           )

    refute Doc.Transaction.verif(
             @transaction_valid
             |> Map.put("outputs", ["2004:-10:SIG(DTgQ97AuJ8UgVXcxmNtULAs8Fg1kKC1Wr9SAS96Br9NG)"])
           )

    refute Doc.Transaction.verif(
             @transaction_valid
             |> Map.put(
               "blockstamp",
               "1678840001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038"
             )
           )

    refute Doc.Transaction.verif(
             @transaction_valid
             |> Map.put(
               "signatures",
               [
                 "$*%SmSweUD4lEMwiZfY8ux9maBjrQQDkC85oMNsin6oSQCPdXG8sFCZ4FisUaWqKsfOlZVb/HNa+TKzD2t0Yte+DA=="
               ]
             )
           )
  end

  test "transaction hash" do
    assert Doc.Transaction.hash(@transaction_hash) == @transaction_hash["hash"]
  end
end
