defmodule Doc.RevocationTest do
  use ExUnit.Case
  doctest Doc.Revocation

  @default_document_version 10

  @revocation_valid %{
    "currency" => "g1",
    "pubkey" => "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
    "idty_uid" => "tic",
    "idty_buid" => "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
    "idty_sig" =>
      "1eubHHbuNfilHMM0G2bI30iZzebQ2cQ1PC7uPAw08FGMMmQCRerlF/3pc4sAcsnexsxBseA/3lY03KlONqJBAg==",
    "revocation" =>
      "XXOgI++6qpY9O31ml/FcfbXCE6aixIrgkT5jL7kBle3YOMr+8wrp7Rt+z9hDVjrNfYX2gpeJsuMNfG4T/fzVDQ=="
  }

  @revocation_invalid %{
    "currency" => "g1",
    "pubkey" => "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
    "idty_uid" => "tictic",
    "idty_buid" => "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
    "idty_sig" =>
      "1eubHHbuNfilHMM0G2bI30iZzebQ2cQ1PC7uPAw08FGMMmQCRerlF/3pc4sAcsnexsxBseA/3lY03KlONqJBAg==",
    "revocation" =>
      "XXOgI++6qpY9O31ml/FcfbXCE6aixIrgkT5jL7kBle3YOMr+8wrp7Rt+z9hDVjrNfYX2gpeJsuMNfG4T/fzVDQ=="
  }

  def replaceKey(map, old_name, new_name) do
    {value, updated_map} = Map.pop(map, old_name)
    Map.put(updated_map, new_name, value)
  end

  test "signature - basic case" do
    assert Doc.Revocation.verif(@revocation_valid)
    refute Doc.Revocation.verif(@revocation_invalid)
  end

  test "different possible key names" do
    for pubkey_name <- ["pubkey", "issuer"],
        idty_uid_name <- ["idty_uid", "uid"],
        idty_buid_name <- ["idty_buid", "buid"],
        idty_sig_name <- ["idty_sig", "sig"],
        version_name <- [nil, "version"] do
      valid =
        if version_name,
          do: Map.put(@revocation_valid, version_name, @default_document_version),
          else:
            @revocation_valid
            |> replaceKey("pubkey", pubkey_name)
            |> replaceKey("idty_uid", idty_uid_name)
            |> replaceKey("idty_buid", idty_buid_name)
            |> replaceKey("idty_sig", idty_sig_name)

      invalid =
        if version_name,
          do: Map.put(@revocation_invalid, version_name, @default_document_version),
          else:
            @revocation_invalid
            |> replaceKey("pubkey", pubkey_name)
            |> replaceKey("idty_uid", idty_uid_name)
            |> replaceKey("idty_buid", idty_buid_name)
            |> replaceKey("idty_sig", idty_sig_name)

      assert Doc.Revocation.verif(valid)
      refute Doc.Revocation.verif(invalid)
    end
  end

  test "wrong fields" do
    refute Doc.Revocation.verif(@revocation_valid |> replaceKey("pubkey", "pukey"))
    refute Doc.Revocation.verif(@revocation_valid |> replaceKey("currency", "curency"))
  end

  test "wrong values format" do
    refute Doc.Revocation.verif(@revocation_valid |> Map.put("currency", "$currency$"))
    refute Doc.Revocation.verif(@revocation_valid |> Map.put("pubkey", "$pubkey$"))

    refute Doc.Revocation.verif(
             @revocation_valid
             |> Map.put(
               "idty_buid",
               "7543x000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A"
             )
           )

    refute Doc.Revocation.verif(
             @revocation_valid
             |> Map.put(
               "idty_sig",
               "$*%SmSweUD4lEMwiZfY8ux9maBjrQQDkC85oMNsin6oSQCPdXG8sFCZ4FisUaWqKsfOlZVb/HNa+TKzD2t0Yte+DA=="
             )
           )

    refute Doc.Revocation.verif(
             @revocation_valid
             |> Map.put(
               "revocation",
               "$*%SmSweUD4lEMwiZfY8ux9maBjrQQDkC85oMNsin6oSQCPdXG8sFCZ4FisUaWqKsfOlZVb/HNa+TKzD2t0Yte+DA=="
             )
           )
  end
end
