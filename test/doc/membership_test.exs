defmodule Doc.MembershipTest do
  use ExUnit.Case
  doctest Doc.Membership

  @default_document_version 10

  @membership_valid %{
    "currency" => "duniter_unit_test_currency",
    "issuer" => "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
    "type" => "IN",
    "blockstamp" => "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
    "userid" => "tic",
    "certts" => "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
    "signature" =>
      "s2hUbokkibTAWGEwErw6hyXSWlWFQ2UWs2PWx8d/kkElAyuuWaQq4Tsonuweh1xn4AC1TVWt4yMR3WrDdkhnAw=="
  }

  @membership_invalid %{
    "currency" => "duniter_unit_test_currency",
    "issuer" => "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
    "type" => "IN",
    "blockstamp" => "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
    "userid" => "tictic",
    "certts" => "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
    "signature" =>
      "s2hUbokkibTAWGEwErw6hyXSWlWFQ2UWs2PWx8d/kkElAyuuWaQq4Tsonuweh1xn4AC1TVWt4yMR3WrDdkhnAw=="
  }

  def replaceKey(map, old_name, new_name) do
    {value, updated_map} = Map.pop(map, old_name)
    Map.put(updated_map, new_name, value)
  end

  test "signature - basic case" do
    assert Doc.Membership.verif(@membership_valid)
    refute Doc.Membership.verif(@membership_invalid)
  end

  test "different possible key names" do
    for issuer_name <- ["issuer", "pubkey"],
        type_name <- ["type", "membership"],
        blockstamp_name <- ["blockstamp", "block"],
        version_name <- [nil, "version"] do
      valid =
        if version_name,
          do: Map.put(@membership_valid, version_name, @default_document_version),
          else:
            @membership_valid
            |> replaceKey("issuer", issuer_name)
            |> replaceKey("type", type_name)
            |> replaceKey("blockstamp", blockstamp_name)

      invalid =
        if version_name,
          do: Map.put(@membership_invalid, version_name, @default_document_version),
          else:
            @membership_invalid
            |> replaceKey("issuer", issuer_name)
            |> replaceKey("type", type_name)
            |> replaceKey("blockstamp", blockstamp_name)

      assert Doc.Membership.verif(valid)
      refute Doc.Membership.verif(invalid)
    end
  end

  test "wrong fields" do
    refute Doc.Membership.verif(@membership_valid |> replaceKey("issuer", "isuer"))
    refute Doc.Membership.verif(@membership_valid |> replaceKey("currency", "curency"))
  end

  test "wrong values format" do
    refute Doc.Membership.verif(@membership_valid |> Map.put("currency", "$currency$"))
    refute Doc.Membership.verif(@membership_valid |> Map.put("issuer", "$pubkey$"))
    refute Doc.Membership.verif(@membership_valid |> Map.put("type", "INOUT"))

    refute Doc.Membership.verif(
             @membership_valid
             |> Map.put(
               "blockstamp",
               "7543x000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A"
             )
           )

    refute Doc.Membership.verif(
             @membership_valid
             |> Map.put(
               "certts",
               "1678840001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038"
             )
           )

    refute Doc.Membership.verif(
             @membership_valid
             |> Map.put(
               "signature",
               "$*%SmSweUD4lEMwiZfY8ux9maBjrQQDkC85oMNsin6oSQCPdXG8sFCZ4FisUaWqKsfOlZVb/HNa+TKzD2t0Yte+DA=="
             )
           )
  end
end
