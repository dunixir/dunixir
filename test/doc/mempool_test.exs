defmodule Doc.MempoolTest do
  use ExUnit.Case
  doctest Doc.Mempool

  test "basic test" do
    doc1 = %{"a" => 1, "b" => 2}
    doc2 = %{"a" => 11, "b" => 2}
    assert Doc.Mempool.can_add?(doc1)
    assert Doc.Mempool.can_add?(doc2)
    Doc.Mempool.add(doc1)
    refute Doc.Mempool.can_add?(doc1)
    assert Doc.Mempool.can_add?(doc2)
    Doc.Mempool.add(doc2)
    refute Doc.Mempool.can_add?(doc1)
    refute Doc.Mempool.can_add?(doc2)
  end
end
