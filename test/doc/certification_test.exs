defmodule Doc.CertificationTest do
  use ExUnit.Case
  doctest Doc.Certification

  @default_document_version 10

  @certification_valid %{
    "currency" => "g1-test",
    "pubkey" => "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
    "buid" => "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
    "sig" =>
      "wqZxPEGxLrHGv8VdEIfUGvUcf+tDdNTMXjLzVRCQ4UhlhDRahOMjfcbP7byNYr5OfIl83S1MBxF7VJgu8YasCA==",
    "idty_issuer" => "mMPioknj2MQCX9KyKykdw8qMRxYR2w1u3UpdiEJHgXg",
    "idty_uid" => "mmpio",
    "idty_buid" => "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    "idty_sig" =>
      "SmSweUD4lEMwiZfY8ux9maBjrQQDkC85oMNsin6oSQCPdXG8sFCZ4FisUaWqKsfOlZVb/HNa+TKzD2t0Yte+DA=="
  }

  @certification_invalid %{
    "currency" => "g1-test",
    "pubkey" => "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
    "buid" => "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
    "sig" =>
      "wqZxPEGxLrHGv8VdEIfUGvUcf+tDdNTMXjLzVRCQ4UhlhDRahOMjfcbP7byNYr5OfIl83S1MBxF7VJgu8YasCA==",
    "idty_issuer" => "mMPioknj2MQCX9KyKykdw8qMRxYR2w1u3UpdiEJHgXg",
    "idty_uid" => "mmpioerrrfkjfj",
    "idty_buid" => "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
    "idty_sig" =>
      "SmSweUD4lEMwiZfY8ux9maBjrQQDkC85oMNsin6oSQCPdXG8sFCZ4FisUaWqKsfOlZVb/HNa+TKzD2t0Yte+DA=="
  }

  def replaceKey(map, old_name, new_name) do
    {value, updated_map} = Map.pop(map, old_name)
    Map.put(updated_map, new_name, value)
  end

  def splitBuid(map, buid_name) do
    if buid_name do
      map
    else
      [block_number, block_hash] = Map.get(map, "buid") |> String.split("-")

      Map.delete(map, "buid")
      |> Map.put("block_number", block_number)
      |> Map.put("block_hash", block_hash)
    end
  end

  test "signature - basic case" do
    assert Doc.Certification.verif(@certification_valid)
    refute Doc.Certification.verif(@certification_invalid)
  end

  test "different possible key names" do
    for pubkey_name <- ["pubkey", "issuer", "from"],
        buid_name <- ["buid", nil],
        idty_issuer_name <- ["idty_issuer", "to"],
        version_name <- [nil, "version"] do
      valid =
        if version_name,
          do: Map.put(@certification_valid, version_name, @default_document_version),
          else:
            @certification_valid
            |> splitBuid(buid_name)
            |> replaceKey("pubkey", pubkey_name)
            |> replaceKey("idty_issuer", idty_issuer_name)

      invalid =
        if version_name,
          do: Map.put(@certification_invalid, version_name, @default_document_version),
          else:
            @certification_invalid
            |> splitBuid(buid_name)
            |> replaceKey("pubkey", pubkey_name)
            |> replaceKey("idty_issuer", idty_issuer_name)

      assert Doc.Certification.verif(valid)
      refute Doc.Certification.verif(invalid)
    end
  end

  test "wrong fields" do
    refute Doc.Certification.verif(@certification_valid |> replaceKey("pubkey", "pukey"))
    refute Doc.Certification.verif(@certification_valid |> replaceKey("currency", "curency"))
  end

  test "wrong values format" do
    refute Doc.Certification.verif(@certification_valid |> Map.put("currency", "$currency$"))
    refute Doc.Certification.verif(@certification_valid |> Map.put("pubkey", "$pubkey$"))

    refute Doc.Certification.verif(
             @certification_valid
             |> Map.put("idty_issuer", "$idty_issuer$")
           )

    refute Doc.Certification.verif(
             @certification_valid
             |> Map.put(
               "idty_buid",
               "7543x000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A"
             )
           )

    refute Doc.Certification.verif(
             @certification_valid
             |> Map.put(
               "idty_sig",
               "$*%SmSweUD4lEMwiZfY8ux9maBjrQQDkC85oMNsin6oSQCPdXG8sFCZ4FisUaWqKsfOlZVb/HNa+TKzD2t0Yte+DA=="
             )
           )

    refute Doc.Certification.verif(
             @certification_valid
             |> Map.put(
               "buid",
               "1678840001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038"
             )
           )

    refute Doc.Certification.verif(
             @certification_valid
             |> Map.put(
               "sig",
               "$*%SmSweUD4lEMwiZfY8ux9maBjrQQDkC85oMNsin6oSQCPdXG8sFCZ4FisUaWqKsfOlZVb/HNa+TKzD2t0Yte+DA=="
             )
           )
  end
end
