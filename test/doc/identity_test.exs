defmodule Doc.IdentityTest do
  use ExUnit.Case
  doctest Doc.Identity

  @default_document_version 10

  @identity_valid %{
    "currency" => "duniter_unit_test_currency",
    "issuer" => "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
    "signature" =>
      "1eubHHbuNfilHMM0G2bI30iZzebQ2cQ1PC7uPAw08FGMMmQCRerlF/3pc4sAcsnexsxBseA/3lY03KlONqJBAg==",
    "buid" => "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
    "uid" => "tic"
  }

  @identity_invalid %{
    "currency" => "duniter_unit_test_currency",
    "issuer" => "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
    "signature" =>
      "1eubHHbuNfilHMM0G2bI30iZzebQ2cQ1PC7uPAw08FGMMmQCRerlF/3pc4sAcsnexsxBseA/3lY03KlONqJBAg==",
    "buid" => "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
    "uid" => "tictic"
  }

  def replaceKey(map, old_name, new_name) do
    {value, updated_map} = Map.pop(map, old_name)
    Map.put(updated_map, new_name, value)
  end

  test "signature - basic case" do
    assert Doc.Identity.verif(@identity_valid)
    refute Doc.Identity.verif(@identity_invalid)
  end

  test "different possible key names" do
    for issuer_name <- ["issuer", "pubkey", "pub"],
        signature_name <- ["signature", "sig"],
        buid_name <- ["buid", "blockstamp"],
        version_name <- [nil, "version"] do
      valid =
        if version_name,
          do: Map.put(@identity_valid, version_name, @default_document_version),
          else:
            @identity_valid
            |> replaceKey("issuer", issuer_name)
            |> replaceKey("signature", signature_name)
            |> replaceKey("buid", buid_name)

      invalid =
        if version_name,
          do: Map.put(@identity_invalid, version_name, @default_document_version),
          else:
            @identity_invalid
            |> replaceKey("issuer", issuer_name)
            |> replaceKey("signature", signature_name)
            |> replaceKey("buid", buid_name)

      assert Doc.Identity.verif(valid)
      refute Doc.Identity.verif(invalid)
    end
  end

  test "wrong fields" do
    refute Doc.Identity.verif(@identity_valid |> replaceKey("issuer", "isuer"))
    refute Doc.Identity.verif(@identity_valid |> replaceKey("currency", "curency"))
  end

  test "wrong values format" do
    refute Doc.Identity.verif(@identity_valid |> Map.put("currency", "$currency$"))
    refute Doc.Identity.verif(@identity_valid |> Map.put("issuer", "$pubkey$"))

    refute Doc.Identity.verif(
             @identity_valid
             |> Map.put(
               "buid",
               "1678840001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038"
             )
           )

    refute Doc.Identity.verif(
             @identity_valid
             |> Map.put(
               "signature",
               "$*%SmSweUD4lEMwiZfY8ux9maBjrQQDkC85oMNsin6oSQCPdXG8sFCZ4FisUaWqKsfOlZVb/HNa+TKzD2t0Yte+DA=="
             )
           )
  end
end
