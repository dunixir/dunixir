defmodule HTTP.Blockchain.CurrentTest do
  @moduledoc """
  Test of HTTP request 'blockchain/current'
  """
  use ExUnit.Case

  @base_url 'https://g1-test.duniter.org/blockchain/block/'

  @base_url_localhost 'http://localhost:8085/blockchain/current'

  test "HTTP Blockchain/current get test" do
    last_block = DB.size('block') - 1
    {:ok, {_, _, res_baseline}} = :httpc.request(:get, {@base_url ++ to_charlist(last_block), []}, [], [])
    {:ok, {_, _, res}} = :httpc.request(:get, {@base_url_localhost, []}, [], [])
    assert Poison.decode!(res) == Poison.decode!(res_baseline)
  end
end
