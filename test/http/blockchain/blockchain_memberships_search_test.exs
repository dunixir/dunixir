defmodule HTTP.Blockchain.MembershipSearch.Test do
  use ExUnit.Case
  doctest BMA.Blockchain.Memberships

  @res_baseline %{
    "uid" => "totoUser",
    "sigDate" => 1_489_677_048,
    "pubkey" => "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
    "memberships" => [
      %{
        "written" => 0,
        "version" => 10,
        "membership" => "IN",
        "currency" => "g1-test",
        "blockNumber" => 7543,
        "blockHash" => "000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A"
      },
      %{
        "written" => 0,
        "version" => 10,
        "membership" => "IN",
        "currency" => "g1-test",
        "blockNumber" => 7543,
        "blockHash" => "000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A"
      }
    ]
  }

  setup_all do
    DB.start_link(['global_iindex_test_membershipsearchtest'])
    DB.start_link(['global_mindex_test_membershipsearchtest'])

    iindex_test = %{
      op: 'CREATE',
      uid: "totoUser",
      pub: "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
      created_on: 1_489_677_041,
      written_on: 1_489_677_048,
      member: true,
      wasMember: true,
      kick: false,
      age: 7,
      uidUnique: false,
      pubUnique: false,
      excludedIsMember: false,
      idBeingKicked: false,
      hasToBeExcluded: false
    }

    mindex_test = %{
      op: 'CREATE',
      pub: "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
      created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
      written_on: "0-E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855",
      expired_on: 0,
      expires_on: 1_489_677_041,
      revokes_on: 1_489_677_823,
      chainable_on: 1_489_677_045,
      type: 'JOIN',
      revoked_on: nil,
      leaving: false,
      age: 92,
      numberFollowing: false,
      distanceOk: false,
      onRevoked: false,
      joinsTwice: false,
      enoughCerts: false,
      leaversIsMember: false,
      activeIsMember: false,
      revokedIsMember: false,
      alreadyRevoked: false,
      revocationSigOk: false,
      isBeingRevoked: false,
      unchainables: false
    }

    DB.insert('global_iindex_test_membershipsearchtest', 0, iindex_test)
    DB.insert('global_mindex_test_membershipsearchtest', 0, mindex_test)
    DB.insert('global_mindex_test_membershipsearchtest', 1, mindex_test)

    res_search_uid =
      BMA.Blockchain.Memberships.get(
        "totoUser",
        'global_iindex_test_membershipsearchtest',
        'global_mindex_test_membershipsearchtest'
        )

    res_search_publickey =
      BMA.Blockchain.Memberships.get(
        "DNann1Lh55eZMEDXeYt59bzHbA3NJR46DeQYCS2qQdLV",
        'global_iindex_test_membershipsearchtest',
        'global_mindex_test_membershipsearchtest'
        )

    DB.delete_db('global_iindex_test_membershipsearchtest')
    DB.delete_db('global_mindex_test_membershipsearchtest')

    {:ok, res_search_uid: res_search_uid, res_search_publickey: res_search_publickey}
  end

  test "HTTP Blockchain/Memberships/search uid test", state do
    assert Poison.decode!(state.res_search_uid) == @res_baseline
  end

  test "HTTP Blockchain/Memberships/search public key test", state do
    assert Poison.decode!(state.res_search_publickey) == @res_baseline
  end
end
