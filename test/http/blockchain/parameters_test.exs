defmodule HTTP.Blockchain.ParametersTest do
  @moduledoc """
  Test of HTTP request 'blockchain/parameters'
  """
  use ExUnit.Case

  @base_url 'http://localhost:8085/blockchain/parameters'
  @paremeters '{
  "xpercent": 0.8,
  "udTime0": 1496527200,
  "udReevalTime0": 1496570400,
  "ud0": 1000,
  "stepMax": 5,
  "sigWindow": 1051920,
  "sigValidity": 12623040,
  "sigStock": 100,
  "sigReplay": 10,
  "sigQty": 5,
  "sigPeriod": 86400,
  "percentRot": 0.67,
  "msWindow": 1051920,
  "msValidity": 6311520,
  "msPeriod": 10,
  "medianTimeBlocks": 24,
  "idtyWindow": 1051920,
  "dtReeval": 631152,
  "dtDiffEval": 12,
  "dt": 86400,
  "currency": "g1-test",
  "c": 0.0488,
  "avgGenTime": 150
}'

  test "HTTP Blockchain/Parameters get test" do
    {:ok, {_, _, res}} = :httpc.request(:get, {@base_url, []}, [], [])
    assert res == @paremeters
  end
end
