defmodule BMA.Blockchain.HardshipTest do
  use ExUnit.Case

  @issuer_searched "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH"
  @issuer1 "DTgQ97AuJ8UgVXcxmNtULAs8Fg1kKC1Wr9SAS96Br9NG"
  @issuer2 "4FgeWzpWDQ2Vp38wJa2PfShLLKXyFGRLwAHA44koEhQj"
  @issuer3 "Do6Y6nQ2KTo5fB4MXbSwabXVmXHxYRB9UUAaTPKn1XqC"

  @bindex_block {:last_block, 5}

  @block0 %{
    version: 10,
    size: 827398,
    hash: "c1594191def0b65942fa28576b0e17b10938d05beea46a57ea3fbad5c6b61aa3",
    issuer: @issuer1,
    time: Time,
    number: 0,
    currency: "g1-test",
    previousHash: "61813a7900148b6d3890afd97ac6fb37880f3ecb7e7e0097d6955a856ce3a1e7",
    previousIssuer: nil,
    membersCount: 8,
    issuersCount: 29,
    issuerIsMember: false,
    issuersFrame: nil,
    issuersFrameVar: nil,
    issuerDiff: nil,
    avgBlockSize: 8273,
    medianTime: 1489677048,
    dividend: nil,
    mass: nil,
    massReeval: nil,
    unitBase: nil,
    powMin: 89,
    udTime: nil,
    diffTime: nil,
    speed: nil,
    diffNumber: nil,
    udReevalTime: nil,
    newDividend: nil,
    powZeros: nil,
    powRemainder: nil,
  }

  @block1 %{
    version: 10,
    size: 827398,
    hash: "c1594191def0b65942fa28576b0e17b10938d05beea46a57ea3fbad5c6b61aa3",
    issuer: @issuer2,
    time: Time,
    number: 1,
    currency: "g1-test",
    previousHash: "61813a7900148b6d3890afd97ac6fb37880f3ecb7e7e0097d6955a856ce3a1e7",
    previousIssuer: @issuer1,
    membersCount: 8,
    issuersCount: 29,
    issuerIsMember: false,
    issuersFrame: nil,
    issuersFrameVar: nil,
    issuerDiff: nil,
    avgBlockSize: 8273,
    medianTime: 1489677048,
    dividend: nil,
    mass: nil,
    massReeval: nil,
    unitBase: nil,
    powMin: 89,
    udTime: nil,
    diffTime: nil,
    speed: nil,
    diffNumber: nil,
    udReevalTime: nil,
    newDividend: nil,
    powZeros: nil,
    powRemainder: nil,
  }

  @block2 %{
    version: 10,
    size: 827398,
    hash: "c1594191def0b65942fa28576b0e17b10938d05beea46a57ea3fbad5c6b61aa3",
    issuer: @issuer3,
    time: Time,
    number: 2,
    currency: "g1-test",
    previousHash: "61813a7900148b6d3890afd97ac6fb37880f3ecb7e7e0097d6955a856ce3a1e7",
    previousIssuer: @issuer2,
    membersCount: 8,
    issuersCount: 29,
    issuerIsMember: false,
    issuersFrame: nil,
    issuersFrameVar: nil,
    issuerDiff: nil,
    avgBlockSize: 8273,
    medianTime: 1489677048,
    dividend: nil,
    mass: nil,
    massReeval: nil,
    unitBase: nil,
    powMin: 89,
    udTime: nil,
    diffTime: nil,
    speed: nil,
    diffNumber: nil,
    udReevalTime: nil,
    newDividend: nil,
    powZeros: nil,
    powRemainder: nil,
  }

  @block3_issuer %{
    version: 10,
    size: 827398,
    hash: "c1594191def0b65942fa28576b0e17b10938d05beea46a57ea3fbad5c6b61aa3",
    issuer: @issuer_searched,
    time: Time,
    number: 3,
    currency: "g1-test",
    previousHash: "61813a7900148b6d3890afd97ac6fb37880f3ecb7e7e0097d6955a856ce3a1e7",
    previousIssuer: @issuer3,
    membersCount: 8,
    issuersCount: 29,
    issuerIsMember: false,
    issuersFrame: nil,
    issuersFrameVar: nil,
    issuerDiff: nil,
    avgBlockSize: 8273,
    medianTime: 1489677048,
    dividend: nil,
    mass: nil,
    massReeval: nil,
    unitBase: nil,
    powMin: 89,
    udTime: nil,
    diffTime: nil,
    speed: nil,
    diffNumber: nil,
    udReevalTime: nil,
    newDividend: nil,
    powZeros: nil,
    powRemainder: nil,
  }

  @block3_non_issuer %{
    version: 10,
    size: 827398,
    hash: "c1594191def0b65942fa28576b0e17b10938d05beea46a57ea3fbad5c6b61aa3",
    issuer: @issuer1,
    time: Time,
    number: 3,
    currency: "g1-test",
    previousHash: "61813a7900148b6d3890afd97ac6fb37880f3ecb7e7e0097d6955a856ce3a1e7",
    previousIssuer: @issuer3,
    membersCount: 8,
    issuersCount: 29,
    issuerIsMember: false,
    issuersFrame: nil,
    issuersFrameVar: nil,
    issuerDiff: nil,
    avgBlockSize: 8273,
    medianTime: 1489677048,
    dividend: nil,
    mass: nil,
    massReeval: nil,
    unitBase: nil,
    powMin: 89,
    udTime: nil,
    diffTime: nil,
    speed: nil,
    diffNumber: nil,
    udReevalTime: nil,
    newDividend: nil,
    powZeros: nil,
    powRemainder: nil,
  }

  @block4_issuer %{
    version: 10,
    size: 827398,
    hash: "c1594191def0b65942fa28576b0e17b10938d05beea46a57ea3fbad5c6b61aa3",
    issuer: @issuer_searched,
    time: Time,
    number: 4,
    currency: "g1-test",
    previousHash: "61813a7900148b6d3890afd97ac6fb37880f3ecb7e7e0097d6955a856ce3a1e7",
    previousIssuer: @issuer_searched,
    membersCount: 8,
    issuersCount: 3,
    issuerIsMember: false,
    issuersFrame: 4,
    issuersFrameVar: nil,
    issuerDiff: nil,
    avgBlockSize: 8273,
    medianTime: 1489677048,
    dividend: nil,
    mass: nil,
    massReeval: nil,
    unitBase: nil,
    powMin: 89,
    udTime: nil,
    diffTime: nil,
    speed: nil,
    diffNumber: nil,
    udReevalTime: nil,
    newDividend: nil,
    powZeros: nil,
    powRemainder: nil,
  }

  @block4_non_issuer %{
    version: 10,
    size: 827398,
    hash: "c1594191def0b65942fa28576b0e17b10938d05beea46a57ea3fbad5c6b61aa3",
    issuer: @issuer2,
    time: Time,
    number: 4,
    currency: "g1-test",
    previousHash: "61813a7900148b6d3890afd97ac6fb37880f3ecb7e7e0097d6955a856ce3a1e7",
    previousIssuer: @issuer1,
    membersCount: 8,
    issuersCount: 29,
    issuerIsMember: false,
    issuersFrame: nil,
    issuersFrameVar: nil,
    issuerDiff: nil,
    avgBlockSize: 8273,
    medianTime: 1489677048,
    dividend: nil,
    mass: nil,
    massReeval: nil,
    unitBase: nil,
    powMin: 89,
    udTime: nil,
    diffTime: nil,
    speed: nil,
    diffNumber: nil,
    udReevalTime: nil,
    newDividend: nil,
    powZeros: nil,
    powRemainder: nil,
  }

  @block5_with_issuer %{
    version: 10,
    size: 827398,
    hash: "c1594191def0b65942fa28576b0e17b10938d05beea46a57ea3fbad5c6b61aa3",
    issuer: @issuer2,
    time: Time,
    number: 5,
    currency: "g1-test",
    previousHash: "61813a7900148b6d3890afd97ac6fb37880f3ecb7e7e0097d6955a856ce3a1e7",
    previousIssuer: @issuer_searched,
    membersCount: 8,
    issuersCount: 29,
    issuerIsMember: false,
    issuersFrame: 5,
    issuersFrameVar: nil,
    issuerDiff: nil,
    avgBlockSize: 8273,
    medianTime: 1489677048,
    dividend: nil,
    mass: nil,
    massReeval: nil,
    unitBase: nil,
    powMin: 89,
    udTime: nil,
    diffTime: nil,
    speed: nil,
    diffNumber: nil,
    udReevalTime: nil,
    newDividend: nil,
    powZeros: nil,
    powRemainder: nil,
  }

  @block5_without_issuer %{
    version: 10,
    size: 827398,
    hash: "c1594191def0b65942fa28576b0e17b10938d05beea46a57ea3fbad5c6b61aa3",
    issuer: @issuer2,
    time: Time,
    number: 5,
    currency: "g1-test",
    previousHash: "61813a7900148b6d3890afd97ac6fb37880f3ecb7e7e0097d6955a856ce3a1e7",
    previousIssuer: @issuer2,
    membersCount: 8,
    issuersCount: 29,
    issuerIsMember: false,
    issuersFrame: 5,
    issuersFrameVar: nil,
    issuerDiff: nil,
    avgBlockSize: 8273,
    medianTime: 1489677048,
    dividend: nil,
    mass: nil,
    massReeval: nil,
    unitBase: nil,
    powMin: 89,
    udTime: nil,
    diffTime: nil,
    speed: nil,
    diffNumber: nil,
    udReevalTime: nil,
    newDividend: nil,
    powZeros: nil,
    powRemainder: nil,
  }

  doctest BMA.Blockchain.Hardship

  setup_all do
    DB.start_link(['global_bindex_test_hardship'])
    # test with a previous block with the issuer

    DB.insert('global_bindex_test_hardship', :last_block, 5)

    DB.insert('global_bindex_test_hardship', 0, @block0)
    DB.insert('global_bindex_test_hardship', 1, @block1)
    DB.insert('global_bindex_test_hardship', 2, @block2)
    DB.insert('global_bindex_test_hardship', 3, @block3_issuer)
    DB.insert('global_bindex_test_hardship', 4, @block4_issuer)
    DB.insert('global_bindex_test_hardship', 5, @block5_with_issuer)

    hardship1 = BMA.Blockchain.Hardship.get(@issuer_searched, 'global_bindex_test_hardship')

    # test without any block with the issuer searched

    DB.delete('global_bindex_test_hardship', 3)
    DB.delete('global_bindex_test_hardship', 4)
    DB.delete('global_bindex_test_hardship', 5)

    DB.insert('global_bindex_test_hardship', 3, @block3_non_issuer)
    DB.insert('global_bindex_test_hardship', 4, @block4_non_issuer)
    DB.insert('global_bindex_test_hardship', 5, @block5_without_issuer)

    hardship2 = BMA.Blockchain.Hardship.get(@issuer_searched, 'global_bindex_test_hardship')

    DB.delete_db('global_bindex_test_hardship')

    {:ok, hardship1: hardship1, hardship2: hardship2}
  end

  test "hardship query", state do
    # level = powMin * exFact + handicap; powMin = 89; exFact = 1; handicap = 2
    assert state.hardship1 == "{\"level\":91,\"block\":6}"
    # level = powMin * exFact + handicap; powMin = 89; exFact = 1; handicap = 0
    assert state.hardship2 == "{\"level\":89,\"block\":6}"
  end
end
