defmodule HTTP.Wot.MembersTest do
  @moduledoc """
  Test of HTTP request 'wot/members'
  """
  use ExUnit.Case
  doctest BMA.Wot.Members

  setup_all do
    DB.start_link(['global_iindex_test_memberstest'])

    DB.insert(
      'global_iindex_test_memberstest',
      0,
      %{
        op: 'CREATE',
        uid: "tutuUser",
        pub: "tutuPub",
        created_on: 1_489_677_041,
        written_on: 1_489_677_048,
        member: true,
        wasMember: true,
        kick: false,
        age: 7,
        uidUnique: false,
        pubUnique: false,
        excludedIsMember: false,
        idBeingKicked: false,
        hasToBeExcluded: false
      }
    )

    DB.insert(
      'global_iindex_test_memberstest',
      1,
      %{
        op: 'CREATE',
        uid: "totoUser",
        pub: "totoPub",
        created_on: 1_489_677_041,
        written_on: 1_489_677_048,
        member: true,
        wasMember: true,
        kick: false,
        age: 7,
        uidUnique: false,
        pubUnique: false,
        excludedIsMember: false,
        idBeingKicked: false,
        hasToBeExcluded: false
      }
    )

    DB.insert(
      'global_iindex_test_memberstest',
      2,
      %{
        op: 'CREATE',
        uid: "totoUser",
        pub: "totoPub",
        created_on: 1_489_677_041,
        written_on: 1_489_677_048,
        member: false,
        wasMember: true,
        kick: false,
        age: 7,
        uidUnique: false,
        pubUnique: false,
        excludedIsMember: false,
        idBeingKicked: false,
        hasToBeExcluded: false
      }
    )

    DB.insert(
      'global_iindex_test_memberstest',
      3,
      %{
        op: 'CREATE',
        uid: "tutuUser",
        pub: "tutuPub",
        created_on: 1_489_677_041,
        written_on: 1_489_677_048,
        member: true,
        wasMember: true,
        kick: false,
        age: 7,
        uidUnique: false,
        pubUnique: false,
        excludedIsMember: false,
        idBeingKicked: false,
        hasToBeExcluded: false
      }
    )

    to_return = BMA.Wot.Members.get('global_iindex_test_memberstest')

    DB.delete_db('global_iindex_test_memberstest')

    {:ok, json: to_return}
  end

  test "members query", state do
    assert Poison.decode!(state.json) == %{
             "result" => [%{"pubKey" => "tutuPub", "uid" => "tutuUser"}]
           }
  end
end
