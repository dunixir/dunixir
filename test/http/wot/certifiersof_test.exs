defmodule BMA.Wot.CertifiersOfTest do
  use ExUnit.Case
  doctest BMA.Wot.CertifiersOf

  setup_all do
    DB.start_link(['global_iindex_test_certifiersof'])
    DB.start_link(['global_cindex_test_certifiersof'])
    DB.start_link(['global_bindex_test_certifiersof'])

    DB.insert(
      'global_iindex_test_certifiersof',
      0,
      %{
        op: 'CREATE',
        uid: "tutuUser",
        pub: "tutuPub",
        created_on: "10-hash10",
        written_on: "11-hash11",
        member: true,
        wasMember: true
      }
    )

    DB.insert(
      'global_iindex_test_certifiersof',
      1,
      %{
        op: 'CREATE',
        uid: "totoUser",
        pub: "totoPub",
        created_on: "11-hash11",
        written_on: "12-hash12",
        member: true,
        wasMember: true
      }
    )

    DB.insert(
      'global_iindex_test_certifiersof',
      2,
      %{
        op: 'CREATE',
        uid: "tataUser",
        pub: "tataPub",
        created_on: "12-ncvqjcvhcjls",
        written_on: "13-bhvslbhcqvcul",
        member: true,
        wasMember: true
      }
    )

    DB.insert(
      'global_iindex_test_certifiersof',
      3,
      %{
        op: 'CREATE',
        uid: "tutuUser",
        pub: "tutuPub",
        created_on: "10-hash10",
        written_on: "11-hash11",
        member: true,
        wasMember: true
      }
    )

    DB.insert(
      'global_cindex_test_certifiersof',
      0,
      %{
        op: 'CREATE',
        issuer: "tataPub",
        receiver: "totoPub",
        created_on: "10-hash10",
        written_on: "11-hash11",
        sig: "sigc0"
      }
    )

    DB.insert(
      'global_cindex_test_certifiersof',
      1,
      %{
        op: 'CREATE',
        issuer: "tataPub",
        receiver: "totoPub",
        created_on: "12-hash12",
        written_on: "13-hash13",
        sig: "sigc1"
      }
    )

    DB.insert(
      'global_cindex_test_certifiersof',
      2,
      %{
        op: 'CREATE',
        issuer: "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
        receiver: "mMPioknj2MQCX9KyKykdw8qMRxYR2w1u3UpdiEJHgXg",
        created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
        written_on: "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
        sig: "sigc2"
      }
    )

    DB.insert(
      'global_cindex_test_certifiersof',
      3,
      %{
        op: 'CREATE',
        issuer: "5B8iMAzq1dNmFe3ZxFTBQkqhq4fsztg1gZvxHXCk1XYH",
        receiver: "mMPioknj2MQCX9KyKykdw8qMRxYR2w1u3UpdiEJHgXg",
        created_on: "7543-000044410C5370DE8DBA911A358F318096B7A269CFC2BB93272E397CC513EA0A",
        written_on: "167884-0001DFCA28002A8C96575E53B8CEF8317453A7B0BA255542CCF0EC8AB5E99038",
        sig: "sigc3"
      }
    )

    DB.insert(
      'global_bindex_test_certifiersof',
      0,
      %{
        hash: "hash10",
        issuer: Issuer,
        number: 10,
        medianTime: 1_489_677_048
      }
    )

    DB.insert(
      'global_bindex_test_certifiersof',
      1,
      %{
        hash: "hash11",
        issuer: Issuer,
        number: 45,
        medianTime: 1_489_677_048
      }
    )

    DB.insert(
      'global_bindex_test_certifiersof',
      2,
      %{
        hash: "hash12",
        issuer: Issuer,
        number: 12,
        medianTime: 1_489_677_048
      }
    )

    DB.insert(
      'global_bindex_test_certifiersof',
      3,
      %{
        hash: "hash13",
        issuer: Issuer,
        number: 13,
        medianTime: 1_489_677_048
      }
    )

    json_to_return =
      BMA.Wot.CertifiersOf.get(
        "totoPub",
        'global_iindex_test_certifiersof',
        'global_cindex_test_certifiersof',
        'global_bindex_test_certifiersof'
      )

    DB.delete_db('global_iindex_test_certifiersof')
    DB.delete_db('global_cindex_test_certifiersof')
    DB.delete_db('global_bindex_test_certifiersof')

    {:ok, test_json: json_to_return}
  end

  test "certifiers_of query", state do
    assert state.test_json ==
             Poison.encode(
               %{
                 certifications: [
                   %{
                     cert_time: %{block: 12, medianTime: 1_489_677_048},
                     isMember: true,
                     pubKey: "tataPub",
                     sigDate: "13-bhvslbhcqvcul",
                     signature: "sigc1",
                     uid: "tataUser",
                     wasMember: true,
                     written: %{hash: "hash13", number: 13}
                   },
                   %{
                     cert_time: %{block: 10, medianTime: 1_489_677_048},
                     isMember: true,
                     pubKey: "tataPub",
                     sigDate: "13-bhvslbhcqvcul",
                     signature: "sigc0",
                     uid: "tataUser",
                     wasMember: true,
                     written: %{hash: "hash11", number: 11}
                   }
                 ],
                 isMember: true,
                 pubKey: "totoPub",
                 sigDate: "12-hash12",
                 uid: "totoUser"
               },
               pretty: true
             )
  end
end
