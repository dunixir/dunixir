defmodule Block.Augmentation.IssuersCountTest do
  @moduledoc """
  Test of validation rule BR_G04
  """
  use ExUnit.Case

  doctest Index.Augmentation.BIndex

  setup_all do
    local_bindex = :ets.new(:local_bindex, [:set, :protected])

    DB.start_link(['global_bindex_test_brg04'])
    DB.insert('global_bindex_test_brg04', 0, %{number: 0, issuersFrame: 1, issuer: "a"})
    DB.insert('global_bindex_test_brg04', 1, %{number: 1, issuersFrame: 2, issuer: "b"})
    DB.insert('global_bindex_test_brg04', 2, %{number: 2, issuersFrame: 3, issuer: "a"})

    :ets.insert(local_bindex, {4, %{number: 0}})
    Index.Augmentation.BIndex.issuersCount(local_bindex, 'global_bindex_test_brg04')
    [{4, head}] = :ets.lookup(local_bindex, 4)
    issuersCount0 = head.issuersCount

    :ets.insert(local_bindex, {4, %{number: 1}})
    Index.Augmentation.BIndex.issuersCount(local_bindex, 'global_bindex_test_brg04')
    [{4, head}] = :ets.lookup(local_bindex, 4)
    issuersCount1 = head.issuersCount

    :ets.insert(local_bindex, {4, %{number: 2}})
    Index.Augmentation.BIndex.issuersCount(local_bindex, 'global_bindex_test_brg04')
    [{4, head}] = :ets.lookup(local_bindex, 4)
    issuersCount2 = head.issuersCount

    :ets.insert(local_bindex, {4, %{number: 3}})
    Index.Augmentation.BIndex.issuersCount(local_bindex, 'global_bindex_test_brg04')
    [{4, head}] = :ets.lookup(local_bindex, 4)

    issuersCount3 = head.issuersCount

    DB.delete_db('global_bindex_test_brg04')

    {:ok,
     issuersCount0: issuersCount0,
     issuersCount1: issuersCount1,
     issuersCount2: issuersCount2,
     issuersCount3: issuersCount3}
  end

  test "check issuersCount", state do
    assert state.issuersCount0 == 0
    assert state.issuersCount1 == 1
    assert state.issuersCount2 == 2
    assert state.issuersCount3 == 2
  end
end
