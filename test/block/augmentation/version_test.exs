defmodule Block.Augmentation.VersionTest do
  @moduledoc """
  Test of validation rule BR_G49
  """
  use ExUnit.Case

  doctest Index.Augmentation.BIndex

  ## Testing BR_G49 - Version

  setup_all do
    local_bindex = :ets.new(:local_bindex, [:set, :protected])
    :ets.insert(local_bindex, {0, %{number: 0}})

    DB.start_link(['global_bindex_test_brg49'])
    DB.insert('global_bindex_test_brg49', 0, %{number: 0, version: 2})

    # We test with head.number = 0
    versionTest0 = Index.Augmentation.BIndex.version(local_bindex, 'global_bindex_test_brg49')

    # We test with head.number > 0
    :ets.insert(local_bindex, {0, %{number: 1, version: 1}})
    versionTest1 = Index.Augmentation.BIndex.version(local_bindex, 'global_bindex_test_brg49')

    # We test with head.version == head~1.version
    :ets.insert(local_bindex, {0, %{number: 1, version: 2}})
    versionTest2 = Index.Augmentation.BIndex.version(local_bindex, 'global_bindex_test_brg49')

    # We test with head.version == head~1.version + 1
    :ets.insert(local_bindex, {0, %{number: 1, version: 3}})
    versionTest3 = Index.Augmentation.BIndex.version(local_bindex, 'global_bindex_test_brg49')

    DB.delete_db('global_bindex_test_brg49')

    {:ok,
     versionTest0: versionTest0,
     versionTest1: versionTest1,
     versionTest2: versionTest2,
     versionTest3: versionTest3}
  end

  test "check BR_G49 - Version", state do
    assert state.versionTest0
    assert !state.versionTest1
    assert state.versionTest2
    assert state.versionTest3
  end
end
