defmodule Block.Augmentation.NumberTest do
  @moduledoc """
  Test of validation rule BR_G01
  """
  use ExUnit.Case

  doctest Index.Augmentation.BIndex

  setup_all do
    local_bindex = :ets.new(:local_bindex, [:set, :protected])
    :ets.insert(local_bindex, {0, %{pub: "pub"}})

    DB.start_link(['global_bindex_test_brg01'])

    Index.Augmentation.BIndex.number(local_bindex, 'global_bindex_test_brg01')
    [{0, head_vide}] = :ets.lookup(local_bindex, 0)
    [{:last_block, last_block_vide}] = DB.lookup('global_bindex_test_brg01', :last_block)
    Index.Augmentation.BIndex.number(local_bindex, 'global_bindex_test_brg01')
    [{0, head_nonvide}] = :ets.lookup(local_bindex, 0)
    [{:last_block, last_block_nonvide}] = DB.lookup('global_bindex_test_brg01', :last_block)

    DB.delete_db('global_bindex_test_brg01')

    {:ok,
     number_vide: head_vide.number,
     number_nonvide: head_nonvide.number,
     last_block_vide: last_block_vide,
     last_block_nonvide: last_block_nonvide}
  end

  test "check number", state do
    assert state.number_vide == 0
    assert state.number_nonvide == 1
    assert state.last_block_vide == 0
    assert state.last_block_nonvide == 1
  end
end
