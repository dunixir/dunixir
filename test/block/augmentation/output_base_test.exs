defmodule Block.Augmentation.OutputBaseTest do
  @moduledoc """
  Test of validation rule BR_G90
  """
  use ExUnit.Case

  doctest Index.Augmentation.SIndex

  setup_all do
    # create a local_sindex table
    local_sindex = :ets.new(:local_sindex, [:set, :protected])

    new_entry = %{
      op: "CREATE",
      tx: " ",
      identifier: " ",
      pos: 0,
      written_on: " ",
      written_time: " ",
      amount: 100,
      base: 3,
      locktime: 1,
      conditions: " ",
      consumed: true,
      created_on: " "
    }

    :ets.insert(local_sindex, {{"CREATE", "elem_0", pos: 0}, new_entry})
    :ets.insert(local_sindex, {{"CREATE", "elem_1", pos: 1}, new_entry})

    # create an empty global_bindex table
    DB.start_link(['global_bindex_test_brg90'])
    DB.insert('global_bindex_test_brg90', 0, %{number: 0, base: 5})
    DB.insert('global_bindex_test_brg90', 1, %{number: 1, base: 7})
    DB.insert('global_bindex_test_brg90', :last_block, %{number: 2, base: 7})

    ################## TEST FOR BR_G90 #####################
    res_test = Index.Augmentation.SIndex.outputBase(local_sindex, 'global_bindex_test_brg90')
    # IO.puts(res_test)
    DB.delete_db('global_bindex_test_brg90')

    {:ok, test_90: res_test}
  end

  test "check BR_G90", state do
    assert state.test_90
  end
end
