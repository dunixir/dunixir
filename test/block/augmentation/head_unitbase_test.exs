defmodule Block.Augmentation.HeadUnitbaseTest do
  @moduledoc """
  Test of validation rule BR_G12
  """
  use ExUnit.Case

  doctest Index.Augmentation.BIndex

  setup_all do
    # create a local_bindex table
    local_bindex = :ets.new(:local_bindex, [:set, :protected])
    :ets.insert(local_bindex, {0, %{number: 0}})

    # create an empty global_bindex table
    DB.start_link(['global_bindex_testbrg12'])

    ################## TEST FOR unitBase #####################
    Index.Augmentation.BIndex.unitBase(local_bindex, 'global_bindex_testbrg12')
    [{0, head}] = :ets.lookup(local_bindex, 0)
    unit_base0 = head.unitBase

    :ets.insert(local_bindex, {0, %{number: 1}})
    DB.insert('global_bindex_testbrg12', 0, %{number: 0, unitBase: 5})

    Index.Augmentation.BIndex.unitBase(local_bindex, 'global_bindex_testbrg12')
    [{0, head}] = :ets.lookup(local_bindex, 0)
    unit_base1 = head.unitBase

    ################## TEST FOR dividend #####################
    # :ets.insert(local_bindex, {0, %{number: 0, }})
    # DB.insert('global_bindex_testbrg12', 0, %{number: 0, unitBase: 5})

    DB.delete_db('global_bindex_testbrg12')

    {:ok, unit_base0: unit_base0, unit_base1: unit_base1}
  end

  test "check unitBase BR_G12", state do
    assert state.unit_base0 == 0
    assert state.unit_base1 == 5
  end
end
