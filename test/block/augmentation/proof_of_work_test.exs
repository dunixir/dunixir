defmodule Block.Augmentation.ProofOfWorkTest do
  @moduledoc """
  Test of validation rule BR_G62
  """
  use ExUnit.Case

  doctest Index.Augmentation.BIndex

  setup_all do
    local_bindex = :ets.new(:local_bindex, [:set, :protected])

    :ets.insert(
      local_bindex,
      {0,
       %{
         powZeros: 3,
         powRemainder: 12,
         hash: "0001CC094C5A8717521506DF6002945E7F4350B6DBA9BFC782E8AC68E697CDA0"
       }}
    )

    powTest0 = Index.Augmentation.BIndex.proofOfWork(local_bindex)

    :ets.insert(
      local_bindex,
      {0,
       %{
         powZeros: 3,
         powRemainder: 5,
         hash: "000ACC094C5A8717521506DF6002945E7F4350B6DBA9BFC782E8AC68E697CDA0"
       }}
    )

    powTest1 = Index.Augmentation.BIndex.proofOfWork(local_bindex)

    :ets.insert(
      local_bindex,
      {0,
       %{
         powZeros: 3,
         powRemainder: 7,
         hash: "000ACC094C5A8717521506DF6002945E7F4350B6DBA9BFC782E8AC68E697CDA0"
       }}
    )

    powTest2 = Index.Augmentation.BIndex.proofOfWork(local_bindex)

    :ets.insert(
      local_bindex,
      {0,
       %{
         powZeros: 3,
         powRemainder: 7,
         hash: "001ACC094C5A8717521506DF6002945E7F4350B6DBA9BFC782E8AC68E697CDA0"
       }}
    )

    powTest3 = Index.Augmentation.BIndex.proofOfWork(local_bindex)

    {:ok, powTest0: powTest0, powTest1: powTest1, powTest2: powTest2, powTest3: powTest3}
  end

  test "BR_G62 - Proof of Work", state do
    assert state.powTest0
    assert state.powTest1
    assert !state.powTest2
    assert !state.powTest3
  end
end
