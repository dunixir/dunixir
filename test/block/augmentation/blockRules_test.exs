defmodule Block.Augmentation.BlockRulesTest do
  use ExUnit.Case

  doctest Index.Augmentation.BIndex

  setup_all do
    local_bindex = :ets.new(:local_bindex, [:set, :protected])

    # BR_G51 - Number
    :ets.insert(local_bindex, {0, %{number: 0}})
    numberTest0 = Index.Augmentation.BIndex.ruleNumber(%{number: 1}, local_bindex)
    numberTest1 = Index.Augmentation.BIndex.ruleNumber(%{number: 0}, local_bindex)

    # BR_G52 - PreviousHash
    :ets.insert(
      local_bindex,
      {0, %{previousHash: "00009695F4FB676F5E1C94E59EA12F0043237636D339CEAD890DD9E35BA087D7"}}
    )

    previousHashBlock0 = %{
      previousHash: "00000EA8DAB64E234404A7086BBE29848368EFE4BF5E52BB6B3DDEA29BAC7B0E"
    }

    previousHashBlock1 = %{
      previousHash: "00009695F4FB676F5E1C94E59EA12F0043237636D339CEAD890DD9E35BA087D7"
    }

    previousHashTest0 =
      Index.Augmentation.BIndex.rulePreviousHash(previousHashBlock0, local_bindex)

    previousHashTest1 =
      Index.Augmentation.BIndex.rulePreviousHash(previousHashBlock1, local_bindex)

    # BR_G53 - PreviousIssuer
    :ets.insert(
      local_bindex,
      {0, %{previousIssuer: "E8Ah8g9vpK7x52Bt4Mzmz9f2rdp7j1dxdgcGiSkpMJ4y"}}
    )

    previousIssuerBlock0 = %{previousIssuer: "B8Ah8g9vpK7x52Bt4Mzmz9f2rdp7j1dxdgcGiSkpMJ4y"}
    previousIssuerBlock1 = %{previousIssuer: "E8Ah8g9vpK7x52Bt4Mzmz9f2rdp7j1dxdgcGiSkpMJ4y"}

    previousIssuerTest0 =
      Index.Augmentation.BIndex.rulePreviousIssuer(previousIssuerBlock0, local_bindex)

    previousIssuerTest1 =
      Index.Augmentation.BIndex.rulePreviousIssuer(previousIssuerBlock1, local_bindex)

    # BR_G54 - IssuersCount
    :ets.insert(local_bindex, {0, %{issuersCount: 2}})

    issuersCountTest0 =
      Index.Augmentation.BIndex.ruleIssuersCount(%{issuersCount: 4}, local_bindex)

    issuersCountTest1 =
      Index.Augmentation.BIndex.ruleIssuersCount(%{issuersCount: 2}, local_bindex)

    # BR_G55 - IssuersFrame
    :ets.insert(local_bindex, {0, %{issuersFrame: 11}})

    issuersFrameTest0 =
      Index.Augmentation.BIndex.ruleIssuersFrame(%{issuersFrame: 12}, local_bindex)

    issuersFrameTest1 =
      Index.Augmentation.BIndex.ruleIssuersFrame(%{issuersFrame: 11}, local_bindex)

    # BR_G56 - IssuersFrameVar
    :ets.insert(local_bindex, {0, %{issuersFrameVar: 0}})

    issuersFrameVarTest0 =
      Index.Augmentation.BIndex.ruleIssuersFrameVar(%{issuersFrameVar: 1}, local_bindex)

    issuersFrameVarTest1 =
      Index.Augmentation.BIndex.ruleIssuersFrameVar(%{issuersFrameVar: 0}, local_bindex)

    # BR_G57 - MedianTime
    :ets.insert(local_bindex, {0, %{medianTime: 1_497_531_232}})

    medianTimeTest0 =
      Index.Augmentation.BIndex.ruleMedianTime(%{medianTime: 1_497_531_880}, local_bindex)

    medianTimeTest1 =
      Index.Augmentation.BIndex.ruleMedianTime(%{medianTime: 1_497_531_232}, local_bindex)

    # BR_G58 - UniversalDividend
    :ets.insert(local_bindex, {0, %{new_dividend: 11}})
    dividendTest0 = Index.Augmentation.BIndex.ruleDividend(%{dividend: 10}, local_bindex)
    dividendTest1 = Index.Augmentation.BIndex.ruleDividend(%{dividend: 11}, local_bindex)

    # BR_G59 - UnitBase
    :ets.insert(local_bindex, {0, %{unitBase: 0}})
    unitBaseTest0 = Index.Augmentation.BIndex.ruleUnitBase(%{unitBase: 1}, local_bindex)
    unitBaseTest1 = Index.Augmentation.BIndex.ruleUnitBase(%{unitBase: 0}, local_bindex)

    # BR_G60 - MembersCount
    :ets.insert(local_bindex, {0, %{membersCount: 10}})

    membersCountTest0 =
      Index.Augmentation.BIndex.ruleMembersCount(%{membersCount: 11}, local_bindex)

    membersCountTest1 =
      Index.Augmentation.BIndex.ruleMembersCount(%{membersCount: 10}, local_bindex)

    # BR_G61 - PowMin
    :ets.insert(local_bindex, {0, %{number: 0, powMin: 10}})
    powMinTest0 = Index.Augmentation.BIndex.rulePowMin(%{powMin: 11}, local_bindex)
    :ets.insert(local_bindex, {0, %{number: 1, powMin: 10}})
    powMinTest1 = Index.Augmentation.BIndex.rulePowMin(%{powMin: 11}, local_bindex)
    powMinTest2 = Index.Augmentation.BIndex.rulePowMin(%{powMin: 10}, local_bindex)

    {:ok,
     numberTest0: numberTest0,
     numberTest1: numberTest1,
     previousHashTest0: previousHashTest0,
     previousHashTest1: previousHashTest1,
     previousIssuerTest0: previousIssuerTest0,
     previousIssuerTest1: previousIssuerTest1,
     issuersCountTest0: issuersCountTest0,
     issuersCountTest1: issuersCountTest1,
     issuersFrameTest0: issuersFrameTest0,
     issuersFrameTest1: issuersFrameTest1,
     issuersFrameVarTest0: issuersFrameVarTest0,
     issuersFrameVarTest1: issuersFrameVarTest1,
     medianTimeTest0: medianTimeTest0,
     medianTimeTest1: medianTimeTest1,
     dividendTest0: dividendTest0,
     dividendTest1: dividendTest1,
     unitBaseTest0: unitBaseTest0,
     unitBaseTest1: unitBaseTest1,
     membersCountTest0: membersCountTest0,
     membersCountTest1: membersCountTest1,
     powMinTest0: powMinTest0,
     powMinTest1: powMinTest1,
     powMinTest2: powMinTest2}
  end

  test "BR_G51 - Number", state do
    assert !state.numberTest0
    assert state.numberTest1
  end

  test "BR_G52 - PreviousHash", state do
    assert !state.previousHashTest0
    assert state.previousHashTest1
  end

  test "BR_G53 - PreviousIssuer", state do
    assert !state.previousIssuerTest0
    assert state.previousIssuerTest1
  end

  test "BR_G54 - DifferentIssuersCount", state do
    assert !state.issuersCountTest0
    assert state.issuersCountTest1
  end

  test "BR_G55 - IssuersFrame", state do
    assert !state.issuersFrameTest0
    assert state.issuersFrameTest1
  end

  test "BR_G56 - IssuersFrameVar", state do
    assert !state.issuersFrameVarTest0
    assert state.issuersFrameVarTest1
  end

  test "BR_G57 - MedianTime", state do
    assert !state.medianTimeTest0
    assert state.medianTimeTest1
  end

  test "BR_G58 - UniversalDividend", state do
    assert !state.dividendTest0
    assert state.dividendTest1
  end

  test "BR_G59 - UnitBase", state do
    assert !state.unitBaseTest0
    assert state.unitBaseTest1
  end

  test "BR_G60 - MembersCount", state do
    assert !state.membersCountTest0
    assert state.membersCountTest1
  end

  test "BR_G61 - PowMin", state do
    assert state.powMinTest0
    assert !state.powMinTest1
    assert state.powMinTest2
  end
end
