defmodule Block.Augmentation.PreviousHashTest do
  @moduledoc """
  Test of validation rule BR_G02
  """
  use ExUnit.Case

  doctest Index.Augmentation.BIndex

  setup_all do
    local_bindex = :ets.new(:local_bindex, [:set, :protected])
    :ets.insert(local_bindex, {0, %{number: 0}})

    DB.start_link(['global_bindex_test_brg02'])

    Index.Augmentation.BIndex.previousHash(local_bindex, 'global_bindex_test_brg02')
    [{0, head}] = :ets.lookup(local_bindex, 0)
    previousHash0 = head.previousHash

    :ets.insert(local_bindex, {0, %{number: 1}})
    DB.insert('global_bindex_test_brg02', 0, %{number: 0, hash: "hashTest"})

    Index.Augmentation.BIndex.previousHash(local_bindex, 'global_bindex_test_brg02')
    [{0, head}] = :ets.lookup(local_bindex, 0)
    previousHash1 = head.previousHash

    DB.delete_db('global_bindex_test_brg02')

    {:ok, previousHash0: previousHash0, previousHash1: previousHash1}
  end

  test "check previousHash", state do
    assert is_nil(state.previousHash0)
    assert state.previousHash1 == "hashTest"
  end
end
