defmodule Block.Augmentation.BlockSizeTest do
  @moduledoc """
  Test of validation rule BR_G50
  """
  use ExUnit.Case

  doctest Index.Augmentation.BIndex

  setup_all do
    local_bindex = :ets.new(:local_bindex, [:set, :protected])
    :ets.insert(local_bindex, {0, %{number: 0}})

    # We test with head.number = 0
    blockSizeTest0 = Index.Augmentation.BIndex.blockSize(local_bindex, 'global_bindex_test_brg50')

    # We test with head.number = 1 and different variations of size and avgBlockSize
    :ets.insert(local_bindex, {0, %{number: 1, avgBlockSize: 400, size: 300}})
    blockSizeTest1 = Index.Augmentation.BIndex.blockSize(local_bindex, 'global_bindex_test_brg50')

    :ets.insert(local_bindex, {0, %{number: 1, avgBlockSize: 400, size: 650}})
    blockSizeTest2 = Index.Augmentation.BIndex.blockSize(local_bindex, 'global_bindex_test_brg50')

    :ets.insert(local_bindex, {0, %{number: 1, avgBlockSize: 700, size: 650}})
    blockSizeTest3 = Index.Augmentation.BIndex.blockSize(local_bindex, 'global_bindex_test_brg50')

    {:ok,
     blockSizeTest0: blockSizeTest0,
     blockSizeTest1: blockSizeTest1,
     blockSizeTest2: blockSizeTest2,
     blockSizeTest3: blockSizeTest3}
  end

  test "check BR_G50 - Block size", state do
    assert state.blockSizeTest0
    assert state.blockSizeTest1
    assert !state.blockSizeTest2
    assert state.blockSizeTest3
  end
end
