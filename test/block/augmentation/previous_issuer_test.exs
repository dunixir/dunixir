defmodule Block.Augmentation.PreviousIssuerTest do
  @moduledoc """
  Test of validation rule BR_G03
  """
  use ExUnit.Case

  doctest Index.Augmentation.BIndex

  setup_all do
    local_bindex = :ets.new(:"test/local_bindex", [:set, :protected])
    DB.start_link(['global_bindex_test_brg03'])

    :ets.insert(local_bindex, {1, %{number: -1}})
    Index.Augmentation.BIndex.previousIssuer(local_bindex, 'global_bindex_test_brg03')
    [{1, head}] = :ets.lookup(local_bindex, 1)
    previousIssuer_nil = head.previousIssuer

    :ets.insert(local_bindex, {0, %{number: 2}})
    DB.insert('global_bindex_test_brg03', 0, %{number: 1, issuer: "John Smith"})
    Index.Augmentation.BIndex.previousIssuer(local_bindex, 'global_bindex_test_brg03')
    [{0, head}] = :ets.lookup(local_bindex, 0)
    previousIssuer_actual = head.previousIssuer

    DB.delete_db('global_bindex_test_brg03')

    {:ok, previousIssuer_nok: previousIssuer_nil, previousIssuer_ok: previousIssuer_actual}
  end

  test "check previousIssuer", state do
    assert state.previousIssuer_nok == nil
    assert state.previousIssuer_ok == "John Smith"
  end
end
