defmodule Block.Augmentation.CurrencyTest do
  @moduledoc """
  Test of validation rule BR_G99
  """
  use ExUnit.Case

  doctest Index.Augmentation.BIndex

  setup_all do
    local_bindex = :ets.new(:local_bindex, [:set, :protected])
    :ets.insert(local_bindex, {0, %{number: 0}})

    DB.start_link(['global_bindex_test_brg99'])

    Index.Augmentation.BIndex.currency(local_bindex, 'global_bindex_test_brg99')
    [{0, head}] = :ets.lookup(local_bindex, 0)
    currency0 = head.currency

    :ets.insert(local_bindex, {0, %{number: 1}})
    DB.insert('global_bindex_test_brg99', 0, %{number: 0, currency: "currencyTest"})

    Index.Augmentation.BIndex.currency(local_bindex, 'global_bindex_test_brg99')
    [{0, head}] = :ets.lookup(local_bindex, 0)
    currency1 = head.currency

    DB.delete_db('global_bindex_test_brg99')

    {:ok, currency0: currency0, currency1: currency1}
  end

  test "check currency", state do
    assert is_nil(state.currency0)
    assert state.currency1 == "currencyTest"
  end
end
