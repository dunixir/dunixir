defmodule Block.Validation.LocalTest do
  use ExUnit.Case
  doctest Block.Validation.Local

  @block_valid %{
    "version" => 10,
    "nonce" => 120_100_000_022_241,
    "number" => 146_317,
    "powMin" => 89,
    "time" => 1_534_583_833,
    "medianTime" => 1_534_581_249,
    "membersCount" => 1257,
    "monetaryMass" => 292_754_727,
    "unitbase" => 0,
    "issuersCount" => 32,
    "issuersFrame" => 161,
    "issuersFrameVar" => 0,
    "currency" => "g1",
    "issuer" => "DTgQ97AuJ8UgVXcxmNtULAs8Fg1kKC1Wr9SAS96Br9NG",
    "signature" =>
      "i4XzSLnslN3ewzsU9jyiI4pBnUcDq49AcCPTtx3rLHIdDRQSoClryEFWYbN/UXGHlnJTTLbD/DxhhUPTLs3HAA==",
    "hash" => "0000025CAB67E0F072E78C47D23771424431CD47FF7AAD781EBF2EF4C845F485",
    "parameters" => "",
    "previousHash" => "000005A03585E30BE20A2AC5A4ACC7A4297DCE57513BAC82FA31339A5F36499C",
    "previousIssuer" => "4FgeWzpWDQ2Vp38wJa2PfShLLKXyFGRLwAHA44koEhQj",
    "inner_hash" => "9DFDA56B29EF55D39B21D38A3092A65521C64105A64456F3ABE94956271B3ABC",
    "dividend" => nil,
    "identities" => [
      "5DUtKnEit3n9opxjn6MTHHtvG9JhNdb6paK8xLR261iP:BXZ06UNLE3AcvUQydfiYkZQGPiwfm3ZTHTSSBbeKQvZ8mMlIgJS9rE+cBcOYHyJis2pvG6v6Kabl+p4JTVxWBA==:136314-0000064DE8BD6F3B8FD9D2F13F1AE63F944265B0E2C63392C55CADC0DBB78F96:Sophie_Jiyu"
    ],
    "joiners" => [
      "5DUtKnEit3n9opxjn6MTHHtvG9JhNdb6paK8xLR261iP:thN8S9nON07dUhTgqrB891vhfQfR1yF82saJ/Y0mOnTk3rut0ksYLakj7ofNUBj55q+pxKbHWSoC/yiIIv3iAg==:136314-0000064DE8BD6F3B8FD9D2F13F1AE63F944265B0E2C63392C55CADC0DBB78F96:136314-0000064DE8BD6F3B8FD9D2F13F1AE63F944265B0E2C63392C55CADC0DBB78F96:Sophie_Jiyu"
    ],
    "actives" => [],
    "leavers" => [],
    "revoked" => [],
    "excluded" => [],
    "certifications" => [
      "6xR76SvEvyBwLsyEyXKAczs9tSYks2scJtxo9aERs3mU:5DUtKnEit3n9opxjn6MTHHtvG9JhNdb6paK8xLR261iP:146315:KTIL0dUinUiDGRvycxpKa+aqJk4MVVGqI7k6qaqqDycxP3HFbcwSLataVmToKs3u8d/6nS9ivlaJDScb7q7yBQ==",
      "71BherhBxbTKFECuV8wRAujVcfv6W3bxPuSu6zse9bB4:5DUtKnEit3n9opxjn6MTHHtvG9JhNdb6paK8xLR261iP:146058:YzCyoT8ZJP+e4ns9YdD6Vyii0/0Q/Nsu/jdCKGzVk6g+VCBQuQRUEAlz7YsQ1FTbDCDjiYupkEc3aVwept8jCA==",
      "34k6isdkoYZjcEvS3qZqATeTYjY1b3K1gLyW1K77Pea4:5DUtKnEit3n9opxjn6MTHHtvG9JhNdb6paK8xLR261iP:139477:Ses/F0EXyc5fiHaCHkW1GlPzbR1XMVsi2HI4BtgFYEHbgUUWeBTIJIsvD+7WGvmyYUqqdDA8FhBy7qH/CssGCg==",
      "5mv9d5YR1h5L2MffxWKevXsct27Abo8tSrz9bwGFSyiu:5DUtKnEit3n9opxjn6MTHHtvG9JhNdb6paK8xLR261iP:136687:ieEOuXp5FJeslMaLnj1BnG7fmlcIBPtI4q4msn32oCpxkKw+d5ycBKSFMehdZGcELOrXhJPayPEFpHq9TcfTCg==",
      "E3xrsFzU88ht8eYUrpiSkm3sreAFadmyvmfzzatpqjdA:5DUtKnEit3n9opxjn6MTHHtvG9JhNdb6paK8xLR261iP:136314:/VgOfvfDwT6drUsUFPLIa70toBHJMPlzSAcXgx5OD41WxCXWMrHL2KiYN68jmdo4heCEEMx1q+xaXtDh8VJ7CQ=="
    ],
    "transactions" => []
  }

  test "minimial test" do
    assert match?({:ok, _}, Block.Validation.Local.valid(@block_valid))
  end

  test "duplicate identities" do
    block_duplicate_identities =
      Map.update!(@block_valid, "identities", fn value -> value ++ value end)

    refute match?({:ok, _}, Block.Validation.Local.valid(block_duplicate_identities))
  end
end
