# Contributors as of 24/03/2022

- ABDELGHANI Nassim <nassim.abdelghani@imt-atlantique.net>
- Andy Mery <andy.mery@imt-atlantique.net>
- Augustin Jaujay <augustin.jaujay@laposte.net>
- BARBIER Thomas <thomas.barbier@imt-atlantique.net>
- BENARD Julien <julien.benard@imt-atlantique.net>
- Bruno MATEU <bruno.mateu@imt-atlantique.net>
- Bruno MATEU <mateubruno@gmail.com>
- Claire Girot <girotperson.claire@gmail.com>
- Etienne Ferrieux <etienne.ferrieux@gmail.com>
- Etienne Ferrieux <etienne.ferrieux@imt-atlantique.net>
- Etienne Ferrieux <etienne@Devuan.lan>
- FAMECHON Olivier <famechonolivier@macbook-pro-de-famechon.home>
- FAMECHON Olivier <olivier.famechon@imt-atlantique.net>
- JAUJAY Augustin <augustin.jaujay@imt-atlantique.net>
- Julien29121998 <benardjr@gmail.com>
- Kristof SZENTES <k19szent@fl-tp-br-365.imta.fr>
- MATEU Bruno <bruno.mateu@imt-atlantique.net>
- MERY Andy <andy.mery@imt-atlantique.net>
- MOLLARD Ambre <samuelle.mollard@imt-atlantique.net>
- MOLLARD Samuelle <samuel.mollard@imt-atlantique.net>
- NAMYST Romain <romain.namyst@imt-atlantique.net>
- Nassim <nassim.abdelghani@imt-atlantique.net>
- Noémie Rousset <noemie.rousset@imt-atlantique.net>
- PUBERT Quentin <quentin.pubert@imt-atlantique.net>
- Quentin PUBERT <quentin.pubert@imt-atlantique.net>
- Quentin PUBERT <quentin.pubert@outlook.fr>
- RIVOAL Samuel <samuel.rivoal@imt-atlantique.net>
- Rhum1N <romain.namyst@gmail.com>
- Ruyi ZHENG <standonthpeak@gmail.com>
- SUN Ye <ye.sun@imt-atlantique.net>
- Santiago Ruano Rincón <santiago.ruano-rincon@imt-atlantique.fr>
- Santiago Ruano Rincón <santiagorr@riseup.net>
- Thierry Jiao <jiaothierry@hotmail.fr>
- Thierry Jiao <thierry.jiao@imt-atlantique.net>
- Thomas Barbier <thomas.barbier@imt-atlantique.net>
- ZHANG Xinyue <xinyue.zhang2@imt-atlantique.net>
- ZHENG Ruyi <ruyi.zheng@imt-atlantique.net>
- Ziyu <lia.ziyu@gmail.com>
- eferrieu <etienne.ferrieux@imt-atlantique.net>
- kristofszentes <kristof.szentes@outlook.com>
- p19poupa <paul.poupard@imt-atlantique.net>
- t19tembo <theo.tembou-nzudie@imt-atlantique.net>
- x18zhan2 <hingjyutcheung@hotmail.com>
- xinyandu <xinyan.du@imt-atlantique.net>
- yesunch <sun.ye@hotmail.com>

You can update this list using the following command:

`git log --all --pretty="- %aN <%aE>%n- %cN <%cE>" | sort | uniq > CONTRIBUTORS.md`