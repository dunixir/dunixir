# Dunixir

Ce projet est une implémentation de [Duniter](https://git.duniter.org/nodes/typescript/duniter) en [Elixir](https://elixir-lang.org/). Il est en cours de développement; les tâches restantes sont consultables [ici](https://gitlab.imt-atlantique.fr/dunixir/dunixir/-/issues).
# Documentation

La [documentation du code](https://dunixir.gitlab-pages.imt-atlantique.fr/dunixir) et le [Wiki du projet](https://gitlab.imt-atlantique.fr/dunixir/dunixir/-/wikis/home) contient des ressources utiles pour comprendre le projet et poursuivre son développement.
# Fonctionnement du code

## Démarrage d'un noeud

A la racine du projet Dunixir, exécutez les commandes suivantes:

- `mix deps.get`

- `mix compile`

- `iex -S mix`

La dernière commande utilisera par défaut le port 8085 pour l’API HTTP, et le port 20900 pour l’API WS2P.

On peut préciser des ports différents, par exemple:

`HTTP_PORT=8086 WS2P_PORT=20901 iex -S mix`

## Serveur HTTP

Une fois le noeud démarré, on peut accéder aux endpoints de l’API HTTP depuis un navigateur.

L'adresse est `http://localhost:<HTTP_PORT>/<ENDPOINT>` avec:
- HTTP_PORT la valeur choisie au démarrage,
- ENDPOINT le endpoint auquel on souhaite accéder.  

Une liste des endpoints est consultable [ici](https://gitlab.imt-atlantique.fr/dunixir/dunixir/-/wikis/uploads/f8d3cdceee81b4ef1c6dd479fbd06d32/docs_Duniter_API_HTTP_-_Catalogue_-R%C3%A9sum%C3%A9.pdf).

Par exemple, pour obtenir des informations sur la version logicielle avec le port par défaut, on utilise cette adresse: `http://localhost:8085/node/summary`.

## Connexion à un noeud distant

Le noeud https://g1-test.duniter.org/ est un noeud du réseau g1-test. On peut se connecter à son API HTTP pour récupérer une liste de noeuds connus: https://g1-test.duniter.org/network/peers.

On peut ainsi se connecter à un noeud distant de la liste précédente, avec la commande:

`WS2P.Endpoint.connect!({IP, WS2P_PORT})` *avec IP l'adresse IPV4 du noeud distant en format string et WS2P_PORT la valeur choisie pour son port WS2P*

Par exemple:

`WS2P.Endpoint.connect!({"82.65.206.220", 10900})`

De la même manière, on peut lancer deux noeuds sur la même machine, et les connecter entre eux, avec la commande:

`WS2P.Endpoint.connect!({"localhost", WS2P_PORT})` *avec WS2P_PORT la valeur choisie au démarrage pour le port WS2P du second noeud*

## Envoi de documents

Une fois une connexion WS2P active, le noeud sera à l’écoute d'éventuels messages.

Il est possible d'en envoyer avec les commandes suivantes:

- `GenServer.cast(pid, {:local_object, block})` *où block est un objet Elixir qui va être encodé en JSON*

- `GenServer.cast(pid, {:local_msg, “message”})` *où le message est un string qui va être envoyé tel quel*

Le paramètre pid correspond à l’ID du GenServer qui gère la connexion avec le noeud que l’on souhaite contacter (une connexion = un GenServer).

Exemple d'échange entre deux noeuds d'une même machine:

***Terminal 1***

`iex -S mix`

***Terminal 2***

`HTTP_PORT=8086 WS2P_PORT=20901 iex -S mix`

*iex(1)>* `pid = WS2P.Endpoint.connect!({"localhost", 20900})`

*iex(2)>* `GenServer.cast(pid, {:local_object, %{"field"=>"value"}})`

# License

This software is distributed under [GNU GPLv3](https://gitlab.imt-atlantique.fr/dunixir/dunixir/-/blob/dev/LICENSE)